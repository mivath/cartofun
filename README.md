
### A space launcher
CartoFun-1.9 is a program for mountain hikers. Like a space launcher, it consists of several floors and of a payload.
* The first floor is **the WORLD interface**, which cuts the Earth surface into squares of one degree sides called "regions".

* The second floor is **the REGION interface**, which allows the exploration of a "region" within which one selects a square of 257 pixels of sides called "terrain" and containing the altitude values of the 257*257 points.

* The third floor is **the TERRAIN interface**, which takes charge of 3D visualization of a "terrain" and of the trails that run through it and whose routes are provided by OpenStreetMap contributors.
These trails are organized as roads and allow the calculation of routes from one point to another.

* The payload : this route is forwarded to **the RouteDlg plugin** which is responsible for making many calculations based in particular on the slope, and to take inventory of the different intersections.

### The open sources
* CartoFun-1.9 was developed in C++ with the 32bit MinGW compiler which accompanied (and still accompanies!) the different versions of Qt "open source".
This is only recently that has been decided a "port" to Visual C++, and quite naturally this is the 32 bit version that was chosen.
Moreover, MinGW and MSVC had to compile the same code, the libraries had to be the same also, and that's why the choice went, quite naturally again, to the "deprecated" QtWebKit library rather than QtWebEngine (that is incompatible with MinGW).
Despite these efforts of convergence, **compilation with MinGW-7.3.0** and **compilation with MSVC2017** have significant differences and are therefore subject to specific documentations.

* In addition to the **frameWork Qt** ([http://download.qt.io/archive/qt/5.13/5.13.2](http://download.qt.io/archive/qt/5.13/5.13.2)), CartoFun uses the following libraries :
  * Quazip : [https://sourceforge.net/projects/quazip/files/quazip/0.7.3/quazip-0.7.3.zip/download](https://sourceforge.net/projects/quazip/files/quazip/0.7.3/quazip-0.7.3.zip/download)
  * QtWebKit : [https://github.com/qtwebkit/qtwebkit/releases/tag/qtwebkit-5.212.0-alpha2](https://github.com/qtwebkit/qtwebkit/releases/tag/qtwebkit-5.212.0-alpha2)
  * GDAL : [http://www.gisinternals.com/query.html?content=filelist&file=release-1911-gdal-2-4-3-mapserver-7-4-2.zip](http://www.gisinternals.com/query.html?content=filelist&file=release-1911-gdal-2-4-3-mapserver-7-4-2.zip)
 
### Online documentation
* [english](https://mivath.gitlab.io/cartofun/en/index.html)
* [french](https://mivath.gitlab.io/cartofun/fr/index.html)

### Deployment of CartoFun-1.9 : 79.0 Mo
The binaries are available here : [http://michel.vanthodiep.free.fr/Qt/cartofun-1.9_mingw32_win10.zip](http://michel.vanthodiep.free.fr/Qt/cartofun-1.9_mingw32_win10.zip)



/*
 - Date : 20/03/2012
 - Nom : routedlg.cpp
*/

#include "routedlg.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

Slippy::Slippy()
{
   $scale=double(pow(2.0,ZOOM));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Slippy::Slippy(const QRectF& rect)
{
   $scale=double(pow(2.0,ZOOM));
   init(rect);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Slippy::init(const QRectF& rect)
{
   $rect=rect;
   $tile00=QPointF(int(lon2TileX($rect.left())),int(lat2TileY($rect.top())));
   $leftTop=QPointF(int(lon2MosaicX($rect.left())),int(lat2MosaicY($rect.top())));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Slippy::init(const QRectF& rect,QPair<int,int>& xPair,QPair<int,int>& yPair)
{
   $rect=rect;
   $tile00=QPointF(int(lon2TileX($rect.left())),int(lat2TileY($rect.top())));
   $leftTop=QPointF(int(lon2MosaicX($rect.left())),int(lat2MosaicY($rect.top())));

   xPair=QPair<int,int>(int(lon2TileX($rect.left())),int(lon2TileX($rect.right())));
   yPair=QPair<int,int>(int(lat2TileY($rect.top())),int(lat2TileY($rect.bottom())));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::lon2TileX(double lon)
{
   return (lon+180.0)/360.0*$scale;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::lat2TileY(double lat)
{
   return (1.0-log(tan(lat*M_PI/180.0)+1.0/cos(lat*M_PI/180.0))/M_PI)/2.0*$scale;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::lon2MosaicX(double lon)
{
   return (lon2TileX(lon)-$tile00.x())*256.0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::lat2MosaicY(double lat)
{
   return (lat2TileY(lat)-$tile00.y())*256.0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::lon2ImageX(double lon)
{
   return (lon2TileX(lon)-$tile00.x())*256.0-$leftTop.x();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::lat2ImageY(double lat)
{
   return (lat2TileY(lat)-$tile00.y())*256.0-$leftTop.y();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::imageX2Lon(double imageX)
{
   return tileX2Lon((imageX+$leftTop.x())/256.00+$tile00.x());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::imageY2Lat(double imageY)
{
   return tileY2Lat((imageY+$leftTop.y())/256.00+$tile00.y());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::tileX2Lon(double x)
{
   return x/$scale*360.0-180.0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

double Slippy::tileY2Lat(double y)
{
   double n=M_PI-2.0*M_PI*y/$scale;
   return 180.0/M_PI*atan(0.5*(exp(n)-exp(-n)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QRect& Slippy::extract(QRect& rect)
{
   int
      left=int(lon2MosaicX($rect.left())),top=int(lat2MosaicY($rect.top())),
      right=int(lon2MosaicX($rect.right())),bottom=int(lat2MosaicY($rect.bottom()));

   rect=QRect(QPoint(left,top),QPoint(right,bottom));
   return rect;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QSize Slippy::getImageSize()
{
   int
      left=int(lon2MosaicX($rect.left())),top=int(lat2MosaicY($rect.top())),
      right=int(lon2MosaicX($rect.right())),bottom=int(lat2MosaicY($rect.bottom()));

   return QSize(right-left+1,bottom-top+1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

int GraphicsView::getTypeItemCount(int type)
{  // outil de debogage, il compte tous les items d'un meme type
   // exemple d'utilisation : qDebug("count=%i",getTypeItemCount(QGraphicsEllipseItem::Type));
   QList<QGraphicsItem*> list=$graphicsScene->items();
   int count=0;

   for (int i=list.count()-1; i>=0; i--) if (list.at(i)->type()==type) count++;
   return count;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

GraphicsView::GraphicsView(qreal w,qreal h,QWidget* parent) : QGraphicsView(parent)
{
   setRenderHint(QPainter::Antialiasing,true);              // gere par Qt (trace bien plus "propre")
   setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);      // n'affiche pas de "scrollbar"
   setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   setMaximumWidth(w+100);    // dimensions ignorees si superieures a la scene (la fenetre s'adapte en fait a
   setMaximumHeight(h+100);   // celle-ci), mais permettent cependant l'innactivation de l'icone "plein ecran"
   setStyleSheet("background-color:#FFFFFF;border:none");   // en une seule fois! (sinon la 2eme ecrase la 1ere)

   $graphicsScene=new QGraphicsScene(0,0,w,h,this);
   $graphicsScene->setFont(QFont("Arial",8));
   setScene($graphicsScene);

   setFocusPolicy(Qt::ClickFocus);                          // the widget accepts (only?) focus by clicking
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void GraphicsView::verticalLabel(const QString& text,qreal x)
{  // "QPen pen(Qt::black,1.0);" par defaut
   QGraphicsTextItem* gti=$graphicsScene->addText(text,$graphicsScene->font());

   gti->setRotation(90);      // par rapport au coin leftTop
   gti->setPos(x+DELTA,$graphicsScene->height()-MARGIN);
   QRectF r=gti->sceneBoundingRect();
   $graphicsScene->addEllipse(r.left()+DELTA-2,r.top()-2,4,4,QPen(Qt::black),QBrush(Qt::black));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void GraphicsView::horizontalLabel(const QString& text,qreal y)
{
   QGraphicsTextItem* gti=$graphicsScene->addText(text,$graphicsScene->font());

   gti->setPos(MARGIN-gti->boundingRect().width(),y-DELTA);
   QRectF r=gti->sceneBoundingRect();
   $graphicsScene->addEllipse(r.right()-2,r.top()+DELTA-2,4,4,QPen(Qt::black),QBrush(Qt::black));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

Node::Node(const QRectF& rect,QGraphicsItem* parent) : QGraphicsEllipseItem(rect,parent)
{
   $initPos=$center=QPointF(rect.left()+rect.width()/2.0,rect.top()+rect.height()/2.0);

   setFlag(QGraphicsItem::ItemIsMovable,true);              // 1
   setFlag(QGraphicsItem::ItemSendsGeometryChanges,true);   // 0x800

   setBrush(QBrush(Qt::red));
   setPen(QPen(Qt::red));
   setZValue(1.0);                                          // 0 par defaut (cas du QGraphicsPathItem)
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Node::reset()
{  // retour a la position initiale
   moveBy(0,$initPos.y()-$center.y());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QVariant Node::itemChange(GraphicsItemChange change,const QVariant& value)
{
   if (change==ItemPositionChange && scene()) {
      QPointF shift=value.toPointF();        // deplacement (toujours des valeurs entieres)
      qreal y=$initPos.y()+shift.y();

      if (y<DELTA || y>scene()->sceneRect().height()-MARGIN) shift=$center-$initPos;
      else {
         $center.setY(y);
         shift.setX(pos().x());              // ancienne valeur
      }

      emit refresh();
      return shift;
   }
   return QGraphicsItem::itemChange(change,value);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

SlopeTab::SlopeTab(qreal w,qreal h,const QVector<qreal>& range,QVector<QPointF>& posVector) : GraphicsView(w,h)
{
   int
      nh=int((range[1]-range[0])*10)+1,
      nv=int(range[3]-range[2])+1,
      i,n= (nh>nv)? nh : nv;
   QPointF p,p0(MARGIN,DELTA),dp((w-MARGIN-DELTA)/(n-1),(h-MARGIN-DELTA)/(n-1)),r=QPointF(4,4);
   bool defaultFlag=posVector.isEmpty();

   $gpi=0;

   for (i=0; i<n; i++) {               // n : nombre de "points", correspondant en principe a la taille de "posVector"
      p= (defaultFlag)? p0+i*dp : posVector.at(i);
      $nodeVector.append(new Node(QRectF(p-r,p+r)));
      $graphicsScene->addItem($nodeVector.last());
      connect($nodeVector.last(),SIGNAL(refresh()),this,SLOT(refresh()));
   }

   drawGrid(range);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

SlopeTab::~SlopeTab()
{
   for (int j=$nodeVector.count()-1; j>=0; j--) delete $nodeVector[j];
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void SlopeTab::drawGrid(const QVector<qreal>& range)
{
   qreal w=$graphicsScene->width(),h=$graphicsScene->height(),step,d;
   QPen pen;
   pen.setDashPattern({1,5});    // 1 point suivi de 5 espaces (RouteDlg::drawPath), croisements pas tres jolis, mais bon...
   int i,first,mid,last;

   first=int(range.at(0)*100);
   last=int(range.at(1)*100);
   mid=(first+last)/2;           // le nombre de points est cense etre impair

   for (i=first,d=MARGIN,step=10*(w-DELTA-d)/(last-first); i<=last; i+=10,d+=step) {
      verticalLabel(QString::number(i)+"%",d);
      $graphicsScene->addLine(d,h-MARGIN,d,DELTA,(i==first || i==mid || i==last)? QPen() : pen);
   }

   first=int(range.at(2));
   last=int(range.at(3));
   for (i=first,d=h-MARGIN,step=(d-DELTA)/(last-first); i<=last; i++,d-=step) {
      horizontalLabel(QString::number(i)+" ",d);
      $graphicsScene->addLine(MARGIN,d,w-DELTA,d,(i==first || i==last)? QPen() : pen);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void SlopeTab::refresh()
{
   QPainterPath path;

   if ($gpi) $graphicsScene->removeItem($gpi);

   catmullRomToBezier(path);

   bool start=$polygon.isEmpty();
   $polygon=path.toFillPolygon();
   $polygon.pop_back();       // supprime le dernier element (sans quoi le polygone est ferme)
   $gpi=$graphicsScene->addPath(path,QPen(QBrush(Qt::darkGray),4,Qt::DashLine,Qt::RoundCap));

   if (start) emit polygon(&$polygon);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void SlopeTab::keyPressEvent(QKeyEvent* event)
{
   switch (event->key()) {
      case Qt::Key_Z:         // Ctrl+Z : la fameuse commande "Annuler"
         if (event->modifiers() & Qt::ControlModifier) {
            for (int i=$nodeVector.count()-1; i>=0; i--) $nodeVector.at(i)->reset();
            refresh();
         }
         break;

      default:                // appel de la classe de base
         QWidget::keyPressEvent(event);return;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void SlopeTab::leaveEvent(QEvent*)
{  // A leave event is sent to the widget when the mouse cursor leaves the widget
   QVector<QPointF> posVector;
   for (int i=0,n=$nodeVector.count(); i<n; i++) posVector.append($nodeVector.at(i)->center());

   emit nodePos(posVector);
   emit polygon(&$polygon);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void SlopeTab::catmullRomToBezier(QPainterPath& path)
{  // d'apres le code javascript d'adresse : http://schepers.cc/svg/path/dotty.svg
   int i,n;
   QPointF p0,p1,p2,p3,control1,control2;

   path.moveTo($nodeVector[0]->center());

   for (i=0,n=$nodeVector.count()-1; i<n; i++) {
      p1=$nodeVector[i]->center();
      p2=$nodeVector[i+1]->center();

      if (i==0) {                         // Catmull-Rom to Cubic Bezier conversion matrix
         p0=$nodeVector[i]->center();     //    0       1       0       0
         p3=$nodeVector[i+2]->center();   //  -1/6      1      1/6      0
      }                                   //    0      1/6      1     -1/6
      else if (i==n-1) {                  //    0       0       1       0
         p0=$nodeVector[i-1]->center();
         p3=$nodeVector[i+1]->center();
      }
      else {
         p0=$nodeVector[i-1]->center();
         p3=$nodeVector[i+2]->center();
      }

      control1=QPointF((-p0.x()+6*p1.x()+p2.x())/6,   (-p0.y()+6*p1.y()+p2.y())/6);
      control2=QPointF((p1.x()+6*p2.x()-p3.x())/6,    (p1.y()+6*p2.y()-p3.y())/6);

      path.cubicTo(control1,control2,p2);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

RouteTab::RouteTab(qreal w,qreal h,const QVector<qreal>& range,qreal thickness,Slippy* slippy,double pixelScale,
      const QMap<qreal,QVector3D>& ramp,qreal statMin,qreal statMax,const QColor& pathColor) : GraphicsView(w,h)
{  // do not specify parent widgets for them
   $range=range;
   $thickness=thickness;
   $slippy=slippy;
   $pixelScale=pixelScale;
   $statMin=statMin;
   $statMax=statMax;
   $pathColor=pathColor;

   $gradient=QLinearGradient(MARGIN,h-MARGIN,MARGIN,DELTA);

   for (QMap<qreal,QVector3D>::const_iterator i=ramp.constBegin(); i!=ramp.constEnd(); i++) {
      QVector3D v=i.value();                       // using an STL-style iterator
      $gradient.setColorAt(i.key(),QColor(v.x(),v.y(),v.z()));
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::setStats(qreal statMin,qreal statMax)
{
   $statMin=statMin;
   $statMax=statMax;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::goToNearestFork(Side direction)
{
   int m=$forkMap.count(),n=$path.elementCount();

   if (direction==LEFT) {        // en arriere (nettement plus complique que "en avant"!)
      if ($index==0) $index=n-1;
      else if (m==0 || $index<=$forkMap.firstKey()) $index=0;
      else if ($index>$forkMap.lastKey()) $index=$forkMap.lastKey();
      else if ($forkMap.contains($index)) $index=($forkMap.lowerBound($index)-1).key();
      else $index=($forkMap.upperBound($index)-1).key();
   }
   else if (direction==RIGHT) {  // en avant
      if ($index==n-1) $index=0;
      else if (m==0 || $index>=$forkMap.lastKey()) $index=n-1;
      else $index=$forkMap.upperBound($index).key();
   }
   else qFatal("qFatal RouteTab::goToNearestFork/1");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::printxy(Side x,int y,const char* fmt,...)
{
   va_list argptr;
   qreal d=5.0;
   int yPos=DELTA+y*15;             // 15 : nombre de pixels separant chaque ligne
   str256 s;

   va_start(argptr,fmt);
   vsprintf(s,fmt,argptr);

   QGraphicsTextItem* gti=$graphicsScene->addText(QString::fromLocal8Bit(s),QFont("Courier",8));

   if (x==LEFT) gti->setPos(MARGIN+d,yPos);                                                           // a gauche
   else if (x==RIGHT) gti->setPos($graphicsScene->width()-DELTA-d-gti->boundingRect().width(),yPos);  // a droite
   else qFatal("qFatal RouteTab::printxy/1");

   va_end(argptr);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::keyPressEvent(QKeyEvent* event)
{
   if (!isActive()) {
      QWidget::keyPressEvent(event);
      return;
   }

   int n=$path.elementCount(),inc=1;
   bool ctrl=false;

   if (event->modifiers() & Qt::ShiftModifier) inc=int(ceil(qreal(n)/100.0));
   else if (event->modifiers() & Qt::ControlModifier) ctrl=true;

   switch (event->key()) {
      case Qt::Key_Left:      // sert aussi pour naviguer entre les onglets (tabulation prealable!), mais bon...
         if (ctrl) goToNearestFork(LEFT);
         else {
            if ($index==0) $index=n-1;
            else $index= ($index-inc<0)? 0 : $index-inc;
         }
         break;

      case Qt::Key_Right:     // meme reflexion qu'avec Qt::Key_Left
         if (ctrl) goToNearestFork(RIGHT);
         else {
            if ($index==n-1) $index=0;
            else $index= ($index+inc>=n)?  n-1 : $index+inc;
         }
         break;

      case Qt::Key_Home:
         $index=0;break;

      case Qt::Key_End:
         $index=n-1;break;

      case Qt::Key_H:         // positionnement sur le point de hauteur maximum
         $index=$indexH;break;

      default:                // appel de la classe de base
         QWidget::keyPressEvent(event);return;
   }

   refresh();
   emit update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool RouteTab::getIndexData(QVector3D& indexData)
{
   if (!isActive()) return false;

   indexData=$dlgVector.at($index);
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool RouteTab::isActive()
{
   return ($graphicsScene->items().count()>8);  // 8 : le nombre d'items d'une page "Nothing to draw!"
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::mousePressEvent(QMouseEvent* event)
{
   if (event->button()!=Qt::LeftButton || !isActive()) {
      QGraphicsView::mousePressEvent(event);
      return;
   }

   qreal x=event->localPos().x(),y=event->localPos().y(),w=$graphicsScene->width(),h=$graphicsScene->height();
   int low=0,mid,top=$path.elementCount()-1;

   if (!(x>MARGIN && x<w-DELTA && y>DELTA && y<h-MARGIN) || top<0) return;

   while (low<=top) {                                    // fouille dichotomique
      mid=(low+top)/2;
      if (QPointF($path.elementAt(mid)).x()<x) low=mid+1;
      else top=mid-1;
   }
   if (QPointF($path.elementAt(mid)).y()<=y) {
      $index=mid;
      refresh();
      emit update();
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

qreal RouteTab::f(qreal x)
{
   int low=0,mid=0,top=$polygon->count()-1;

   while (low<=top) {                                    // fouille dichotomique
      mid=(low+top)/2;
      if ($polygon->at(mid).x()<x) low=mid+1;
      else top=mid-1;
   }
   if ($polygon->at(mid).x()>x) mid--;                   // et si mid==0 ?

   QPointF p=$polygon->at(mid),q=$polygon->at(mid+1);

   return (x-p.x())*(q.y()-p.y())/(q.x()-p.x())+p.y();   // (q.x()-p.x()==0) possible ?
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::init(const QString& msg)
{
   qreal w=$graphicsScene->width(),h=$graphicsScene->height();

   $graphicsScene->clear();
   $graphicsScene->addRect(QRectF(QPointF(MARGIN,DELTA),QPointF(w-DELTA,h-MARGIN)));

   verticalLabel(QString::number(0),MARGIN);

   horizontalLabel(QString::number($statMax),DELTA);
   horizontalLabel(QString::number($statMin),h-MARGIN);

   if (!msg.isEmpty()) printxy(LEFT,0,"%s",qPrintable(msg));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::refresh()
{
   init();     // efface tous les items

   qreal w=$graphicsScene->width(),h=$graphicsScene->height();
   qreal length=0,c2=h-MARGIN,c=(c2-DELTA)/($statMax-$statMin),elevation;
   QPainterPath gradientPath=$path,colorPath;

   colorPath.moveTo(QPointF($path.elementAt(0)));
   for (int i=1; i<=$index; i++) {
      length+=$lengthVector.at(i);
      colorPath.lineTo(QPointF($path.elementAt(i)));
   }

   gradientPath.lineTo(QPointF(w-DELTA,h-MARGIN));
   gradientPath.lineTo(QPointF(MARGIN,h-MARGIN));
   gradientPath.closeSubpath();
   $graphicsScene->addPath(gradientPath,QPen(),QBrush($gradient));

   QPen pathPen($pathColor);
   pathPen.setWidth(2);
   $graphicsScene->addPath(colorPath,pathPen);                             // a ajouter apres gradientPath

   QPointF point=QPointF($path.elementAt($index));
   QPen dashPen;
   dashPen.setDashPattern({1,5});                                          // 1 point suivi de 5 espaces

   horizontalLabel(QString::number($hMax),DELTA+c*($statMax-qreal($hMax)));
   horizontalLabel(QString::number($hMin),DELTA+c*($statMax-qreal($hMin)));
   verticalLabel(QString::number(qRound($length)),w-DELTA);

   $graphicsScene->addLine(point.x(),point.y(),MARGIN,point.y(),dashPen);  // pointilles horizontaux
   elevation=$dlgVector[$index].z()*($statMax-$statMin)/$thickness+$statMin;
   horizontalLabel(QString::number(qRound(elevation)),point.y());

   $graphicsScene->addLine(point.x(),point.y(),point.x(),c2,dashPen);      // pointilles verticaux
   verticalLabel(QString::number(qRound(length)),point.x());

   QColor color= ($index==0)? Qt::blue : ($index==$path.elementCount()-1)? Qt::red : Qt::black;
   $graphicsScene->addEllipse(point.x()-3,point.y()-3,6,6,color,QBrush(color));  // plus de bordure noire

   moreInfo(elevation,length);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::minMaxSlope(qreal& slopeMin,qreal& slopeMax)
{
   if (!$index) {
      slopeMin=slopeMax=0;
      return;
   }

   qreal slope;

   slopeMin=1E9;
   slopeMax=-1E9;

   for (int i=1; i<=$index; i++) {
      slope=$slopeVector.at(i);

      if (slope<slopeMin) slopeMin=slope;
      if (slope>slopeMax) slopeMax=slope;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::polygon(QPolygonF* polygon)
{  // slot execute suite a la modif de la courbe "slope", seuls les champs de duree (identifies grace a "mn"!) sont affectes
   $polygon=polygon;

   if (!$lengthVector.count() || !isActive()) return;

   QList<QGraphicsItem*> list=$graphicsScene->items();
   QGraphicsItem* gi;
   int k=0;

   for (int i=0,m=list.count(); i<m; i++) {
      gi=list.at(i);
      if (gi->type()==QGraphicsTextItem::Type && ((QGraphicsTextItem*)gi)->toPlainText().contains("mn")) {
         $graphicsScene->removeItem(gi);                 // efface le resultat precedent
         k++;
      }
   }
   if (k!=2) qFatal("qFatal RouteTab::polygon/1");

   duration();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::moreInfo(qreal elevation,qreal length)
{  // en cas de modif de l'affichage, ne pas oublier de mettre a jour "RouteTab::polygon"
   QSize imageSize=$slippy->getImageSize();
   qreal min=-SIDE/2.0,ax=SIDE/imageSize.height(),ay=SIDE/imageSize.width();
   QVector3D v=$dlgVector.at($index);
   qreal x=(v.y()-min)/ay,y=(v.x()-min)/ax;
   qreal lon=$slippy->imageX2Lon(x),lat=$slippy->imageY2Lat(y);
   qreal dh=elevation-$dlgVector[0].z()*($statMax-$statMin)/$thickness-$statMin,slopeMin,slopeMax;

   minMaxSlope(slopeMin,slopeMax);

   // left
   printxy(LEFT,0,"lat %-9.7f%c",qAbs(lat),(lat>=0)? 'N' : 'S');
   printxy(LEFT,1,"lon %-9.7f%c",qAbs(lon),(lon>=0)? 'E' : 'W');  // les coordonnees OSM ont 7 decimales
   printxy(LEFT,2,"length %im",qRound(length));
   printxy(LEFT,3,"h %im",qRound(elevation));
   printxy(LEFT,4,"dh %im",qRound(dh));                           // negatif possible

   if (!$forkMap.isEmpty() && $forkMap.contains($index)) {
      quint16 value=$forkMap.value($index);
      printxy(LEFT,5,"fork (%i,%i/%i)",value>>8 & 0xFF,value>>4 & 0xF,value & 0xF); // de la gauche vers la droite
   }  // dans l'ordre : num fork, num de la branche en partant de la gauche, nombre de branches (a l'exclusion de celle d'ou l'on vient)

   // right
   duration();
   printxy(RIGHT,3,"slope min %0.2f%%",slopeMin*100.0);           // doublage du '%' pour l'afficher
   printxy(RIGHT,4,"slope max %0.2f%%",slopeMax*100.0);
   printxy(RIGHT,5,"slope %0.2f%%",$slopeVector[$index]*100.0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::duration()
{
   qreal
      slope,speed,seconds[]={0,0},
      range0=$range.at(0),range1=$range.at(1),
      range2=$range.at(2),range3=$range.at(3),
      a1=($graphicsScene->width()-MARGIN-DELTA)/(range1-range0),
      b1=MARGIN-a1*range0,
      a2=5.0*(range2-range3)/18.0/($graphicsScene->height()-MARGIN-DELTA),
      b2=5.0*range3/18.0-a2*DELTA;                       // en m/s : 1000/3600=5/18

   for (int j=1; j<=$index; j++) {
      slope=$slopeVector.at(j);

      for (int i=0; i<2; i++) {
         if (slope<=range0) speed=$polygon->first().y();
         else if (slope>=range1) speed=$polygon->last().y();
         else speed=f(a1*slope+b1);
         seconds[i]+=$lengthVector.at(j)/(a2*speed+b2);
         slope=-slope;
      }
   }

   int s0=qRound(seconds[0]),m0=s0/60,h0=m0/60;
   printxy(RIGHT,0,"aller %ih %imn %is",h0,m0%60,s0%60);

   int s1=qRound(seconds[1]),m1=s1/60,h1=m1/60;
   printxy(RIGHT,1,"retour %ih %imn %is",h1,m1%60,s1%60);

   int s2=s0+s1,m2=s2/60,h2=m2/60;
   printxy(RIGHT,2,"total %ih %imn %is",h2,m2%60,s2%60);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::setData(const QVector<QVector3D>& dlgVector,const QMap<int,quint16>& forkMap)
{  // default : const QMap<int,quint16>& forkMap=QMap<int,quint16>()
   $dlgVector=dlgVector;
   $forkMap=forkMap;
   getLength();
   refresh();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteTab::getLength()
{  // cxy=(taille_du_pixel*nombre_de_cellules-1)/taille du terrain
   QVector3D *v,*w;
   qreal
      cxy=$pixelScale*(qreal(DIM)-1)/SIDE,cxycxy=cxy*cxy,
      d,dd,h,flatLength=0.0,length=0.0,
      cz=($statMax-$statMin)/$thickness,wz,min,max,   // cz=1/e
      maxy=10000.0,y,c1,c2=$graphicsScene->height()-MARGIN,c3=(c2-DELTA)/$thickness;
   int i,n;
   QVector<qreal> tmpVector;

   // preliminaires
   tmpVector.append(0);                // les donnees commencant a partir de l'index 1
   $lengthVector.clear();
   $lengthVector.append(0);
   $slopeVector.clear();
   $slopeVector.append(0);
   v=&$dlgVector[0];
   min=max=v->z();

   // 1ere passe
   for (i=1,n=$dlgVector.count(); i<n; i++) {
      w=&$dlgVector[i];
      wz=w->z();
      if (wz<min) min=wz;
      if (wz>max) max=wz;
      dd=((w->x()-v->x())*(w->x()-v->x())+(w->y()-v->y())*(w->y()-v->y()))*cxycxy;
      h=(wz-v->z())*cz;
      d=sqrt(dd);
      tmpVector.append(d);
      flatLength+=d;
      d=sqrt(dd+h*h);
      $lengthVector.append(d);
      $slopeVector.append(h/d);
      length+=d;                       // formulaire, p. 141
      v=w;
   }

   c1=($graphicsScene->width()-MARGIN-DELTA)/flatLength;
   $path-=$path;                       // "clear()", en quelque sorte!
   $path.moveTo(MARGIN,c2-$dlgVector[0].z()*c3);

   // 2eme passe
   for (i=1; i<n; i++) {
      d=tmpVector.at(i)*c1;
      y=c2-$dlgVector[i].z()*c3;       // donne delta avec pour hauteur statMax, height-margin avec statMin

      $path.lineTo($path.currentPosition().x()+d,y);
      if (y<maxy) {
         $indexH=$path.elementCount()-1;
         maxy=y;
      }
   }

   // affectation des attributs
   $index=$path.elementCount()-1;      // positionnement par defaut a la fin du path
   $length=length;
   $hMin=qRound(min*cz)+$statMin;
   $hMax=qRound(max*cz)+$statMin;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteDlg::closeEvent(QCloseEvent*)
{
   emit closeRouteDlg();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteDlg::keyPressEvent(QKeyEvent* event)
{  // si cet "escape" n'est pas gere, le plugin se ferme sans provoquer l'evenement "closeEvent"
   if (event->key()==Qt::Key_Escape) close();      // entraine l'evenement "closeEvent"
   else QWidget::keyPressEvent(event);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteDlg::showEvent(QShowEvent*)
{  // utilite ? (voir QWidget::showEvent)
   ((RouteTab*)this->page(0))->setFocus(Qt::ActiveWindowFocusReason);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteDlg::center()
{  // ancetre (class Window : public QMainWindow) determine par tatonnements grace a la methode "pos"
   QWidget* ancestor=this->parentWidget()->parentWidget()->parentWidget()->parentWidget();
   QPoint ap=ancestor->pos();
   QSize as=ancestor->size(),ts=this->size();
   int x=ap.x()+(as.width()-ts.width())/2,y=ap.y()+(as.height()-ts.height())/2;

   move(x,y);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteDlg::setTabText(int index,const QString& label)
{
   $tabs->setTabText(index,label);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RouteDlg::setCurrentIndex(int index)
{
   $tabs->setCurrentIndex(index);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

RouteDlg::RouteDlg(qreal thickness,const QRectF& rect,double pixelScale,const QMap<qreal,QVector3D>& ramp,
   QVector<QPointF>& posVector,qreal statMin,qreal statMax,QWidget* parent) :
   QWidget(parent,Qt::Window | Qt::MSWindowsFixedSizeDialogHint)  // modalite : false par defaut
{
   qreal w=600,h=400;
   QVector<qreal> range={-0.5,0.5,1.0,7.0};     // [-0.5,0.5] : abcisses, [1.0,7.0] : ordonnees (nb de pts : impair)
   $slippy=new Slippy(rect);

   $tabs=new QTabWidget(this);
   $tabs->setGeometry(0,0,w+7,h+27);            // corrections determinees de maniere empirique

   RouteTab* page0=new RouteTab(w,h,range,thickness,$slippy,pixelScale,ramp,statMin,statMax,Qt::magenta);
   RouteTab* page1=new RouteTab(w,h,range,thickness,$slippy,pixelScale,ramp,statMin,statMax,QColor("#FFBF00"));   // jaune d'or
   SlopeTab* page2=new SlopeTab(w,h,range,posVector);

   $tabs->addTab(page0,"hiking");
   $tabs->addTab(page1,"gps file");
   $tabs->addTab(page2,"slope");

   connect(page0,SIGNAL(update()),parent,SLOT(update()));
   connect(page1,SIGNAL(update()),parent,SLOT(update()));
   connect(page2,SIGNAL(polygon(QPolygonF*)),page0,SLOT(polygon(QPolygonF*)));
   connect(page2,SIGNAL(polygon(QPolygonF*)),page1,SLOT(polygon(QPolygonF*)));
   connect(page2,SIGNAL(nodePos(QVector<QPointF>&)),parent,SLOT(nodePos(QVector<QPointF>&)));
   connect(this,SIGNAL(closeRouteDlg()),parent,SLOT(closeRouteDlg()));

   page2->refresh();                   // seul moyen pour emettre le signal "polygon" au demarrage

   setWindowTitle("CartoFun");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

RouteDlg::~RouteDlg()
{
   delete $slippy;

   delete (RouteTab*)this->page(0);    // page0
   delete (RouteTab*)this->page(1);    // page1
   delete (SlopeTab*)this->page(2);    // page2
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

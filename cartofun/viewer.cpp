/*
 - Date : 20/06/2012
 - Nom : viewer.cpp
*/

#include "viewer.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

QString lonLatToString(const QPointF& lonLat)
{
   QString s;
   return s.sprintf("%c%8.6f_%c%8.6f",(lonLat.y()>=0)? 'N' : 'S',qAbs(lonLat.y()),(lonLat.x()>=0)? 'E' : 'W',qAbs(lonLat.x()));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

Transform::Transform(const QString& terrainDir)
{
   const char* arg[]={
      "exe_name",       // 0
      "-s_srs",         // 1 (s_ource)
      PROJ,             // 2
      "-t_srs",         // 3 (t_arget)
      "WGS84"           // 4
   };

   // attributs
   $terrainDir=terrainDir;
   $gdalTransform=new GdalTransform(sizeof(arg)/sizeof(char*),(char**)arg);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Transform::setModel(const double modelPixelScale[3],const double modelTiepoint[6])
{
   $pixelScale0=modelPixelScale[0];
   $pixelScale1=modelPixelScale[1];
   $tiepoint3=modelTiepoint[3];
   $tiepoint4=modelTiepoint[4];

   $position=QPointF(0,0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Transform::getCorners(QPointF& lonwLatn,QPointF& loneLats)
{
   lonwLatn=pixelToLonLat($position);
   loneLats=pixelToLonLat($position+QPointF(DIM-1,DIM-1));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QRectF Transform::getRect()
{
   return QRectF(pixelToLonLat($position),pixelToLonLat($position+QPointF(DIM-1,DIM-1)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QString Transform::preset()
{
   return $terrainDir+lonLatToString(pixelToLonLat($position+QPointF(0,DIM-1)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QPointF Transform::pixelToLonLat(const QPointF& p,bool oldStyle)
{  // default : bool oldStyle=false
   static const qreal R=qreal(DIM)/qreal(DIM-1);
   static const QPointF K(0.5,-0.5);

   str1k szLine;
   double dfX,dfY,dfZ;
   QPointF q= (oldStyle)? p+K : $position+R*(p-$position)+K;

   sprintf(szLine,"%f %f",
      $tiepoint3+q.x()*$pixelScale0,                     // modelPixelScale[1]
      $tiepoint4-q.y()*$pixelScale1                      // modelPixelScale[0]
   );
   $gdalTransform->convert(szLine,&dfX,&dfY,&dfZ);
   return QPointF(dfX,dfY);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

QPointF UnTransform::lonLatToPixel(const QPointF& lonLat)
{
   str1k szLine;
   double dfX,dfY,dfZ;

   sprintf(szLine,"%f %f",lonLat.x(),lonLat.y());
   $gdalTransform->convert(szLine,&dfX,&dfY,&dfZ);

   dfX=(dfX-$tiepoint3)/$pixelScale0;
   dfY=($tiepoint4-dfY)/$pixelScale1;

   return QPointF(dfX-0.5,dfY-0.5);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void UnTransform::setModel(const double modelPixelScale[3],const double modelTiepoint[6])
{
   $pixelScale0=modelPixelScale[0];
   $pixelScale1=modelPixelScale[1];
   $tiepoint3=modelTiepoint[3];
   $tiepoint4=modelTiepoint[4];
}

////////////////////////////////////////////////////////////////////////////////////////////////////

UnTransform::UnTransform()
{
   const char* arg[]={
      "exe_name",       // 0
      "-i",             // 1 : transformation inverse, de la destination vers la source
      "-s_srs",         // 2 (s_ource)
      PROJ,             // 3
      "-t_srs",         // 4 (t_arget)
      "WGS84"           // 5
   };

   $gdalTransform=new GdalTransform(sizeof(arg)/sizeof(char*),(char**)arg);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void GLop::glopMakeIdentityd(double m[16])
{
   m[0+4*0]=1; m[0+4*1]=0; m[0+4*2]=0; m[0+4*3]=0;
   m[1+4*0]=0; m[1+4*1]=1; m[1+4*2]=0; m[1+4*3]=0;
   m[2+4*0]=0; m[2+4*1]=0; m[2+4*2]=1; m[2+4*3]=0;
   m[3+4*0]=0; m[3+4*1]=0; m[3+4*2]=0; m[3+4*3]=1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void GLop::glopMultMatrixVecd(const double matrix[16],const double in[4],double out[4])
{
   for (int i=0; i<4; i++) out[i]=in[0]*matrix[0*4+i] + in[1]*matrix[1*4+i] + in[2]*matrix[2*4+i] + in[3]*matrix[3*4+i];
}

////////////////////////////////////////////////////////////////////////////////////////////////////

int GLop::glopInvertMatrixd(const double src[16],double inverse[16])
{
   int i,j,k,swap;
   double t,temp[4][4];

   for (i=0; i<4; i++)
      for (j=0; j<4; j++) temp[i][j]=src[i*4+j];

   glopMakeIdentityd(inverse);

   for (i=0; i<4; i++) {
      // Look for largest element in column
      swap=i;
      for (j=i+1; j<4; j++) {
         if (fabs(temp[j][i])>fabs(temp[i][i])) swap=j;
      }

      if (swap!=i)   // Swap rows.
         for (k=0; k<4; k++) {
            t=temp[i][k];
            temp[i][k]=temp[swap][k];
            temp[swap][k]=t;

            t=inverse[i*4+k];
            inverse[i*4+k]=inverse[swap*4+k];
            inverse[swap*4+k]=t;
         }

      // No non-zero pivot. The matrix is singular, which shouldn't happen.  This means the user gave us a bad matrix.
      if (temp[i][i]==0) return 0;

      t=temp[i][i];
      for (k=0; k<4; k++) {
         temp[i][k]/=t;
         inverse[i*4+k]/=t;
      }

      for (j=0; j<4; j++) {
         if (j!=i) {
            t=temp[j][i];
            for (k=0; k<4; k++) {
               temp[j][k]-=temp[i][k]*t;
               inverse[j*4+k]-=inverse[i*4+k]*t;
            }
         }
      }
   }
   return 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void GLop::glopMultMatricesd(const double a[16],const double b[16],double r[16])
{
   for (int i=0; i<4; i++)
      for (int j=0; j<4; j++)
         r[i*4+j]= a[i*4+0]*b[0*4+j] + a[i*4+1]*b[1*4+j] + a[i*4+2]*b[2*4+j] + a[i*4+3]*b[3*4+j];
}

////////////////////////////////////////////////////////////////////////////////////////////////////

int GLop::gluUnProject(double winx,double winy,double winz,const double modelMatrix[16],const double projMatrix[16],
      const int viewport[4],double* objx,double* objy,double* objz)
{  // origine : https://github.com/Alexpux/superglu/blob/master/libutil/project.c
   double finalMatrix[16],in[4],out[4];

   glopMultMatricesd(modelMatrix, projMatrix, finalMatrix);
   if (!glopInvertMatrixd(finalMatrix, finalMatrix)) return 0;

   in[0]=winx;
   in[1]=winy;
   in[2]=winz;
   in[3]=1.0;

   // Map x and y from window coordinates
   in[0]=(in[0]-viewport[0])/viewport[2];
   in[1]=(in[1]-viewport[1])/viewport[3];

   // Map to range -1 to 1
   in[0]=in[0]*2-1;
   in[1]=in[1]*2-1;
   in[2]=in[2]*2-1;

   glopMultMatrixVecd(finalMatrix,in,out);
   if (out[3]==0.0) return 0;

   out[0]/=out[3];
   out[1]/=out[3];
   out[2]/=out[3];

   *objx=out[0];
   *objy=out[1];
   *objz=out[2];

   return 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void GLop::crossProd(const double p[3],const double q[3],double r[3])
{
   r[0]=p[1]*q[2]-p[2]*q[1];
   r[1]=p[2]*q[0]-p[0]*q[2];
   r[2]=p[0]*q[1]-p[1]*q[0];
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void GLop::normalize(double p[3])
{  // note : quand on "normalize" un vecteur qui l'a deja ete, il ne change pas
   double d=sqrt(p[0]*p[0]+p[1]*p[1]+p[2]*p[2]);

   if (d==0.0) return;

   p[0]/=d;
   p[1]/=d;
   p[2]/=d;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void GLop::getCamPos(double cam[3],const double modelview[16])
{  // origine : http://www.gamedev.net/topic/397751-how-to-get-camera-position/
   cam[0]=-(modelview[0]*modelview[12] + modelview[1]*modelview[13] + modelview[2]*modelview[14]);
   cam[1]=-(modelview[4]*modelview[12] + modelview[5]*modelview[13] + modelview[6]*modelview[14]);
   cam[2]=-(modelview[8]*modelview[12] + modelview[9]*modelview[13] + modelview[10]*modelview[14]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

HikingThread::HikingThread(NodeMapType& nodeMap,WayMapType& wayMap,SummitMapType& summitMap,const QRectF& rect,
      const QByteArray& array,const QString& terrainDir,bool keepXmlFile,QObject* parent) :
   QThread(parent),$nodeMap(nodeMap),$wayMap(wayMap),$summitMap(summitMap),$rect(rect),
      $array(array),$terrainDir(terrainDir),$keepXmlFile(keepXmlFile)
{}

////////////////////////////////////////////////////////////////////////////////////////////////////

void HikingThread::run()
{
   XmlToFun($nodeMap,$wayMap,$summitMap,$rect,$array,$terrainDir,$keepXmlFile);
   emit operationDone((!$nodeMap.isEmpty()));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::nodePos(QVector<QPointF>& posVector)
{
   $posVector=posVector;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QVector<QPointF> Viewer::posVector()
{
   return $posVector;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::checkExtensions()
{
   QString extensions((const char*)glGetString(GL_EXTENSIONS));
   if (extensions.isEmpty()) qFatal("qFatal Viewer::checkExtensions/1");

   QStringList list={"GL_ARB_multisample","GL_ARB_vertex_buffer_object","GL_EXT_framebuffer_object",
         "GL_EXT_framebuffer_multisample"};

   for (int i=0,n=list.count(); i<n; i++)
      qDebug("%s=%s",qPrintable(list[i]),(extensions.contains(list[i]))? "true" : "false");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::isoSegment(int indexA,int indexO,int indexB)
{
   static qreal h=0;

   if (indexA==-1) {          // initialisation
      h=$vertexVector.at($posy*DIM+$posx).z();
      return;
   }

   const QVector3D &A=$vertexVector.at(indexA),&O=$vertexVector.at(indexO),&B=$vertexVector.at(indexB);
   qreal ra=(h-O.z())/(A.z()-O.z()),rb=(h-O.z())/(B.z()-O.z());

   glVertex3f((1-ra)*O.x()+ra*A.x(),(1-ra)*O.y()+ra*A.y(),h);
   glVertex3f((1-rb)*O.x()+rb*B.x(),(1-rb)*O.y()+rb*B.y(),h);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::paintIsoLine()
{  // trace des courbes d'iso-valeurs, origine : "D:\Dev\szafran\pdf\cours3.pdf"
   qreal h=$vertexVector.at($posy*DIM+$posx).z(),z1,z2,z3,z4;
   int i,j,k,n=DIM-1;

   isoSegment(-1,-1,-1);                                 // initialisation (seul le 1er "-1" est utile, mais bon...)

   glColor3f(1,1,1);                                     // white
   glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
   glBegin(GL_LINES);

   for (i=0; i<n; i++)
      for (j=0,k=i*DIM; j<n; j++,k++) {
         z1=$vertexVector.at(k).z();
         z2=$vertexVector.at(k+DIM).z();
         z3=$vertexVector.at(k+DIM+1).z();
         z4=$vertexVector.at(k+1).z();

         // triangle0 constitue des points P1, P2 et P3 (S1, S2 et S3 "signes" de P1, P2 et P3)
         if (!((z1>h && z2>h && z3>h)||(z1<=h && z2<=h && z3<=h))) {
            if ((z2>h && z3>h)||(z2<=h && z3<=h)) isoSegment(k+DIM,k,k+DIM+1);      // S1!=S2 et S1!=S3
            else if ((z1>h && z3>h)||(z1<=h && z3<=h)) isoSegment(k,k+DIM,k+DIM+1); // S2!=S1 et S2!=S3
            else isoSegment(k,k+DIM+1,k+DIM);                                       // S3!=S1 et S3!=S2
         }
         // triangle1 constitue des points P1, P4 et P3
         if (!((z1>h && z4>h && z3>h)||(z1<=h && z4<=h && z3<=h))) {
            if ((z4>h && z3>h)||(z4<=h && z3<=h)) isoSegment(k+DIM+1,k,k+1);        // S1!=S4 et S1!=S3
            else if ((z1>h && z3>h)||(z1<=h && z3<=h)) isoSegment(k,k+1,k+DIM+1);   // S4!=S1 et S4!=S3
            else isoSegment(k,k+DIM+1,k+1);                                         // S3!=S1 et S3!=S4
         }
      }

   glEnd();
   glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

float Viewer::mod(float f)
{
   return (f>=360)? f-360 : (f<0)? f+360 : f;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

char* Viewer::compass()
{
   static const char* array[]={"East","South-East","South","South-West","West","North-West","North","North-East"};
   static str256 buf;

   return strcpy(buf,array[qRound(qreal(int($theta))/45.0)%8]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::paintAxes()
{
   int i,j,k;

   glColor3f(1,0,0);                   // rouge
   glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

   glBegin(GL_LINE_STRIP);
   for (j=0,k=$posy*DIM; j<DIM; j++,k++) glVertex3f($vertexVector[k].x(),$vertexVector[k].y(),$vertexVector[k].z());
   glEnd();

   glBegin(GL_LINE_STRIP);
   for (i=0,k=$posx; i<DIM; i++,k+=DIM) glVertex3f($vertexVector[k].x(),$vertexVector[k].y(),$vertexVector[k].z());
   glEnd();

   glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::clearSegmPath()
{
   $segmPath.clear();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::addSegm(const Segm& segm,const QColor& color)
{
   $segmPath.append(QPair<Segm,char>(segm,(color==Qt::magenta)? 1 : 0));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::msgBox(bool update,const QString& s)
{
   if (update) setDlgHikingCurve();
   qDebug("%s",qPrintable(s));
   QMessageBox::warning(this,"warning",s);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Viewer::getNearest(qint64& nid,qint64& sid,qreal x,qreal y)
{  // nid : node le plus proche du click "a vol d'oiseau", sid : sommet le plus proche du click "sur la route"
   if ($nodeMap.isEmpty()) return false;

   double d,min=1e10;

   // recherche du node nid le plus proche "a vol d'oiseau" du click
   for (NodeMapType::const_iterator it=$nodeMap.constBegin(); it!=$nodeMap.constEnd(); it++) {
      const QPointF& p=(*it).first;
      d=(p.x()-x)*(p.x()-x)+(p.y()-y)*(p.y()-y);            // extraction racine inutile
      if (d<min) {
         min=d;
         nid=it.key();
      }
   }

   // recherche du sommet sid le plus proche "sur la route" de nid et appartenant au meme way que nid
   if ($summitMap.contains(nid)) sid=nid;
   else {
      QPair<qint64,qint16>& pair=$nodeMap[nid].second[0];   // nid noeud ordinaire, n'appartient qu'a un seul way
      qint64 wid=pair.first;
      qint16 index=pair.second;
      const QList<QPair<qint64,int>>& route=$wayMap[wid].first;
      QMap<qint16,qint64>::const_iterator it=$wayMap[wid].second.upperBound(index);    // yop!

      min=1e10;
      for (int j=0; j<2; j++,it--) {   // passage 1 : le sommet d'index superieur
         QPainterPath path;            // passage 2 : le sommet d'index inferieur
         qint16 begin=index,end=it.key();

         if (end<begin) qSwap(begin,end);

         path.moveTo($nodeMap[route[begin].first].first);
         for (int i=begin+1; i<=end; i++) path.lineTo($nodeMap[route[i].first].first);

         d=path.length();

         if (d<min) {
            min=d;
            sid=*it;
         }
      }
   }
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::unProject(QMouseEvent* event,double obj[3])
{  // origine : http://sites.google.com/site/vamsikrishnav/unproj.c
   makeCurrent();                                  // arff..., indispensable, sans quoi 'z' retourne toujours 0.0

   double modelview[16],projection[16];
   int viewport[4],x=event->x(),y=event->y();
   float z;

   glGetDoublev(GL_PROJECTION_MATRIX,projection);  // get the projection matrix
   glGetDoublev(GL_MODELVIEW_MATRIX,modelview);    // get the modelview matrix
   glGetIntegerv(GL_VIEWPORT,viewport);            // get the viewport

   // Read the window z co-ordinate (the z value on that point in unit cube)
   // glReadPixel retourne la valeur du DepthBuffer a un point donne (et si erreur?)
   glReadPixels(x,viewport[3]-y,1,1,GL_DEPTH_COMPONENT,GL_FLOAT,&z);

   // Unproject the window co-ordinates to find the world co-ordinates.
   if (!GLop::gluUnProject(x,viewport[3]-y,z,modelview,projection,viewport,&obj[0],&obj[1],&obj[2]))
         qFatal("qFatal Viewer::unProject/1");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::toggleIsRouting()
{
   $isRouting=!$isRouting;

   clearSegmPath();
   $stepList.clear();

   $routing->next();             // reinitialisation des methodes next (avec arguments par defaut)
   $branch->next();

   if ($routeDlg) ((RouteTab*)$routeDlg->page(0))->init("Nothing to draw!");

   update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::mouseDoubleClickEvent(QMouseEvent* event)
{
   static bool isRouting=true;

   double obj[3];
   qint64 nid,sid;
   Qt::MouseButton button=event->button();

   unProject(event,obj);

   if (getNearest(nid,sid,obj[0],obj[1])) {
      if (button==Qt::RightButton) {         // bouton droit : initialisation
         isRouting=$isRouting;
         clearSegmPath();
         $stepList={Step(nid,1)};
         if ($routeDlg) ((RouteTab*)$routeDlg->page(0))->init("Nothing to draw!");
         if (isRouting) $routing->next(nid,sid,button);
         else $branch->next(nid,button);
      }
      else if (button==Qt::LeftButton) {     // bouton gauche : cas usuel $routing, fin $branch
         if ((isRouting && $routing->next(nid,sid,button)) || (!isRouting && $branch->next(nid,button))) setDlgHikingCurve();
      }
      else return;

      update();
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::wheelEvent(QWheelEvent* event)
{
   $rho+= (event->delta()>0)? 10 : -10;
   update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::mousePressEvent(QMouseEvent* event)
{
   $lastPos=event->pos();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::mouseMoveEvent(QMouseEvent* event)
{
   if (event->buttons() && Qt::LeftButton) {
      $theta=mod($theta+event->x()-$lastPos.x());
      $phi=mod($phi+event->y()-$lastPos.y());
      update();
   }
   $lastPos=event->pos();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::moveTo(char dx,char dy)
{
   $posx=($posx+((dx>=0)? dx : DIM-1))%DIM;
   $posy=($posy+((dy>=0)? dy : DIM-1))%DIM;

   emit selectedPos(QPointF(qreal($posx),qreal($posy)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::setPos(qint16 posx,qint16 posy)
{
   $posx=posx;
   $posy=posy;

   emit selectedPos(QPointF(qreal($posx),qreal($posy)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::timeout()
{
   static QList<float> dq;
   static int frameCount=0,stop;

   if (frameCount==0) {
      float t[3];

      for (int i=0; i<3; i++) {
         dq.append($run[1][i]-$run[0][i]);   // to[i]-from[i] : -240, -38, 170
         t[i]=qAbs(dq.last());
      }

      float q= (t[0]>t[1] && t[0]>t[2])? t[0] : (t[1]>t[0] && t[1]>t[2])? t[1] : t[2];

      for (int i=0; i<3; i++) dq[i]/=q;
      stop=q+1;

      $rho=-$run[0][0];
      $phi=mod($run[0][1]-90);
      $theta=mod($run[0][2]-90);
   }
   else if (frameCount<stop) {
      $rho-=dq.at(0);
      $phi=mod($phi+dq.at(1));
      $theta=mod($theta+dq.at(2));
   }
   else  {                                   // (frameCount==stop)
      if ($timer->isActive()) $timer->stop();
      dq.clear();
      frameCount=0;
      return;
   }

   frameCount++;
   update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::keyPressEvent(QKeyEvent* event)
{  // codes des touches : "Qt Namespace"
   bool shift=(event->modifiers() & Qt::ShiftModifier),alt=(event->modifiers() & Qt::AltModifier);

   switch (event->key()) {
      case Qt::Key_Down:
         $phi=mod($phi+1);break;

      case Qt::Key_Up:
         $phi=mod($phi-1);break;

      case Qt::Key_Right:
         $theta=mod($theta+1);break;

      case Qt::Key_Left:
      $theta=mod($theta-1);break;

      case Qt::Key_PageUp:case Qt::Key_Plus:
         $rho++;break;

      case Qt::Key_PageDown:case Qt::Key_Minus:
         $rho--;break;

      case Qt::Key_F1:           // l'empecher d'etre negatif?, Qt::Key_Plus?, Qt::Key_Minus?
         $scaleZ+= (shift)? -10 : 10;
         break;

      case Qt::Key_F2:           // "lightX", plus vraiment importantes, releguees en fin de rangee...
         $light[0]+= (shift)? -1 : 1;break;

      case Qt::Key_F3:           // "lightY"
         $light[1]+= (shift)? -1 : 1;break;

      case Qt::Key_F4:           // "lightZ"
         $light[2]+= (shift)? -1 : 1;break;

      case Qt::Key_F5:           // animation : from (test : 300   50     0)
         $run[0]={-$rho,mod($phi+90),mod($theta+90)};
         $runFlag|=(1<<0);
         break;

      case Qt::Key_F6:           // animation : to   (test :  60   12   170)
         $run[1]={-$rho,mod($phi+90),mod($theta+90)};
         $runFlag|=(1<<1);
         break;

      case Qt::Key_F7:           // animation : lancement du timer
         if (!$timer->isActive() && $runFlag==3 && $run[0]!=$run[1]) $timer->start();
         return;

      case Qt::Key_1:
         if (alt) setPos(0,256);
         else moveTo(-1,1);      // Shift-1
         break;

      case Qt::Key_2:
         moveTo(0,1);break;      // Shift-2

      case Qt::Key_3:
         if (alt) setPos(256,256);
         else moveTo(1,1);       // Shift-3
         break;

      case Qt::Key_4:
         moveTo(-1,0);break;     // Shift-4

      case Qt::Key_5:
         setPos(128,128);break;  // Shift-5

      case Qt::Key_6:
         moveTo(1,0);break;      // Shift-6

      case Qt::Key_7:
         if (alt) setPos(0,0);
         else moveTo(-1,-1);     // Shift-7
         break;

      case Qt::Key_8:
         moveTo(0,-1);break;     // Shift-8

      case Qt::Key_9:
         if (alt) setPos(256,0);
         else moveTo(1,-1);      // Shift-9
         break;

      case Qt::Key_Tab:          // $branch : autre selection
         if ($nodeMap.isEmpty()) return;
         $branch->next(-1);
         break;

      case Qt::Key_Insert:       // $branch : validation
         if ($nodeMap.isEmpty()) return;
         if ($branch->next(-2)) setDlgHikingCurve();
         break;

      default: // it is very important that you call the base class implementation if you do not act upon the key
         QOpenGLWidget::keyPressEvent(event);return;
   }
   update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Viewer::~Viewer()
{
   $vertexBuffer->destroy();
   $normalBuffer->destroy();
   $textureBuffer->destroy();
   $indexBuffer->destroy();
   $hikingBuffer->destroy();
   $gpsBuffer->destroy();

   delete $texture;

   delete $routing;
   delete $branch;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Viewer::Viewer(QMap<qreal,QVector3D>& ramp,QVector<QPointF> posVector,bool isRouting,Transform* transform,QWidget* parent) :
      QOpenGLWidget(parent)
{
   $ramp=ramp;
   $posVector=posVector;
   $isRouting=isRouting;
   $transform=transform;

   int count=DIM*DIM;

   $vertexVector.reserve(count);
   $normalVector.reserve(count);
   $textureVector.reserve(count);
   $indexVector.reserve((DIM-1)*(DIM-1)*6);

   $vertexBuffer=new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
   $normalBuffer=new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
   $textureBuffer=new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
   $indexBuffer=new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
   $hikingBuffer=new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
   $gpsBuffer=new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);

   $routing=new Routing($nodeMap,$wayMap,$summitMap,$coef);
   $branch=new Branch($nodeMap,$wayMap,$summitMap);

   $texture=0;
   $routeDlg=0;
   $bgColor=QColor::fromCmykF(0.39,0.39,0,0).darker();      // merci HelloGL!
   $textColor=Qt::white;

   $light[0]=$light[1]=0.0;
   $light[2]=10.0;
   $light[3]=1.0;                            // ($light[3]==0) : lumiere directionnelle, ($light[3]!=0) : ponctuelle

   setFocusPolicy(Qt::StrongFocus);          // Qt::StrongFocus = TabFocus | ClickFocus (necessaire)

   presetConstVectors();

   makeConnection($routing);
   makeConnection($branch);

   connect(this,SIGNAL(getOptionState(const QString&,bool&)),parent,SLOT(getOptionState(const QString&,bool&)));
   connect(this,SIGNAL(resetOptions(const QStringList&)),parent,SLOT(resetOptions(const QStringList&)));
   connect(this,SIGNAL(selectedPos(QPointF)),parent,SLOT(selectedPos(QPointF)));

   $timer=new QTimer(this);
   $timer->setInterval(1000/24);             // framerate : 24 fps, la periode est donc de 41ms
   connect($timer,SIGNAL(timeout()),this,SLOT(timeout()));
   $run={{0,0,0},{0,0,0}};
   $runFlag=0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::makeConnection(const QObject* sender)
{
   connect(sender,SIGNAL(clearSegmPath()),this,SLOT(clearSegmPath()));
   connect(sender,SIGNAL(addSegm(const Segm&,const QColor&)),this,SLOT(addSegm(const Segm&,const QColor&)));
   connect(sender,SIGNAL(msgBox(bool,const QString&)),this,SLOT(msgBox(bool,const QString&)));
   connect(sender,SIGNAL(addStepList(const QList<Step>&)),this,SLOT(addStepList(const QList<Step>&)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::queryMetaData(const QString& metaData)
{  // "QXmlQuery" vraiment necessaire? (plutot que d'utiliser le DOM avec "QtXml")
   QXmlQuery query;
   query.setFocus(metaData);

   $statMin=getPAM(query,"STATISTICS_MINIMUM");
   $statMax=getPAM(query,"STATISTICS_MAXIMUM");
   $thickness=SIDE*ellipticArc()/100.0;
   qDebug("Viewer::queryMetaData : dh=%i  $thickness=%f",$statMax-$statMin,$thickness);

   if ($routeDlg) {
      ((RouteTab*)$routeDlg->page(0))->setStats($statMin,$statMax);
      ((RouteTab*)$routeDlg->page(1))->setStats($statMin,$statMax);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

qreal Viewer::ellipticArc()
{
   qreal a=9000,b=40,x=$statMax-$statMin;

   return b/a*sqrt(-x*x+2*a*x);  // renvoie un pourcentage
}

////////////////////////////////////////////////////////////////////////////////////////////////////

int Viewer::getPAM(QXmlQuery& query,const QString& name)
{  // PAM : "Persistent Auxiliary Metadata"
   query.setQuery("GDALMetadata/Item[@name='"+name+"']/fn:string()");
   QStringList list;

   if (!query.evaluateTo(&list)) qFatal("qFatal Viewer::getPAM/1");
   return list.at(0).toInt();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::perspective(double fovY,double aspect,double zNear,double zFar)
{  // origine : http://stackoverflow.com/questions/12943164/replacement-for-gluperspective-with-glfrustrum
   double fH=tan(fovY/360*M_PI)*zNear,fW=fH*aspect;

   glFrustum(-fW,fW,-fH,fH,zNear,zFar);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::resizeGL(int width,int height)
{
   glViewport(0,0,width,height);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   perspective(40,float(width)/float(height),1,100);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T> void Viewer::createBuffer(QOpenGLBuffer* buffer,QVector<T>* vector,QOpenGLBuffer::UsagePattern pattern)
{
   buffer->create();
   buffer->bind();
   buffer->setUsagePattern(pattern);
   buffer->allocate(vector->constData(),vector->size()*sizeof(T));
   buffer->release();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::createVBOs()
{
   createBuffer($vertexBuffer,&$vertexVector,QOpenGLBuffer::DynamicDraw);  // Vertex buffer
   createBuffer($normalBuffer,&$normalVector,QOpenGLBuffer::DynamicDraw);  // Normal buffer
   createBuffer($indexBuffer,&$indexVector,QOpenGLBuffer::StaticDraw);     // Indices buffer
   createBuffer($textureBuffer,&$textureVector,QOpenGLBuffer::StaticDraw); // texture buffer
   // traces
   createBuffer($hikingBuffer,&$hikingVector,QOpenGLBuffer::DynamicDraw);
   createBuffer($gpsBuffer,&$gpsPack.second,QOpenGLBuffer::DynamicDraw);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::lookAt(const double eye[3],const double center[3],double up[3])
{  // origine : https://forums.khronos.org/showthread.php/4991-The-Solution-for-gluLookAt()-Function!!!!
   double f[]={center[0]-eye[0],center[1]-eye[1],center[2]-eye[2]};

   GLop::normalize(f);
   GLop::normalize(up);

   double s[3],u[3];

   GLop::crossProd(f,up,s);
   GLop::crossProd(s,f,u);

   double M[]={
      s[0], u[0], -f[0], 0,
      s[1], u[1], -f[1], 0,
      s[2], u[2], -f[2], 0,
         0,    0,     0, 1
   };

   glMultMatrixd(M);
   glTranslated(-eye[0],-eye[1],-eye[2]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::initializeGL()
{  // This virtual function is called once before the first call to () or resizeGL()
   initializeOpenGLFunctions();

   checkExtensions();

   $texture=new QOpenGLTexture(QOpenGLTexture::Target2D);

   getTexture("color");
   createVBOs();

   glClearColor($bgColor.redF(),$bgColor.greenF(),$bgColor.blueF(),$bgColor.alphaF());

   // eclairage
   fv specular={float(0.3),float(0.3),float(0.3)}; // cast sinon warning msvc
   float shininess=10.0;                           // brillance (de 0 a 128) : 0 -> brillance maxi

   glMaterialfv(GL_FRONT,GL_SPECULAR,specular);    // Parametrage du materiau
   glMaterialf(GL_FRONT,GL_SHININESS,shininess);
   glEnable(GL_LIGHT0);                            // Enable light 0
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::paintGL()
{
   static double eye[]={0,0,5},center[]={0,0,0},up[]={0,1,0};

   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);   // initialisations de l'ecran et du z-buffer

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();                   // initialisation de la matrice de modelisation-visualisation

   lookAt(eye,center,up);
   glLightfv(GL_LIGHT0,GL_POSITION,$light);

   glTranslatef(0.0,0.0,$rho/10.0);    // on translate la scene le long de l'axe Oz de la valeur $rho
   glRotatef($phi,1.0,0.0,0.0);        // on tourne la scene d'un angle "$phi" autour de l'axe Ox
   glRotatef($theta,0.0,0.0,1.0);      // on tourne la scene d'un angle "$theta" autour de l'axe Oz
   glScalef(1.0,1.0,$scaleZ/100.0);

   glEnable(GL_DEPTH_TEST);            // necessaire
   glEnable(GL_TEXTURE_2D);
   glEnable(GL_POLYGON_OFFSET_FILL);
   glPolygonOffset(1,1);
   glEnable(GL_LIGHTING);              // activation de l'eclairage

   paintVBOs();

   glDisable(GL_LIGHTING);             // desactivation de l'eclairage
   glDisable(GL_POLYGON_OFFSET_FILL);
   glDisable(GL_TEXTURE_2D);

   if (optionsContains("isoline")) paintIsoLine();

   paintPath();
   paintPolyLines();

   paintAxes();

   glDisable(GL_DEPTH_TEST);

   renderText();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::renderText()                    // renderText : en souvenir de QGLWidget::renderText!
{
   QPainter painter(this);
   painter.setPen($textColor);
   QFont font("Courier",10);
   painter.setFont(font);

   // nettement plus optimise que QGLWidget::renderText, qui semble-il initialise un QPainter a chaque appel.
   painter.drawText(10, 20,QString::asprintf("rho     : %3i",int(-$rho)));
   painter.drawText(10, 35,QString::asprintf("phi     : %3i\u00B0",int(mod($phi+90))));
   painter.drawText(10, 50,QString::asprintf("theta   : %3i\u00B0",int(mod($theta+90))));
   painter.drawText(10, 65,QString::asprintf("scaleZ  : %3i%%",int($scaleZ)));

   painter.drawText(10, 90,QString::asprintf("lightX  : %3i",int($light[0])));
   painter.drawText(10,105,QString::asprintf("lightY  : %3i",int($light[1])));
   painter.drawText(10,120,QString::asprintf("lightZ  : %3i",int($light[2])));

   painter.drawText(10,145,QString::asprintf("compass : %s",compass()));

   painter.drawText(10,170,QString::asprintf("posx    :%4i",$posx-128));   // domaine : [-128,+128], default : (0,0)
   painter.drawText(10,185,QString::asprintf("posy    :%4i",$posy-128));

   if (($runFlag>>0)& 1) painter.drawText(10,210,
         QString::asprintf("from : %3i %3i %3i",int($run[0][0]),int($run[0][1]),int($run[0][2])));
   if (($runFlag>>1)& 1) painter.drawText(10,225,
         QString::asprintf("to   : %3i %3i %3i",int($run[1][0]),int($run[1][1]),int($run[1][2])));

   painter.end();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Viewer::polyLines(const QString& option,int size,const QColor& color,QOpenGLBuffer* buffer,qreal lineWidth)
{
   if (!(optionsContains(option) && size)) return false;

   glLineWidth(lineWidth);
   glColor3ub(color.red(),color.green(),color.blue());
   buffer->bind();
   glVertexPointer(3,GL_FLOAT,0,0);
   buffer->release();
   glDrawArrays(GL_LINES,0,size);

   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::paintPolyLines()
{
   bool hiking,gps;

   glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
   glEnableClientState(GL_VERTEX_ARRAY);

   hiking=polyLines("hiking",$hikingVector.size(),Qt::black,$hikingBuffer,1.0);
   gps=polyLines("gps file",$gpsPack.second.size(),QColor("#FFBF00"),$gpsBuffer,2.0);

   glDisableClientState(GL_VERTEX_ARRAY);
   glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
   glLineWidth(1.0);

   if (hiking && $stepList.count()) drawCookies(0);
   if (gps) drawCookies(1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::paintVBOs()
{
   glEnableClientState(GL_VERTEX_ARRAY);
   glEnableClientState(GL_NORMAL_ARRAY);
   glEnableClientState(GL_TEXTURE_COORD_ARRAY);

   $vertexBuffer->bind();
   glVertexPointer(3,GL_FLOAT,0,0);
   $vertexBuffer->release();

   $normalBuffer->bind();
   glNormalPointer(GL_FLOAT,0,0);
   $normalBuffer->release();

   $texture->bind();
   $textureBuffer->bind();
   glTexCoordPointer(2,GL_FLOAT,0,0);
   $textureBuffer->release();

   $indexBuffer->bind();
   glDrawElements(GL_TRIANGLES,$indexVector.size(),GL_UNSIGNED_INT,0);
   $indexBuffer->release();

   glDisableClientState(GL_TEXTURE_COORD_ARRAY);
   glDisableClientState(GL_NORMAL_ARRAY);
   glDisableClientState(GL_VERTEX_ARRAY);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::presetConstVectors()
{
   int i,j,k,n=DIM-1;
   qreal begin=-SIDE/2.0,di,dj;

   // vertices, dont les elements x et y sont constants (di+=STEP et dj+=STEP provoquent l'affichage -0.0,-0.0 pour le centre)
   for (i=0,di=begin; i<DIM; i++,di=begin+i*STEP)
      for (j=0,dj=begin; j<DIM; j++,dj=begin+j*STEP) $vertexVector.push_back(QVector3D(di,dj,0));

   // texture
   for (i=n; i>=0; i--)
      for (j=0; j<DIM; j++) $textureVector.push_back(QVector2D(j,i)/DIM);

   // indices
   for (i=0; i<n; i++)
      for (j=0,k=i*DIM; j<n; j++,k++) {
         $indexVector.push_back(k);
         $indexVector.push_back(k+DIM);
         $indexVector.push_back(k+DIM+1);
         $indexVector.push_back(k);
         $indexVector.push_back(k+DIM+1);
         $indexVector.push_back(k+1);
      }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QString Viewer::oldTexture(const QString& texture)
{
   static QString oldOne("");

   if (texture=="") return oldOne;        // lecture

   oldOne=texture;                        // ecriture
   return "";
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::getTexture(const QString& texture)
{
   QImage image;

   if (texture=="color") {
      image=QImage(DIM,DIM,QImage::Format_ARGB32);
      makeColorImage(image);
   }
   else {
      QString fileName=$transform->preset()+"_"+texture+".jpg";
      QImage texImage(fileName);
      if (texImage.isNull()) qFatal("qFatal Viewer::getTexture/1, file \"%s\"",qPrintable(fileName));
      qDebug("Viewer::getTexture/2 : file \"%s\" ok...",qPrintable(fileName));

      int delta=(qMax(texImage.width(),texImage.height())-1024)/246+4;  // formule magique, fruit de longues experimentations!
      image=QImage(texImage.size()+QSize(delta,delta),texImage.format());
      QPainter painter(&image);
      painter.drawImage(QPoint(0,delta),texImage);
      painter.end();
   }

   if ($texture->textureId()) {
      makeCurrent();
      $texture->destroy();
   }

   $texture->create();
   $texture->setSize(image.width(),image.height());
   $texture->setMinMagFilters(QOpenGLTexture::Linear,QOpenGLTexture::Linear);
   $texture->setData(image.mirrored(),QOpenGLTexture::DontGenerateMipMaps);

   oldTexture(texture);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::makeColorImage(QImage& image)
{
   QVector3D mix;
   QList<qreal> key=$ramp.keys();                  // que l'on retrouve donc rangees par ordre croissant
   QList<QVector3D> value=$ramp.values();
   int i,j,k,p,count=key.count();
   unsigned int AA=0xFF<<24;
   QVector<qreal> diff(count);                     // "QList" ne supporte pas la "reservation"
   qreal z,r;

   for (p=0; p<count; p++) key[p]*=$thickness;
   for (p=1; p<count; p++) diff[p]=key.at(p)-key.at(p-1);

   for (i=k=0; i<DIM; i++)
      for (j=0; j<DIM; j++,k++) {
         z=$vertexVector.at(k).z();

         for (p=1; p<count && z>key.at(p); p++);
         if (p==count) {
            p--;
            qDebug("Viewer::makeColorImage/1 : z-key.at(p-1)=%f",z-key.at(p));
         }

         r=(key.at(p)-z)/diff.at(p);
         mix=value.at(p-1)*r+value.at(p)*(1.0-r);
         image.setPixel(j,i,AA | qRound(mix.x())<<16 | qRound(mix.y())<<8 | qRound(mix.z()));
      }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::init(uchar* regionData,quint32 width)
{
   static bool start=true;

   QPointF pos=$transform->position();
   int i,j,k,y,x0=int(pos.x()),y0=int(pos.y());
   qreal e=$thickness/qreal($statMax-$statMin);
   uchar* ptr;

   $rho=-300;
   $phi=320;
   $theta=270;
   $scaleZ=100;
   $posx=$posy=(DIM-1)/2;

   for (i=k=0,y=y0; i<DIM; i++,y++) {  // seul z varie (partie constante dans "presetConstVectors"?)
      ptr=regionData+2*(y*width+x0);
      for (j=0; j<DIM; j++,k++,ptr+=2)
         $vertexVector[k].setZ(e*(qreal(*((qint16*)ptr))-$statMin));
   }

   // a refaire en fonction du nouveau terrain
   setNormalVector();
   $gpsPack.second.clear();
   clearSegmPath();
   $stepList.clear();

   $routing->next();                   // reinitialisation des methodes next (avec arguments par defaut)
   $branch->next();

   if (start) start=false;
   else {
      getTexture("color");
      updateBuffer($vertexBuffer,&$vertexVector);
      updateBuffer($normalBuffer,&$normalVector);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::clearHikingBuffer()
{
   $hikingVector.clear();              // a ete teste seul avec succes mais il vaut mieux etre prudent
   $hikingVector<<QVector3D(0,0,0);    // size==0 refuse par Viewer::updateBuffer()

   updateBuffer($hikingBuffer,&$hikingVector,true);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::updateBuffer(QOpenGLBuffer* buffer,QVector<QVector3D>* vector,bool allocate)
{  // default : allocate=false
   if (vector->count()) {
      buffer->bind();
      if (allocate) buffer->allocate(vector->constData(),vector->size()*sizeof(QVector3D));
      buffer->write(0,vector->constData(),vector->size()*sizeof(QVector3D));
      buffer->release();
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::setNormalVector()
{
   QVector3D v0(0,0,0),v1,v2,sum;
   int i,j,k,n;

   $normalVector.fill(v0,DIM*DIM);     // QVector(int size,const T & value)

   // calcul des normales
   for (i=0,n=DIM-1; i<n; i++)
      for (j=0,k=i*DIM; j<n; j++,k++) {
         v1=QVector3D::normal($vertexVector.at(k),$vertexVector.at(k+DIM),$vertexVector.at(k+DIM+1));
         v2=QVector3D::normal($vertexVector.at(k),$vertexVector.at(k+DIM+1),$vertexVector.at(k+1));
         sum=v1+v2;

         $normalVector[k]+=sum;
         $normalVector[k+DIM]+=v1;
         $normalVector[k+DIM+1]+=sum;
         $normalVector[k+1]+=v2;
      }

   // normalisation des normales
   for (i=0,n=DIM*DIM; i<n; i++) $normalVector[i].normalize();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::xmlToFun(const QByteArray& array,const QString& terrainDir,bool keepXmlFile,QWidget* parent)
{
   $nodeMap.clear();
   $wayMap.clear();
   $summitMap.clear();

   HikingThread* hikingThread=new HikingThread($nodeMap,$wayMap,$summitMap,$transform->getRect(),
         array,terrainDir,keepXmlFile,this);

   connect(hikingThread,SIGNAL(operationDone(bool)),parent,SLOT(operationDone(bool)));
   connect(hikingThread,SIGNAL(operationDone(bool)),hikingThread,SLOT(deleteLater()));

   hikingThread->start();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::paintPath()
{
   if (!optionsContains("hiking") || $segmPath.isEmpty()) return;

   glLineWidth(2.0);
   glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
   glBegin(GL_LINES);

   for (int j=0,n=$segmPath.count(); j<n; j++) {
      glColor3f($segmPath.at(j).second,0,1);             // 1 : magenta (couleur dominante), 0 : blue

      const Segm& segm=$segmPath.at(j).first;
      const QList<QPair<qint64,int>>& route=$wayMap.value(segm.wid()).first;
      int i,begin=segm.begin(),end=segm.end(),index1=route.at(begin).second,index2=route.at(end).second;

      if (begin>end) qSwap(index1,index2);

      for (i=index1+index1%2; i<=index2; i++) {
         const QVector3D& p=$hikingVector.at(i);
         glVertex3f(p.x(),p.y(),p.z());
      }
   }

   glEnd();
   glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
   glLineWidth(1.0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::addStepList(const QList<Step>& stepList)
{  // signal en provenance de "$routing" et de "Branch"
   $stepList=stepList;

   str2k buf;
   sprintf(buf,"Viewer::addStepList/1 stepList (%i) : ",$stepList.count());
   for (int j=0,n=$stepList.count(); j<n; j++) concat(buf,"(%lld,%i) ",$stepList.at(j).first,$stepList.at(j).second);
   qDebug("%s",buf);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

uchar Viewer::forkInfo(qint64 prior,qint64 sid,qint64 next,const QPointF& a)
{
   const QList<QPair<qint64,qint16>>& nlist=$nodeMap.value(sid).second;    // liste des ways ou apparaissent "sid"
   QMap<qint64,QPointF> closedMap;

   for (int j=0,n=nlist.count(); j<n; j++) {
      const QList<QPair<qint64,int>>& route=$wayMap.value(nlist.at(j).first).first;
      qint16 index=nlist.at(j).second;

      if (index==0) closedMap.insert(route.at(index+1).first,QPointF());
      else if (index==route.count()-1) closedMap.insert(route.at(index-1).first,QPointF());
      else {
         closedMap.insert(route.at(index+1).first,QPointF());
         closedMap.insert(route.at(index-1).first,QPointF());
      }
   }

   QPolygonF polygon;
   qreal min=-SIDE/2.0;

   for (QMap<qint64,QPointF>::iterator it=closedMap.begin(); it!=closedMap.end(); it++) {
      const QPointF &p=$nodeMap.value(it.key()).first,q((p.y()-min)/a.y(),(p.x()-min)/a.x());
      polygon.append(q);
      it.value()=q;
   }

   static ConvexHull ch;
   ch.getHull(polygon);
   const QPointF &pt=$nodeMap.value(sid).first,o((pt.y()-min)/a.y(),(pt.x()-min)/a.x());

   if (polygon.count()-1!=closedMap.count()) for (QMap<qint64,QPointF>::iterator it=closedMap.begin(); it!=closedMap.end(); it++) {
      if (polygon.contains(it.value())) continue;

      int i;
      for (i=1; i<polygon.count(); i++) {
         // verification des intersections
         QList<QLineF> lineList={QLineF(polygon.at(i-1),it.value()),QLineF(it.value(),polygon.at(i))};
         bool fail=false;

         for (const QLineF& line: lineList) {
            QPointF p=line.p1(),q=line.p2(),r;

            for (int j=0,n=polygon.count()-1; j<n; j++) {
               if (line.intersect(QLineF(o,polygon.at(j)),&r)==QLineF::BoundedIntersection && r!=p && r!=q) {
                  fail=true;
                  break;
               }
            }
            if (fail) break;     // sort de la boucle range-for
         }
         if (fail) continue;

         polygon.insert(i,it.value());
         break;
      }
      if (i==polygon.count()) qFatal("qFatal Window::forkInfo/1 : failing insert %lld node",it.key());
   }

   int priorIndex=polygon.indexOf(closedMap.value(prior)),nextIndex=polygon.indexOf(closedMap.value(next));
   uchar m=polygon.count()-1,info= (priorIndex>nextIndex)? priorIndex-nextIndex : priorIndex+m-nextIndex;

   return ((info & 0xF)<<4 | ((m-1) & 0xF));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::refresh(const QString& arg)
{
   if (arg=="hiking") {
      if (!loadFunFile()) return;                           // erreur ouverture fichier (hautement improbable)
      else updateBuffer($hikingBuffer,&$hikingVector,true); // on est deja en mode "viewer", maj de tous les buffers inutile
   }
   else getTexture(arg);
   update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Viewer::loadFunFile()
{
   QString funFile=$transform->preset()+".fun";

   $hikingVector.clear();
   $nodeMap.clear();
   $wayMap.clear();
   $summitMap.clear();
   clearSegmPath();
   $stepList.clear();

   FunTo3D funTo3D($vertexVector,&$hikingVector);

   if (!funTo3D.loadFunFile($nodeMap,$wayMap,$summitMap,funFile,$transform->getRect(),$coef)) {
      QMessageBox::warning(this,"warning","Viewer::loadFunFile : \nfail opening file\n\""+funFile+"\"");
      return false;
   }

   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::loadGpsFile(bool checked)
{
   static QString lastDir=QString();

   $gpsPack.second.clear();

   if (!checked) {
      clearRouteDlg(1);
      if ($routeDlg) $routeDlg->setTabText(1,"gps file");
      return;
   }

   QString gpsFile=QFileDialog::getOpenFileName(this,"gps file",lastDir,"gps file (*.gpx *.kml)");

   if (gpsFile.isNull()) {             // suite a "esc"
      emit resetOptions({"gps file"});
      return;
   }

   lastDir=gpsFile.section('/',0,-2);

   FunTo3D funTo3D($vertexVector,&$gpsPack.second);
   QString errMsg,ext=gpsFile.section('.',-1).toLower();
   bool (FunTo3D::*method)(const QString&,QList<int>&,const QRectF&,QString&);

   method= (ext=="gpx")? &FunTo3D::loadGpxFile : &FunTo3D::loadKmlFile;

   if (!(funTo3D.*method)(gpsFile,$gpsPack.first,$transform->getRect(),errMsg)) {
      emit resetOptions({"gps file"});
      QMessageBox box(QMessageBox::Warning,"Warning",
            "<center>"+gpsFile.section('/',-1)+" :<br/>"+errMsg+"</center>",QMessageBox::Ok,this);
      box.setTextInteractionFlags(Qt::TextSelectableByMouse);
      box.exec();
      return;
   }

   updateBuffer($gpsBuffer,&$gpsPack.second,true);
   update();
   setDlgGpsCurve(gpsFile.section('/',-1));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::drawCookies(char tabIndex)
{
   static const QPointF p(1.0,STEP/8.0),q(30.0,STEP);
   static const qreal a=(q.y()-p.y())/(q.x()-p.x()),b=(p.y()*q.x()-p.x()*q.y())/(q.x()-p.x());

   double modelview[16],cam[3],radius;

   glGetDoublev(GL_MODELVIEW_MATRIX,modelview);
   GLop::getCamPos(cam,modelview);

   QVector<QVector3D> targetVector;
   QList<QPair<QVector3D,char>> cookieList;
   QVector3D indexData;
   FunTo3D funTo3D($vertexVector,&targetVector);
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

   if (tabIndex==0) {               // hiking
      for (int j=0,n=$stepList.count(); j<n; j++) {
         char color=$stepList.at(j).second;
         if (color==3) continue;    // black (ignore, utile pour la 2D), 1:blue, 2:red (Branch::drawSegmList)

         QPair<qint64,qint16>& pair=$nodeMap[$stepList.at(j).first].second.first();
         cookieList.append(QPair<QVector3D,char>($hikingVector.at($wayMap[pair.first].first.at(pair.second).second),color));
      }
   }
   else {                           // gps
      cookieList<<
         QPair<QVector3D,char>($gpsPack.second.first(),1)<<    // blue (cookie "depart")
         QPair<QVector3D,char>($gpsPack.second.last(),2);      // red (cookie "arrivee")
   }

   if ($routeDlg && ((RouteTab*)$routeDlg->page(tabIndex))->getIndexData(indexData)) {
      int j;
      for (j=cookieList.count()-1; j>=0; j--) if (qFuzzyCompare(cookieList.at(j).first,indexData)) break;

      if (j>=0) cookieList[j].second=3;
      else cookieList.append(QPair<QVector3D,char>(indexData,3));
   }

   for (int j=0,n=cookieList.count(); j<n; j++) {
      targetVector.clear();
      const QVector3D& o=cookieList.at(j).first;
      char color=cookieList.at(j).second;
      // sqrt(...) : distance camera -> vecteur o
      radius=a*sqrt((cam[0]-o.x())*(cam[0]-o.x())+(cam[1]-o.y())*(cam[1]-o.y())+(cam[2]-o.z())*(cam[2]-o.z()))+b;
      funTo3D.getCookie(o.toPointF(),radius);
      float min=1e6,max=-1e6,z;

      for (int i=targetVector.count()-1; i>=0; i--) {
         z=targetVector.at(i).z();
         if (z<min) min=z;
         if (z>max) max=z;
      }

      if (color==1 || color==4) glColor4f(float(0),float(0),float(1),float(0.6));   // blue (cast sinon warning msvc)
      else if (color==3) glColor4f(float(0),float(0),float(0),float(0.6));          // 3 : black
      else glColor4f(float(1),float(0),float(0),float(0.6));                        // red

      glBegin(GL_TRIANGLE_FAN);
      glVertex3f(o.x(),o.y(),qMax(o.z(),float((min+3.0*max)/4.0)));
      for (int i=targetVector.count()-1; i>=0; i--)
            glVertex3f(targetVector.at(i).x(),targetVector.at(i).y(),targetVector.at(i).z());
      glEnd();
   }

   glDisable(GL_BLEND);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::setDlgHikingCurve()
{
   if (!$routeDlg) return;

   if ($segmPath.isEmpty() || $segmPath.at(0).second==0) {
      clearRouteDlg(0);
      return;
   }

   RouteTab* routeTab=(RouteTab*)$routeDlg->page(0);
   QList<qint64> nidList;

   QVector<QVector3D> dlgVector;
   const Segm& segm=$segmPath.at(0).first;
   const QList<QPair<qint64,int>>& route=$wayMap.value(segm.wid()).first;
   dlgVector.append($hikingVector.at(route.at(segm.begin()).second));
   nidList.append(route.at(segm.begin()).first);

   for (int j=0,n=$segmPath.count(); j<n && $segmPath.at(j).second==1; j++) { // tant que color==magenta
      const Segm& segm=$segmPath.at(j).first;
      const QList<QPair<qint64,int>>& route=$wayMap.value(segm.wid()).first;
      int begin=segm.begin(),end=segm.end();

      if (begin<end) for (int i=begin+1; i<=end; i++) {
         nidList.append(route.at(i).first);
         dlgVector.append($hikingVector.at(route.at(i).second));
      }
      else for (int i=begin-1; i>=end; i--) {
         nidList.append(route.at(i).first);
         dlgVector.append($hikingVector.at(route.at(i).second));
      }
   }

   Slippy slippy($transform->getRect());
   QSize imageSize=slippy.getImageSize();
   const QPointF a(SIDE/imageSize.height(),SIDE/imageSize.width());
   QMap<int,quint16> forkMap;
   uchar count=0;

   for (int i=1,m=$stepList.count()-1,j,start=0,n=nidList.count(); i<m; i++) {
      qint64 nid=$stepList.at(i).first;

      if (!$summitMap.contains(nid)) continue;

      for (j=start; j<n && nid!=nidList.at(j); j++);
      if (j==n) qFatal("qFatal Viewer::setDlgHikingCurve/1");

      forkMap.insert(j,(++count)<<8 | forkInfo(nidList.at(j-1),nid,nidList.at(j+1),a));
      start=j+1;
   }

   qDebug("Viewer::setDlgHikingCurve/2 : dlgVector.count=%i  forkMap.count=%i",dlgVector.count(),forkMap.count());
   str2k buf;
   sprintf(buf,"%s","Viewer::setDlgHikingCurve/3  forkMap : ");
   for (QMap<int,quint16>::const_iterator it=forkMap.constBegin(); it!=forkMap.constEnd(); it++) concat(buf,"%i ",it.key());
   qDebug("%s",buf);

   routeTab->setData(dlgVector,forkMap);
   $routeDlg->setCurrentIndex(0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::setDlgGpsCurve(const QString& gpsFile)
{  // default : gpsFile=""
   static QString name="";

   if (!$routeDlg) {
      name=gpsFile;
      return;
   }

   if (!optionsContains("gps file")) {
      clearRouteDlg(1);
      return;
   }

   if (!gpsFile.isEmpty()) name=gpsFile;
   $routeDlg->setTabText(1,name);

   RouteTab* routeTab=(RouteTab*)$routeDlg->page(1);
   QVector<QVector3D> dlgVector;

   for (int i: $gpsPack.first) dlgVector.append($gpsPack.second.at(i));

   routeTab->setData(dlgVector);
   $routeDlg->setCurrentIndex(1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::saveGpsPath()
{
   static QString lastDir=QString();

   if (!optionsContains("hiking") || $hikingVector.size()==0 || $segmPath.isEmpty() || $segmPath.at(0).second==0) {
      QMessageBox::warning(this,"Viewer::saveGpsPath/1","There is no path to save!");
      return;
   }

   QString gpsFile=QFileDialog::getSaveFileName(this,"gps file",lastDir,"gps file (*.gpx *.kml)");
   if (gpsFile.isNull()) return;                   // suite a "esc"
   lastDir=gpsFile.section('/',0,-2);

   QFile file(gpsFile);
   if (!file.open(QIODevice::WriteOnly|QIODevice::Text)) {
      QMessageBox::warning(this,"Viewer::saveGpsPath/2","Unable to open \""+gpsFile+"\" file!");
      return;
   }

   QList<QPointF> saveList;
   const Segm& segm=$segmPath.at(0).first;
   const QList<QPair<qint64,int>>& route=$wayMap.value(segm.wid()).first;
   saveList.append($nodeMap.value(route.at(segm.begin()).first).first);

   for (int j=0,n=$segmPath.count(); j<n && $segmPath.at(j).second==1; j++) { // tant que color==magenta
      const Segm& segm=$segmPath.at(j).first;
      const QList<QPair<qint64,int>>& route=$wayMap.value(segm.wid()).first;
      int begin=segm.begin(),end=segm.end();

      if (begin<end) for (int i=begin+1; i<=end; i++) saveList.append($nodeMap.value(route.at(i).first).first);
      else for (int i=begin-1; i>=end; i--) saveList.append($nodeMap.value(route.at(i).first).first);
   }

   Slippy slippy($transform->getRect());
   QSize imageSize=slippy.getImageSize();
   qreal min=-SIDE/2.0,ax=SIDE/imageSize.height(),ay=SIDE/imageSize.width();

   for (int j=0,n=saveList.count(); j<n; j++) {
      QPointF& p=saveList[j];
      qreal x=(p.y()-min)/ay,y=(p.x()-min)/ax;
      qreal lon=slippy.imageX2Lon(x),lat=slippy.imageY2Lat(y);
      p=QPointF(lon,lat);
   }

   QTextStream stream(&file);
   QString ext=gpsFile.section('.',-1).toLower();  // en absence d'extension Qt rajoute .gpx, sinon ne rajoute rien
   QString name=gpsFile.section('/',-1).section('.',0,0);

   if (ext=="gpx") saveGpxFile(stream,name,saveList);
   else if (ext=="kml") saveKmlFile(stream,name,saveList);
   else qFatal("qFatal Viewer::saveGpsPath/3");

   file.close();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::saveGpxFile(QTextStream& stream,const QString& name,const QList<QPointF>& saveList)
{
   stream<<       // preambule
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"<<
      "<gpx version=\"1.0\" xmlns=\"http://www.topografix.com/GPX/1/0\">\n"<<
      "<trk>\n"<<
      "<name>"<<name<<"</name>\n"<<
      "<trkseg>\n";

   // finalement rien de mieux (et concis) qu'un bon vieux "sprintf"
   for (const QPointF& p: saveList) stream<<QString::asprintf("<trkpt lat=\"%9.7f\" lon=\"%9.7f\"></trkpt>\n",p.y(),p.x());

   stream<<       // conclusion
      "</trkseg>\n"<<
      "</trk>\n"<<
      "</gpx>\n";
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::saveKmlFile(QTextStream& stream,const QString& name,const QList<QPointF>& saveList)
{  // fichier reduit a sa plus simple expression, alors peut-etre manque-t-il des balises (comme "style"?)
   stream<<       // preambule (sans les donnees de style et autres fariboles)
      "<?xml version='1.0' encoding='UTF-8'?>\n"<<
      "<kml xmlns='http://www.opengis.net/kml/2.2'>\n"<<
      "<Document>\n"<<
      "<name>"<<name<<"</name>\n"<<
      "<Placemark>\n"<<
      "<LineString>\n"<<
      "<coordinates>\n";

   // les fichiers kml affichent d'abord la longitude (et ce sans balises ni quotes, d'ou un poids divise par 2)
   for (const QPointF& p: saveList) stream<<QString::asprintf("%9.7f,%9.7f\n",p.x(),p.y());

   stream<<       // conclusion
      "</coordinates>\n"<<
      "</LineString>\n"<<
      "</Placemark>\n"<<
      "</Document>\n"<<
      "</kml>\n";
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::openRouteDlg(bool checked)
{
   if (checked) {                            // on considere que ($routeDlg==0), $posVector : configuration "slopeTab"
      $routeDlg=new RouteDlg($thickness,$transform->getRect(),$transform->pixelScale(),$ramp,$posVector,$statMin,$statMax,this);
      $routeDlg->show();
      $routeDlg->center();

      setDlgHikingCurve();
      setDlgGpsCurve();
   }
   else if ($routeDlg) $routeDlg->close();   // qui entraine ($routeDlg==0)
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::closeRouteDlg()
{  // depuis Window::getBack, et reponse au signal RouteDlg::closeRouteDlg
   if ($routeDlg) {
      delete $routeDlg;                      // et les pages associees
      $routeDlg=0;
      emit resetOptions({"routedlg"});
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Viewer::clearRouteDlg(int pageIndex)
{
   if ($routeDlg) ((RouteTab*)$routeDlg->page(pageIndex))->init("Nothing to draw!");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Viewer::optionsContains(const QString& option)
{
   bool state;

   emit getOptionState(option,state);
   return state;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

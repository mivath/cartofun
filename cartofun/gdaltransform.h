/******************************************************************************
 *
 * Project:  GDAL
 * Purpose:  Command line point transformer.
 * Author:   Frank Warmerdam <warmerdam@pobox.com>
 *
 ******************************************************************************
 * Copyright (c) 2007, Frank Warmerdam <warmerdam@pobox.com>
 * Copyright (c) 2008-2013, Even Rouault <even dot rouault at mines-paris dot org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

#ifndef GDALTRANSFORM_H
#define GDALTRANSFORM_H

#include "gdalwarper.h"
#include "cpl_string.h"
#include "ogr_spatialref.h"

#include "global.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

class GdalTransform
{
   public:
      GdalTransform(int argc,char* argv[]);
      bool convert(str1k szLine,double* dfX,double* dfY,double* dfZ);
      ~GdalTransform();

   private:
      void* $hTransformArg;
      GDALTransformerFunc $pfnTransformer;
      int $bInverse,$nOrder,$nGCPCount;
      GDALDatasetH $hSrcDS,$hDstDS;
      GDAL_GCP* $pasGCPs;

      void Usage(const char* pszErrorMsg=NULL);
      char* SanitizeSRS(const char* pszUserInput);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif

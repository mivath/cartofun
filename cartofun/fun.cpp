/*
 - Date : 08/12/2011
 - Nom : fun.cpp
*/

#include "fun.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

char* concat(char* dest,const char* fmt,...)
{
   va_list argptr;
   str256 str;

   va_start(argptr,fmt);      // Initialize va_ functions
   vsprintf(str,fmt,argptr);  // print string to buffer
   va_end(argptr);            // Close va_ functions
   return strcat(dest,str);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

qreal ConvexHull::distSquared(const QPointF& p,const QPointF& q)
{
   return (p.x()-q.x())*(p.x()-q.x())+(p.y()-q.y())*(p.y()-q.y());
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// To find orientation of ordered triplet (p, q, r). The function returns following values :
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// -1--> Counter-clockwise
char ConvexHull::orientation(const QPointF& p,const QPointF& q,const QPointF& r)
{
   qreal rslt=(r.x()-q.x())*(q.y()-p.y())-(q.x()-p.x())*(r.y()-q.y());

   return (rslt<0.0)? -1 : (rslt==0.0)? 0 : 1;  // counter-clockwise (sens anti-horaire), colinear, clock
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool ConvexHull::lessThan(const QPointF& p1,const QPointF& p2)
{  // Used by library function qsort() to sort an array of points with respect to the first point
   char rslt=orientation($p0,p1,p2);

   if (rslt==0) return (distSquared($p0,p2)>=distSquared($p0,p1));
   return (rslt==-1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QPointF ConvexHull::nextToTop(QStack<QPointF>& stack)
{  // A utility function to find next to top in a stack
   const QPointF &p=stack.pop(),&next=stack.top();
   stack.push(p);
   return next;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QPolygonF& ConvexHull::getHull(QPolygonF& polygon)
{  // origine du code : http://www.geeksforgeeks.org/convex-hull-set-2-graham-scan/
   int min=0,m=polygon.count();
   qreal y,ymin=polygon.at(0).y();

   for (int i=1; i<m; i++) {     // Find the bottommost point
      y=polygon.at(i).y();
      // Pick the bottom-most or chose the left most point in case of tie
      if (y<ymin || (y==ymin && polygon.at(i).x()<polygon.at(min).x())) {
         ymin=y;
         min=i;
      }
   }

   // Place the bottom-most point at first position
   qSwap(polygon[0],polygon[min]);
   // Sort m points with respect to the first point. A point p1 comes before p2 in sorted ouput if p2
   // has larger polar angle (in counterclockwise direction) than p1
   $p0=polygon.at(0);
   // Shell sort (source : Le langage C, Kernighan & Ritchie, page 62)
   for (int i=m/2,j,k; i>0; i/=2)
      for (j=i; j<m; j++)
         for (k=j-i; k>=1 && lessThan(polygon.at(k+i),polygon.at(k)); k-=i) qSwap(polygon[k+i],polygon[k]);

   // Create an empty stack and push first three points to it.
   QStack<QPointF> stack;
   stack.push(polygon.at(0));
   stack.push(polygon.at(1));
   stack.push(polygon.at(2));

   // Process remaining n-3 points
   for (int i=3; i<m; i++) {
      // Keep removing top while the angle formed by points next-to-top, top, and points[i] makes a non-left turn
      while (orientation(nextToTop(stack),stack.top(),polygon.at(i))!=-1) stack.pop();
      stack.push(polygon.at(i));
   }

   // Now stack has the output points
   polygon.clear();
   while (!stack.isEmpty()) polygon.append(stack.pop());
   polygon.append(polygon.at(0));   // fermeture du polygone

   return polygon;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

char* Clip::out(qint64 nid)
{
   static str256 buf;
   const QPointF& p=$nodeMap.value(nid).first;

   sprintf(buf,"%lld(%i,%i)",nid,qRound(p.x()),qRound(p.y()));
   return buf;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Clip::isTip(const QPair<qint64,qint16>& pair)
{
   return (pair.second==0 || pair.second==$wayMap.value(pair.first).first.count()-1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Clip::removeWay(qint64 wid)
{
   const QList<QPair<qint64,int>>& route=$wayMap.value(wid).first;
   const QMap<qint16,qint64>& wmap=$wayMap.value(wid).second;
   QMap<qint16,qint64>::const_iterator it;

   for (int j=0,n=route.count()-1; j<=n; j++) {
      qint64 nid=route.at(j).first;

      if (wmap.contains(j)) {
         if ($summitMap.value(nid).count()==1) {   // suppression d'un sommet
            $summitMap.remove(nid);
            $nodeMap.remove(nid);
         }
         else {
            it=wmap.find(j);
            if (j && !$summitMap[nid].remove(*(it-1))) qFatal("qFatal Clip::removeWay/1 wid=%lld",wid);
            if (j<n && !$summitMap[nid].remove(*(it+1))) qFatal("qFatal Clip::removeWay/2 wid=%lld",wid);

            updateNode(nid,wid);
         }
      }
      else $nodeMap.remove(nid);
   }

   if (!$wayMap.remove(wid)) qFatal("qFatal Clip::removeWay/3 wid=%lld",wid);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Clip::isDoubleLiaison(qint64 sid1,qint64 sid2)
{  // sid1 et sid2 sommets supposes voisins
   const QList<QPair<qint64,qint16>>& nlist=$nodeMap.value(sid1).second;
   int count=0;

   for (int j=nlist.count()-1; j>=0; j--) {
      const QMap<qint16,qint64>& wmap=$wayMap.value(nlist.at(j).first).second;
      qint16 index=nlist.at(j).second;
      QMap<qint16,qint64>::const_iterator it=wmap.find(index);

      if (it==wmap.end()) qFatal("qFatal Clip::isDoubleLiaison/1");
      if (index && *(it-1)==sid2) count++;
      if (it!=wmap.end()-1 && *(it+1)==sid2) count++;
   }
   return (count>1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Clip::updateSummit(qint64 wid,qint16 index,qint64 neighbor)
{  // par "neighbor" comprendre en fait le suivant dans le traitement de Clip::Clip
   qint64 sid=$wayMap.value(wid).second.value(index);

   if (sid==neighbor) {                                     // Coll de Port (N42.191598_E1.508493)
      $summitMap[sid].remove(neighbor);
      QList<QPair<qint64,qint16>>& nlist=$nodeMap[sid].second;
      for (int i=nlist.count()-1; i>=0; i--) if (nlist.at(i)==QPair<qint64,qint16>(wid,index)) {
         nlist.removeAt(i);
         return;
      }
   }

   if (!isDoubleLiaison(sid,neighbor)) {
      $summitMap[sid].remove(neighbor);
      $summitMap[neighbor].remove(sid);
   }

   updateNode(sid,wid);                                     // en mode "remove"

   if ($summitMap.value(sid).isEmpty()) {                   // suppression d'un sommet
      $summitMap.remove(sid);
      $nodeMap.remove(sid);
      return;
   }

   if ($nodeMap.value(sid).second.count()!=1 || isTip($nodeMap.value(sid).second.at(0))) return;   // a+!a.b=a+b

   const QMap<qint64,double>& smap=$summitMap.value(sid);
   const QPair<qint64,qint16>& pair=$nodeMap.value(sid).second.at(0);
   qint64 sidi,sidj,widx=pair.first;
   QMap<qint16,qint64>& wmap=$wayMap[widx].second;
   int count=smap.count();
   double length=0.0;

   if (count==2) {                                          // les Angles (N42.558383_E2.065617)
      const QMap<qint64,double>::const_iterator iti=smap.constBegin(),itj=iti+1;
      sidi=iti.key();
      sidj=itj.key();
      length=(*iti)+(*itj);
   }
   else if (count==1) {
      sidj=neighbor;
      sidi=smap.constBegin().key();

      if (isDoubleLiaison(sid,sidi)) return;                // mas-de-Londres (N43.777651_E3.513417)

      qDebug("Clip::updateSummit/a : wid=w%lld  index=%i  neighbor=s%lld",wid,index,neighbor);

      // depuis ici jusqu'a // <- : encore utile? (initialement reglait le probleme "les Angles" desormait regle plus haut)
      const QList<QPair<qint64,qint16>> &nlistj=$nodeMap.value(sidj).second,&nlisti=$nodeMap.value(sidi).second;
      int j,i;

      for (j=nlistj.count()-1; j>=0; j--) if (nlistj.at(j).first==widx) break;
      if (j<0) qFatal("qFatal Clip::updateSummit/1 : wid=w%lld  index=%i  neighbor=s%lld",wid,index,neighbor);

      for (i=nlisti.count()-1; i>=0; i--) if (nlisti.at(i).first==widx) break;
      if (i<0) qFatal("qFatal Clip::updateSummit/2 : wid=w%lld  index=%i  neighbor=s%lld",wid,index,neighbor);

      qint16 index0=nlistj.at(j).second,index1=nlisti.at(i).second;

      if (index0>index1) qSwap(index0,index1);
      const QList<QPair<qint64,int>>& route=$wayMap.value(widx).first;
      QPointF p=$nodeMap.value(route.at(index0).first).first,q;

      for (j=index0+1; j<=index1; j++,p=q) {
         q=$nodeMap.value(route.at(j).first).first;
         length+=sqrt((p.x()-q.x())*(p.x()-q.x())+(p.y()-q.y())*(p.y()-q.y()));
      }
      // <-
   }
   else qFatal("qFatal Clip::updateSummit/3 : wid=w%lld  index=%i  neighbor=s%lld",wid,index,neighbor);

   $summitMap[sidi].remove(sid);
   $summitMap[sidj].remove(sid);

   if (!$summitMap.value(sidi).contains(sidj)) {
      $summitMap[sidi].insert(sidj,length);
      $summitMap[sidj].insert(sidi,length);
   }
   else $summitMap[sidi][sidj]=$summitMap[sidj][sidi]=qMin(length,$summitMap.value(sidi).value(sidj));

   if (!wmap.remove(pair.second)) qFatal("qFatal Clip::updateSummit/4 : wid=w%lld  index=%i  neighbor=s%lld",wid,index,neighbor);
   $summitMap.remove(sid);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Clip::updateNode(qint64 nid,qint64 wid,qint16 index)
{  // default : qint16 index=-1 (mode "remove")
   QList<QPair<qint64,qint16>>& nlist=$nodeMap[nid].second;

   for (int i=nlist.count()-1; i>=0; i--) if (nlist.at(i).first==wid) {
      if (index==-1) nlist.removeAt(i);
      else nlist[i].second=index;
      return;
   }
   qFatal("qFatal Clip::updateNode/1");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

char Clip::getStart(const QList<qint16>& changeList,const QList<QPair<qint64,int>>& route,qint16& change)
{
   const QPointF &p=$nodeMap.value(route.at(0).first).first,&q=$nodeMap.value(route.last().first).first;
   bool
      flp=(p.x()>0 && p.x()<$imageSize.width() && p.y()>0 && p.y()<$imageSize.height()),
      flq=(q.x()>0 && q.x()<$imageSize.width() && q.y()>0 && q.y()<$imageSize.height());
   int nfo=changeList.count(),max=0,k=0;

   // cas trivial (85%)
   if (nfo==1) {
      change=changeList.at(0);
      return (flp)? 1 : -1;
   }

   // 0 : nfo pair, extemites cachees
   if (!flp && !flq) {
      for (int i=1,d; i<nfo; i+=2) {
         d=changeList.at(i)-changeList.at(i-1);
         if (d>max) {
            max=d;
            k=i;
         }
      }
      change=changeList.at(k);
      return 2;                     // suppression de la fin
   }

   // 2 : nfo impair
   if (flp && !flq) {
      for (int i=0,d; i<nfo; i+=2) {
         d= (i==0)? changeList.at(0) : changeList.at(i)-changeList.at(i-1);
         if (d>max) {
            max=d;
            k=i;
         }
      }
      change=changeList.at(k);
      if (k==0) return 1;           // cas trivial : la section a conserver est au debut
      else return 2;                // suppression de la fin
   }

   // 1 : nfo impair
   if (!flp && flq) {
      for (int i=nfo-1,d; i>=0; i-=2) {
         d= (i==nfo-1)? route.count()-changeList.at(i) : changeList.at(i+1)-changeList.at(i);
         if (d>max) {
            max=d;
            k=i;
         }
      }
      if (k==nfo-1) {               // cas trivial : la section a conserver est a la fin
         change=changeList.at(k);
         return -1;
      }
      else {
         change=changeList.at(k+1);
         return 2;                  // suppression de la fin
      }
   }

   // 3 : nfo pair, extremites visibles (flp && flq)
   for (int i=0,d=0; i<=nfo; i+=2) {
      d= (i==0)? changeList.at(0) : (i==nfo)? route.count()-changeList.at(i-1) : changeList.at(i)-changeList.at(i-1);
      if (d>max) {
         max=d;
         k=i;
      }
   }
   if (k==0) {
      change=changeList.at(0);
      return 1;
   }
   else if (k==nfo) {
      change=changeList.at(nfo-1);
      return -1;
   }
   else {                           // a ce jour jamais realise, malgre les differentes cartes essayees
      change=changeList.at(k);
      return 2;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Clip::mainLoop(QMap<qint64,QList<qint16>>& workMap)
{  // reduction de 13.5% du "poids" des donnees
   QList<qint64> rslt;

   for (QMap<qint64,QList<qint16>>::iterator it=workMap.begin(); it!=workMap.end(); it++) {
      qint64 wid=it.key(),nid,neighbor;
      QList<qint16>& changeList=*it;
      QList<QPair<qint64,int>>& route=$wayMap[wid].first;
      QMap<qint16,qint64>& wmap=$wayMap[wid].second;
      qint16 change;
      QPointF p,q;
      char start;

      do {
         bool isNotSummit=false;
         int lastIndex=route.count()-1;
         double length=0;

         start=getStart(changeList,route,change);

         if (start>0) {                      // suppression de la fin (in==true), cas 1 et 2
            if (change==1 && start==1) {     // et si (start==2)?
               removeWay(wid);
               break;
            }

            for (int i=lastIndex; i>=change; i--) {
               nid=route.at(i).first;
               if ($summitMap.contains(nid)) {        // sommet
                  qint64 prior=*(wmap.find(i)-1);     // avant "wmap.remove(i);", non? (mais marchait aussi apres!)
                  updateSummit(wid,i,prior);
                  wmap.remove(i);
               }
               else $nodeMap.remove(nid);             // nid est un noeud ordinaire

               route.removeLast();
            }

            nid=route.last().first;
            updateNode(nid,wid,route.count()-1);      // ici la valeur d'"index" ne change pas, mais bon...

            if (!$summitMap.contains(nid)) {          // "noeud ordinaire" devient sommet
               isNotSummit=true;
               wmap.insert(route.count()-1,nid);

               qint16 priorIndex=(wmap.find(route.count()-1)-1).key();
               neighbor=wmap.value(priorIndex);
               p=$nodeMap.value(neighbor).first;
                                                      // calcul de la nouvelle longueur
               for (int i=priorIndex+1,m=route.count(); i<m; i++,p=q) {
                  q=$nodeMap.value(route.at(i).first).first;
                  length+=sqrt((p.x()-q.x())*(p.x()-q.x())+(p.y()-q.y())*(p.y()-q.y()));
               }
            }

            // mise a jour de "changeList"
            if (start==2) {
               qint16 last;
               do {
                  last=changeList.last();
                  changeList.removeLast();
               } while (last!=change);    // "last" ne peut etre declare dans la boucle
            }
         }
         else {                           // (start<0) : suppression du debut, cas -1
            if (change==lastIndex) {
               removeWay(wid);
               break;
            }

            for (int i=0; i<change; i++) {
               nid=route.at(0).first;                 // equivalent a : route.first().first
               if ($summitMap.contains(nid)) {        // sommet
                  qint64 next=*(wmap.find(i)+1);      // autre idee : *(wmap.begin()+1) ? (avant ligne suivante)
                  updateSummit(wid,i,next);
               }
               else $nodeMap.remove(nid);             // nid est un noeud ordinaire

               route.removeFirst();
            }

            nid=route.at(0).first;
            updateNode(nid,wid,0);                    // devient sommet (si ne l'etait pas deja!)
            wmap.clear();
            wmap[0]=nid;

            for (int i=1,m=route.count(); i<m; i++) { // decalage de tous les nodes restant
               updateNode(route.at(i).first,wid,i);
               if ($summitMap.contains(route.at(i).first)) wmap.insert(i,route.at(i).first);
            }

            if (!$summitMap.contains(nid)) {
               isNotSummit=true;
               qint16 nextIndex=(wmap.begin()+1).key();
               neighbor=wmap.value(nextIndex);
               p=$nodeMap.value(nid).first;

               for (int i=1; i<=nextIndex; i++,p=q) {
                  q=$nodeMap.value(route.at(i).first).first;
                  length+=sqrt((p.x()-q.x())*(p.x()-q.x())+(p.y()-q.y())*(p.y()-q.y()));
               }
            }
         }
         // retour au tronc commun
         if (isNotSummit) {
            $summitMap.insert(nid,QMap<qint64,double>());
            $summitMap[nid].insert(neighbor,length);
            $summitMap[neighbor].insert(nid,length);
         }
         // mise a jour a l'interieur de la boucle "do-while" a cause de "removeWay" qui en sort
         if (start!=2) rslt.append(wid);
      } while (start==2);
   }  // fin de la boucle "for"
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Clip::Clip(NodeMapType& nodeMap,WayMapType& wayMap,SummitMapType& summitMap,const QSize& imageSize) :
      $nodeMap(nodeMap),$wayMap(wayMap),$summitMap(summitMap),$imageSize(imageSize)
{
   QSet<qint64> outNodeSet;   // nodes en dehors de la carte (QSet::contains|insert?  bien plus rapide qu'avec QList)
   QMap<qint64,QList<qint16>> workMap;

   for (NodeMapType::const_iterator it=$nodeMap.constBegin(); it!=$nodeMap.constEnd(); it++) {
      const QPointF& p=it->first;
      if (p.x()>0.0 && p.x()<$imageSize.width() && p.y()>0.0 && p.y()<$imageSize.height()) continue;
      outNodeSet.insert(it.key());

      if ($summitMap.contains(it.key())) {
         const QList<QPair<qint64,qint16>>& nlist=it->second;
         for (int j=nlist.count()-1; j>=0; j--) workMap.insert(nlist.at(j).first,QList<qint16>());
      }
      else workMap.insert(it->second.at(0).first,QList<qint16>());
   }

   for (QMap<qint64,QList<qint16>>::iterator it=workMap.begin(); it!=workMap.end(); it++) {
      const QList<QPair<qint64,int>>& route=$wayMap.value(it.key()).first;
      bool oldState=outNodeSet.contains(route.at(0).first);

      for (int j=1,n=route.count(); j<n; j++) {
         bool state=outNodeSet.contains(route.at(j).first);

         if (state!=oldState) {
            oldState=state;
            it->append(j);
         }
      }

      if (it->isEmpty()) qFatal("qFatal Clip::Clip/1");  // cas '0' (way complet hors carte) en principe ecarte par overPass
   }

   mainLoop(workMap);
   check();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Clip::check()
{  // suite au bug dit du "coll_de_port", <node id="1962974607" lat="42.1889426" lon="1.5957765"/>
   // qui se manifeste par l'existence dans $summitMap de sommets "zombies" effaces dans $nodeMap
   for (SummitMapType::const_iterator itj=$summitMap.constBegin(); itj!=$summitMap.constEnd(); itj++) {
      const QMap<qint64,double>& smap=itj.value();

      if (!$nodeMap.contains(itj.key())) qFatal("qFatal Clip::check/1 : zombie=%lld",itj.key());

      for (QMap<qint64,double>::const_iterator iti=smap.constBegin(); iti!=smap.constEnd(); iti++)
         if (!$nodeMap.contains(iti.key())) qFatal("qFatal Clip::check/2 : key=%lld  zombie=%lld",itj.key(),iti.key());
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

char* XmlToFun::out(qint64 nid)
{
   static str256 buf;
   const QPointF& p=$nodeMap.value(nid).first;

   QSize imageSize=$slippy.getImageSize();
   qreal min=-SIDE/2.0, ax=SIDE/imageSize.height(), ay=SIDE/imageSize.width(), x=ax*p.y()+min, y=ay*p.x()+min;

   sprintf(buf,"%lld(%6.2f,%6.2f)",nid,y,x);
   return buf;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::save(const QString& saveFile)
{
   QFile file(saveFile);
   if (!file.open(QIODevice::WriteOnly)) qFatal("qFatal XmlToFun::save/1");
   QDataStream stream(&file);       // we will serialize the data into the file
   stream << $nodeMap;              // stream << $nodeMap << $wayMap << $summitMap;    // refuse (types differents?)
   stream << $wayMap;
   stream << $summitMap;
   file.close();
   qDebug("XmlToFun::save/2 : \"%s\" ok!",qPrintable(saveFile));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

int XmlToFun::indexOf(qint64 wid,qint64 nid)
{  // en cas de node present plusieurs fois dans la meme route, renvoie sa 1ere position
   const QList<QPair<qint64,int>>& route=$wayMap.value(wid).first;

   for (int j=0,n=route.count(); j<n; j++) if (route.at(j).first==nid) return j;
   return -1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool XmlToFun::isTip(const QPair<qint64,qint16>& pair)
{
   return (pair.second==0 || pair.second==$wayMap.value(pair.first).first.count()-1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::fetchNodes(const QDomDocument& doc)
{
   const QDomNodeList& nodeList=doc.elementsByTagName("node");

   for (int j=0,n=nodeList.count(); j<n; j++) {          // insertion de tous les nodes
      const QDomElement& element=nodeList.at(j).toElement();
      $nodeMap.insert(
         element.attribute("id").toLongLong(),           // key
         QPair<QPointF,QList<QPair<qint64,qint16>>>(     // value
            QPointF(
               $slippy.lon2ImageX(element.attribute("lon").toDouble()),
               $slippy.lat2ImageY(element.attribute("lat").toDouble())
            ),
            QList<QPair<qint64,qint16>>()                // first : id du way, second : index du node
         )
      );
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::fetchWays(const QDomDocument& doc)
{
   const QDomNodeList& wayList=doc.elementsByTagName("way");

   // parcours de tous les way
   for (int j=0,n=wayList.count(); j<n; j++) {
      const QDomElement& element=wayList.at(j).toElement();
      qint64 id=element.attribute("id").toLongLong();
      const QDomNodeList& ndList=element.elementsByTagName("nd");

      $wayMap.insert(id,QPair<QList<QPair<qint64,int>>,QMap<qint16,qint64>>());
      QList<QPair<qint64,int>>& route=$wayMap[id].first;

      for (int i=0,m=ndList.count(); i<m; i++) {
         qint64 ref=ndList.at(i).toElement().attribute("ref").toLongLong();

         route.append(QPair<qint64,int>(ref,0));
         $nodeMap[ref].second.append(QPair<qint64,qint16>(id,i));
      }
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::fetchSummits()
{  // (re)parcours de tous les node pour en extraire les sommets
   for (NodeMapType::const_iterator it=$nodeMap.constBegin(); it!=$nodeMap.constEnd(); it++) {
      const QList<QPair<qint64,qint16>>& nlist=(*it).second;   // raccourci pour : it.value().second
      int n=nlist.count();

      if (!(n==1 && !isTip(nlist.at(0)))) {                    // le noeud est un sommet
         $summitMap.insert(it.key(),QMap<qint64,double>());
         for (int j=0; j<n; j++) $wayMap[nlist.at(j).first].second.insert(nlist.at(j).second,it.key());
      }
   }

   // constitution de la liste, pour chaque sommet, de ses sommets voisins
   for (SummitMapType::iterator itj=$summitMap.begin(); itj!=$summitMap.end(); itj++) {
      const QList<QPair<qint64,qint16>>& nlist=$nodeMap.value(itj.key()).second;
      for (int j=0,n=nlist.count(); j<n; j++) {
         qint64 wid=nlist.at(j).first;
         qint16 index=nlist.at(j).second;
         QMap<qint16,qint64>::const_iterator iti=$wayMap.value(wid).second.find(index);   // positionnement index

         if (isTip(nlist.at(j))) {                             // extremites
            if (index) fetchLength(itj,wid,iti,-1);            // fin
            else fetchLength(itj,wid,iti,1);                   // debut
         }
         else {                                                // cas "ordinaire"
            fetchLength(itj,wid,iti,-1);
            fetchLength(itj,wid,iti,1);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::fetchLength(SummitMapType::iterator& itj,qint64 wid,const QMap<qint16,qint64>::const_iterator& it1,char add)
{
   const QMap<qint16,qint64>::const_iterator& it2=it1+add;

   if (*it1>*it2) {
      if (!(*itj).contains(*it2)) (*itj).insert(*it2,$summitMap.value(*it2).value(*it1));
      return;
   }

   qint16 index1,index2;

   if (it1.key()>it2.key()) {
      index1=it2.key();
      index2=it1.key();
   }
   else {
      index1=it1.key();
      index2=it2.key();
   }

   const QList<QPair<qint64,int>>& route=$wayMap.value(wid).first;
   QPointF *p=&$nodeMap[route.at(index1).first].first,*q;
   double length=0.0;

   for (qint16 i=index1+1; i<=index2; i++,p=q) {
      q=&$nodeMap[route.at(i).first].first;
      length+=sqrt((p->x()-q->x())*(p->x()-q->x())+(p->y()-q->y())*(p->y()-q->y()));
   }

   if (!(*itj).contains(*it2)) (*itj).insert(*it2,length);
   else if (length<(*itj).value(*it2)) (*itj).insert(*it2,length);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::adjustPetrovskLoop()
{  // concerne les sommets a la fois debut et fin d'un way et n'appartenant qu'a un seul way (boucle)
   QList<qint64> toRemove;

   for (SummitMapType::iterator itj=$summitMap.begin(); itj!=$summitMap.end(); itj++) {
      qint64 sid0=itj.key();
      QList<QPair<qint64,qint16>>& nlist0=$nodeMap[sid0].second;
      if (!(nlist0.count()==2 && nlist0.at(0).first==nlist0.at(1).first && isTip(nlist0.at(0)) && isTip(nlist0.at(1)))) continue;
      if (itj->count()==1 && itj->begin().key()==sid0) {
         qDebug("XmlToFun::adjustPetrovskLoop/1 %s",out(sid0));
         continue;
      }

      qint64 wid=nlist0.at(0).first;
      QList<QPair<qint64,int>>& route=$wayMap[wid].first,newRoute;
      QMap<qint16,qint64>& wmap=$wayMap[wid].second,newWmap;

      qint64 sid1=*wmap.upperBound(0);
      int n=route.count(),index1=wmap.upperBound(0).key(),i;
      for (int j=0,m=n-1,k=index1; j<=m; j++,k=(k+1)%m) newRoute.append(route.at(k));

      newWmap.insert(0,sid1);
      newWmap.insert(n-1,sid1);
      for (QMap<qint16,qint64>::iterator iti=wmap.begin(); iti!=wmap.end(); iti++)
            if (*iti!=sid0 && *iti!=sid1) newWmap.insert(iti.key()-index1,*iti);

      // $nodeMap
      nlist0.clear();
      nlist0.append(QPair<qint64,qint16>(wid,n-1-index1));

      QList<QPair<qint64,qint16>>& nlist1=$nodeMap[sid1].second;
      for (i=nlist1.count()-1; i>=0; i--) if (nlist1.at(i).first==wid) break;
      if (i<0) qFatal("qFatal XmlToFun::adjustPetrovskLoop/2");
      nlist1[i]=QPair<qint64,qint16>(wid,0);
      nlist1.append(QPair<qint64,qint16>(wid,n-1));

      for (int j=0; j<n; j++) {
         qint64 node=newRoute.at(j).first;
         if (node==sid0 || node==sid1) continue;
         QList<QPair<qint64,qint16>>& nlist=$nodeMap[node].second;
         for (i=nlist.count()-1; i>=0; i--) if (nlist.at(i).first==wid) break;
         if (i<0) qFatal("qFatal XmlToFun::adjustPetrovskLoop/3");
         nlist[i]=QPair<qint64,qint16>(wid,j);
      }

      // $summitMap
      double length=0.0;
      if (itj->count()==2) {  // seul ce cas est connu
         qint64 sid2;
         for (QMap<qint64,double>::iterator iti=itj->begin(); iti!=itj->end(); iti++) if (iti.key()!=sid1) {
            sid2=iti.key();                                          // voila le 2eme voisin
            break;
         }

         int index2=newRoute.indexOf(QPair<qint64,int>(sid2,0));     // 3, en principe
         QPointF *p=&$nodeMap[newRoute.at(index2).first].first,*q;

         for (int j=index2+1; j<n; j++,p=q) {                        // from "XmlToFun::fetchLength",
            q=&$nodeMap[newRoute.at(j).first].first;                 // calcul de la longueur sid2->sid1
            length+=sqrt((p->x()-q->x())*(p->x()-q->x())+(p->y()-q->y())*(p->y()-q->y()));
         }

         if (!$summitMap.value(sid1).contains(sid2)|| length<$summitMap.value(sid1).value(sid2)) {
            $summitMap[sid1].insert(sid2,length);
            $summitMap[sid2].insert(sid1,length);
         }
         $summitMap[sid1].remove(sid0);
         $summitMap[sid2].remove(sid0);
      }
      else {                  // 1 seul voisin different de lui-meme (cas traite en "aveugle")
         QPointF *p=&$nodeMap[newRoute.at(0).first].first,*q;

         for (int j=1; j<n; j++,p=q) {
            q=&$nodeMap[newRoute.at(j).first].first;
            length+=sqrt((p->x()-q->x())*(p->x()-q->x())+(p->y()-q->y())*(p->y()-q->y()));
         }
         $summitMap[sid1].insert(sid1,length);
         $summitMap[sid1].remove(sid0);
      }
      // pour plus de surete, suppression en dehors de la boucle principale "itj"
      toRemove.append(sid0);
      // $wayMap : on remplace tout!
      route=newRoute;
      wmap=newWmap;

      qDebug("XmlToFun::adjustPetrovskLoop/4 : summit %s sucessfully removed (%i neighbors)",out(sid0),itj->count());
   }

   for (int j=toRemove.count()-1; j>=0; j--) $summitMap.remove(toRemove.at(j));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::removeNests(int& nestCount,qreal side,int max)
{  // nest : petit reseau isole (souvent un chemin entourant une propriete ou une entreprise)
   // side=40, max=5 utilise seulement dans XmlToFun::recursiv, nombre maxi de ways tolere
   QSet<qint64> toRemove;

   for (SummitMapType::const_iterator itj=$summitMap.constBegin(); itj!=$summitMap.constEnd(); itj++) {
      qint64 key=itj.key();
      // en partant ici d'une extremite...
      if ((*itj).count()!=1 || toRemove.contains(key) || (*itj).begin().key()==key) continue;

      QSet<qint64> summit,way;
      if (!recursiv(key,summit,way,max)) continue;

      qreal left=1e6,right=-1e6,top=1e6,bottom=-1e6;

      for (QSet<qint64>::const_iterator iti=way.constBegin(); iti!=way.constEnd(); iti++) {
         const QList<QPair<qint64,int>>& route=$wayMap.value(*iti).first;
         for (int i=route.count()-1; i>=0; i--) {
            const QPointF& p=$nodeMap.value(route.at(i).first).first;

            if (p.x()<left) left=p.x();
            if (p.x()>right) right=p.x();
            if (p.y()<top) top=p.y();
            if (p.y()>bottom) bottom=p.y();
         }
      }
      if ((right-left)>side || (bottom-top)>side) continue;

      for (QSet<qint64>::const_iterator iti=way.constBegin(); iti!=way.constEnd(); iti++) {
         const QList<QPair<qint64,int>>& route=$wayMap.value(*iti).first;
         for (int i=route.count()-1; i>=0; i--) $nodeMap.remove(route.at(i).first);
         $wayMap.remove(*iti);
      }
      nestCount++;

      for (QSet<qint64>::const_iterator iti=summit.constBegin(); iti!=summit.constEnd(); iti++)
         toRemove.insert(*iti);
   }

   for (QSet<qint64>::const_iterator itj=toRemove.constBegin(); itj!=toRemove.constEnd(); itj++)
         $summitMap.remove(*itj);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool XmlToFun::recursiv(qint64 sid,QSet<qint64>& summit,QSet<qint64>& way,int max)
{
   const QList<QPair<qint64,qint16>>& nlist=$nodeMap.value(sid).second;

   for (int j=nlist.count()-1; j>=0; j--) way.insert(nlist.at(j).first);
   if (way.count()>max) return false;

   summit.insert(sid);
   const QMap<qint64,double>& smap=$summitMap.value(sid);

   for (QMap<qint64,double>::const_iterator it=smap.constBegin(); it!=smap.constEnd(); it++)
      if (!summit.contains(it.key()) && !recursiv(it.key(),summit,way,max)) return false;

   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::logNode(qint64 nid,char flag)
{  // methode inutilisee, mais pourrait quand meme servir...
   if (!$nodeMap.contains(nid)) {
      qDebug("XmlToFun::logNode/0 : %lld is not a valid node",nid);
      return;
   }

   const QList<QPair<qint64,qint16>>& nlist=$nodeMap.value(nid).second;

   qDebug("XmlToFun::logNode/%i : %s",flag,out(nid));

   str1k buf;
   sprintf(buf,"%s","Ways : ");           // initialise buf
   for (int j=0,n=nlist.count(); j<n; j++) concat(buf,"%lld  ",nlist.at(j).first);
   qDebug("%s",buf);

   sprintf(buf,"%s","Nearby summits : "); // reinitialise buf
   if ($summitMap.contains(nid)) {
      const QMap<qint64,double>& smap=$summitMap.value(nid);

      for (QMap<qint64,double>::const_iterator it=smap.constBegin(); it!=smap.constEnd(); it++) concat(buf,"%s  ",out(it.key()));

      qDebug("%s",buf);
   }
   else qDebug("%lld is not a summit",nid);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

qreal XmlToFun::dist(const QPointF& p,const QPointF& q)
{
   return sqrt((p.x()-q.x())*(p.x()-q.x())+(p.y()-q.y())*(p.y()-q.y()));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool XmlToFun::isValid(qint64 sid1,qint64 sid2,const QMap<qint64,QRectF>& boundMap)
{
   static ConvexHull ch;

   const QPair<qint64,qint16> &pair1=$nodeMap.value(sid1).second.at(0),&pair2=$nodeMap.value(sid2).second.at(0);
   qint64   // identification des plus proches voisins
      nid1=$wayMap.value(pair1.first).first.at((pair1.second)? pair1.second-1 : 1).first,
      nid2=$wayMap.value(pair2.first).first.at((pair2.second)? pair2.second-1 : 1).first;
   const QPointF
      &p1=$nodeMap.value(sid1).first,&p2=$nodeMap.value(sid2).first,
      &q1=$nodeMap.value(nid1).first,&q2=$nodeMap.value(nid2).first,mid=(p1+p2)/2.0;
   QPolygonF polygon=QPolygonF()<<p1<<p2<<q2<<q1;        // et si des cotes se croisent, on s'en fout...
   qreal x=mid.x(),y=mid.y(),dd,min=1e10;                // C++11 "QPolygonF polygon={p1,p2,q2,q1};" refuse
   qint64 nid,key;

   // recherche du node le plus proche "a vol d'oiseau" de "mid"
   for (QMap<qint64,QRectF>::const_iterator it=boundMap.constBegin(); it!=boundMap.constEnd(); it++) {
      if (!it->contains(mid)) continue;

      const QList<QPair<qint64,int>>& route=$wayMap.value(it.key()).first;

      for (int j=route.count()-1; j>=0; j--) {
         const QPointF& p=$nodeMap.value(route.at(j).first).first;
         dd=(p.x()-x)*(p.x()-x)+(p.y()-y)*(p.y()-y);     // extraction racine inutile
         if (dd<min) {
            key=route.at(j).first;
            if (key==sid1 || key==sid2 || key==nid1 || key==nid2) continue;
            min=dd;
            nid=key;
         }
      }
   }

   return (min==1e10)? true : (!ch.getHull(polygon).containsPoint($nodeMap.value(nid).first,Qt::OddEvenFill));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::makeEndsMeet(const QList<QPair<qint64,qint64>>& workList)
{  // ou "joindre les 2 bouts" en francais dialectal
   for (int j=0,n=workList.count(); j<n; j++) {
      qint64 sid1=workList.at(j).first,sid2=workList.at(j).second;

      // $summitMap (en commencant par les maps de "haut" niveau)
      QMap<qint64,double>::const_iterator it=$summitMap.value(sid1).constBegin();
      QMap<qint64,double> &smap0=$summitMap[it.key()],&smap2=$summitMap[sid2];
      double newDistance=(*it)+dist($nodeMap.value(sid1).first,$nodeMap.value(sid2).first);
      // sid2 acquiert un nouveau voisin sid0
      smap2.insert(it.key(),newDistance);
      // sid0 acquiert un nouveau voisin sid2
      smap0.insert(sid2,newDistance);
      // sid0 perd son voisin sid1
      smap0.remove(sid1);
      // suppression de sid1 qui n'est plus sommet
      $summitMap.remove(sid1);

      // $wayMap
      qint64 wid1=$nodeMap.value(sid1).second.at(0).first;
      QList<QPair<qint64,int>>& route=$wayMap[wid1].first;
      QMap<qint16,qint64>& wmap=$wayMap[wid1].second;

      if ($nodeMap.value(sid1).second.at(0).second) { // sid1 est le dernier element du way
         qint16 index=route.count()-1;
         // la liste accepte a sa fin un nouvel element sid2
         route.append(QPair<qint64,int>(sid2,0));
         // la wmap (qui ne concerne que les sommets) perd son dernier element sid1
         wmap.remove(index);
         // que l'on remplace par l'element sid2
         wmap.insert(index+1,sid2);

         // $nodeMap
         // sid1 n'est plus une extremite (son "w" devient positif)
         $nodeMap[sid1].second[0].first=wid1;
         // sid2 appartient desormais a un nouveau chemin dont il est extremite
         $nodeMap[sid2].second.append(QPair<qint64,qint16>(wid1,index+1));
      }
      else {                                 // sid1 est le premier element du way
         // dans la liste sid2 devient le nouveau premier element
         route.prepend(QPair<qint64,int>(sid2,0));
         // dans la wmap tous les index des sommets sont decales d'un cran, il semble plus sage de la
         // refaire en repartant de zero (puisque $summitMap est a jour)
         wmap.clear();
         for (qint16 i=0,m=route.count(); i<m; i++)
            if ($summitMap.contains(route.at(i).first)) wmap.insert(i,route.at(i).first);

         // $nodeMap
         // sid1 n'est plus une extremite (son "w" devient positif et son index 1)
         $nodeMap[sid1].second[0]=QPair<qint64,qint16>(wid1,1);
         // sid2 appartient desormais a un nouveau way wid1 dont il est le premier element
         $nodeMap[sid2].second.append(QPair<qint64,qint16>(wid1,0));

         for (qint16 i=route.count()-1; i>1; i--) {   // i>1 : pour (i==0) et (i==1) c'est deja en place
            QList<QPair<qint64,qint16>>& nlist=$nodeMap[route.at(i).first].second;

            for (int k=nlist.count()-1; k>=0; k--) if (nlist.at(k).first==wid1) nlist[k].second++;
         }                                         // note1 : autre syntaxe possible : nlist[k].second=i;
      }                                            // note2 : apres la mise a jour, faire une break?
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::repairBreaks()
{  // reparation des intersections avec les routes
   const qreal threshold=4.0*4.0;   // max?, 4.0 : le diametre d'un point noir

   QList<QPair<qint64,qint64>> workList;
   QMap<qint64,QPointF> endingMap;  // derive de $summitMap (conversion QMap->QHash sans grand interet)
   QMap<qint64,QRectF> boundMap;    // derive de $wayMap
   QList<qint64> sidpList,sidqList;

   for (WayMapType::const_iterator it=$wayMap.constBegin(); it!=$wayMap.constEnd(); it++) {
      qreal minx=1e10,miny=1e10,maxx=-1e10,maxy=-1e10;
      const QList<QPair<qint64,int>>& route=it->first;

      for (int j=route.count()-1; j>=0; j--) {
         const QPointF& p=$nodeMap.value(route.at(j).first).first;
         if (p.x()<minx) minx=p.x();
         if (p.x()>maxx) maxx=p.x();
         if (p.y()<miny) miny=p.y();
         if (p.y()>maxy) maxy=p.y();
      }
      boundMap.insert(it.key(),QRectF(QPointF(minx,miny),QPointF(maxx,maxy)));// topLeft, bottomRight
   }

   for (SummitMapType::const_iterator it=$summitMap.constBegin(); it!=$summitMap.constEnd(); it++)
      if (it->count()==1) endingMap.insert(it.key(),$nodeMap.value(it.key()).first);

   for (QMap<qint64,QPointF>::const_iterator itj=endingMap.constBegin(); itj!=endingMap.constEnd()-1; itj++) {
      qreal px=itj->x(),py=itj->y();
      for (QMap<qint64,QPointF>::const_iterator iti=itj+1; iti!=endingMap.constEnd(); iti++) {
         // calcul sur le carre de la distance suffisant (temps de calcul divise a peu pres par 2)
         if (((px-iti->x())*(px-iti->x())+(py-iti->y())*(py-iti->y()))>threshold) continue;
         sidpList.append(itj.key());
         sidqList.append(iti.key());
      }
   }

   QList<qint64> chain;
   AStar astar($nodeMap,$summitMap,chain,QPointF(1.0,1.0));

   for (int j=0,n=sidpList.count(); j<n; j++) {
      qint64 sidp=sidpList.at(j),sidq=sidqList.at(j);
      // unicites requises (frequent)
      if (sidpList.count(sidp)+sidqList.count(sidp)>1 || sidpList.count(sidq)+sidqList.count(sidq)>1) continue;

      // unique voisin en commun (frequent)
      if ($summitMap.value(sidp).begin().key()==$summitMap.value(sidq).begin().key()) continue;

      // phenomene de boucle (plausible?, deja constate il me semble...)
      if ($nodeMap.value(sidp).second.count()>1 || $nodeMap.value(sidq).second.count()>1) continue;

      // appartenance au meme way (plausible?, a priori oui mais jamais constate)
      if ($nodeMap.value(sidp).second.at(0).first==$nodeMap.value(sidq).second.at(0).first) continue;

      // ecarte les combinaisons en relation dont la "chain" comporte entre 2 et 6 sommets extremites comprises (frequent)
      if (!astar.approve(sidp,sidq)) continue;

      // detection de traces situes entre sidp et sidq (rare mais deja constate)
      if (!isValid(sidp,sidq,boundMap)) continue;

      workList.append(QPair<qint64,qint64>(sidp,sidq));
   }

   makeEndsMeet(workList);

   qDebug("XmlToFun::repairBreaks : endingMap.count=%i  sidpList.count=%i  workList.count=%i",
         endingMap.count(),sidpList.count(),workList.count());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void XmlToFun::analyze(const QDomDocument& doc,QTime& time,const QString& baseName)
{
   int t1=time.restart();        // lecture fichier, analyse "xml"

   fetchNodes(doc);
   fetchWays(doc);
   fetchSummits();
   int t2=time.restart();        // transfert des donnees dans les differentes tables de Qterrain

   int nestCount=0,nodeCount=$nodeMap.count(),summitCount=$summitMap.count(),wayCount=$wayMap.count();

   adjustPetrovskLoop();
   Clip($nodeMap,$wayMap,$summitMap,$slippy.getImageSize());
   removeNests(nestCount,40,5);  // max=5, avec (1<=max && max<=6)
   int t3=time.restart();        // correction & suppression des elements indesirables

   // origine de ce test : plantage du programme avec le fichier N18.161932_E42.259056.xml (4Ko, Arabie Saoudite)
   if (!$nodeMap.isEmpty()) repairBreaks();
   int t4=time.elapsed();        // correction des discontinuites, consommatrice d'un temps non negligeable

   qDebug("XmlToFun::analyze/1 : deletion %i nodes  %i ways  %i summits  %i local systems",
         nodeCount-$nodeMap.count(),wayCount-$wayMap.count(),summitCount-$summitMap.count(),nestCount);
   qDebug("XmlToFun::analyze/2 : $nodeMap.count=%i  $wayMap.count=%i  $summitMap.count=%i",
         $nodeMap.count(),$wayMap.count(),$summitMap.count());
   qDebug("XmlToFun::analyze/3 : t1=%ims  t2=%ims  t3=%ims  t4=%ims  total=%ims",t1,t2,t3,t4,t1+t2+t3+t4);

   if (!$nodeMap.isEmpty()) save(baseName+".fun");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

XmlToFun::XmlToFun(NodeMapType& nodeMap,WayMapType& wayMap,SummitMapType& summitMap,const QRectF& rect,
      const QByteArray& array,const QString& terrainDir,bool keepXmlFile) :
   $nodeMap(nodeMap),$wayMap(wayMap),$summitMap(summitMap)
{
   QTime time;
   time.start();

   QDomDocument doc=QDomDocument("root");
   QString baseName,errorMsg;

   baseName.sprintf("%s%c%8.6f_%c%8.6f",qPrintable(terrainDir),(rect.bottomRight().y()>=0)? 'N' : 'S',
         qAbs(rect.bottomRight().y()),(rect.topLeft().x()>=0)? 'E' : 'W',qAbs(rect.topLeft().x()));
   QFile file(baseName+".xml");

   if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) qFatal("qFatal XmlToFun::XmlToFun/1");
   QTextStream stream(&file);
   stream << array;     // difference entre le .xml genere et celui recupere dans une page
   file.close();        // html : la fin des lignes, ici CRLF au lieu de LF avec l'autre methode!
   if (!doc.setContent(array,&errorMsg)) qFatal("qFatal XmlToFun::XmlToFun/2 : errorMsg=%s",qPrintable(errorMsg));

   $slippy.init(rect);

   analyze(doc,time,baseName);

   if (!keepXmlFile) {  // cas usuel, l'analyse ayant reussi, on peut supprimer le fichier xml
      if (!QFile::remove(baseName+".xml")) qFatal("qFatal XmlToFun::XmlToFun/3");
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

FunTo3D::FunTo3D(QVector<QVector3D>& vertexVector,QVector<QVector3D>* targetVector) :
      $vertexVector(vertexVector),$targetVector(targetVector) {}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool FunTo3D::loadFunFile(NodeMapType& nodeMap,WayMapType& wayMap,SummitMapType& summitMap,const QString& funFile,
      const QRectF& rect,QPointF& coef)
{
   QFile file(funFile);
   if (!file.open(QIODevice::ReadOnly)) return false;

   QDataStream stream(&file);
   stream >> nodeMap;         // ensuite modifie
   stream >> wayMap;          // ensuite modifie
   stream >> summitMap;       // inchange
   file.close();

   Slippy slippy(rect);
   QSize imageSize=slippy.getImageSize();
   qreal min=-SIDE/2.0,ax=SIDE/imageSize.height(),ay=SIDE/imageSize.width(),x,y;
   QVector3D p;

   for (NodeMapType::iterator it=nodeMap.begin(); it!=nodeMap.end(); it++) {
      x=ax*it->first.y()+min;
      y=ay*it->first.x()+min;
      it->first=QPointF(x,y);
   }

   for (WayMapType::iterator it=wayMap.begin(); it!=wayMap.end(); it++) {
      QList<QPair<qint64,int>>& route=it->first;

      for (int i=0,m=route.count(); i<m; i++) {
         const QPointF& point=nodeMap.value(route.at(i).first).first;

         if ($targetVector->count()%2) qFatal("qFatal FunTo3D::loadFunFile/1");

         if (i==0) {
            p=QVector3D(point);
            route[0].second=$targetVector->count();   // pair
         }
         else {                                       // cas ordinaire, traitement entre 2 nodes consecututifs
            addSegment(p,QVector3D(point));           // c'est la qu'est "renseigne" $targetVector
            route[i].second=$targetVector->count()-1; // impair
         }
      }
   }
   coef=QPointF(ax*ax,ay*ay);                         // destine a la classe AStar via la classe Routing
   qDebug("FunTo3D::loadFunFile/2 : $targetVector->count=%i  nodeMap.count=%i  wayMap.count=%i  summitMap.count=%i",
         $targetVector->count(),nodeMap.count(),wayMap.count(),summitMap.count());

   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool FunTo3D::loadGpxFile(const QString& gpxFile,QList<int>& indexList,const QRectF& rect,QString& errMsg)
{  // appel depuis "Viewer::openGpsFile"
   QFile file(gpxFile);
   if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
      errMsg=QString::fromLocal8Bit("access denied!");
      return false;
   }

   QDomDocument doc=QDomDocument("root");
   if (!doc.setContent(&file)) {
      errMsg=QString::fromLocal8Bit("malformed file!");
      return false;
   }

   file.close();
   const QDomNodeList& trksegList=doc.elementsByTagName("trkseg");

   if (trksegList.count()!=1) {
      errMsg=QString::fromLocal8Bit("refused multi-traces file!");
      return false;
   }

   Slippy slippy(rect);
   QSize imageSize=slippy.getImageSize();
   qreal min=-SIDE/2.0,max=-min,ax=SIDE/imageSize.height(),ay=SIDE/imageSize.width(),x,y;
   QVector3D p;
   bool start=true;
   const QDomNodeList& trkptList=trksegList.at(0).toElement().elementsByTagName("trkpt");
   indexList.clear();

   for (int i=0,m=trkptList.count(); i<m; i++) {
      const QDomElement& element=trkptList.at(i).toElement();

      x=ax*slippy.lat2ImageY(element.attribute("lat").toDouble())+min;
      y=ay*slippy.lon2ImageX(element.attribute("lon").toDouble())+min;

      if (x<min || x>max || y<min || y>max) {
         errMsg=QString::fromLocal8Bit("out-of-box data!");
         return false;
      }
      else if (start) {                                        // desormais execute qu'une seule fois
         p=QVector3D(x,y,0);
         indexList.append(0);
         start=false;
      }
      else {                                                   // cas ordinaire
         addSegment(p,QVector3D(x,y,0));
         indexList.append($targetVector->count()-1);
      }
   }

   if ($targetVector->isEmpty()) {                             // desormais inutile
      errMsg=QString::fromLocal8Bit("missing data!");
      return false;
   }

   qDebug("FunTo3D::loadGpxFile/1 : $targetVector->count=%i  indexList.count=%i",$targetVector->count(),indexList.count());
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool FunTo3D::loadKmlFile(const QString& kmlFile,QList<int>& indexList,const QRectF& rect,QString& errMsg)
{  // appel depuis "Viewer::openGpsFile"
   QFile file(kmlFile);
   if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
      errMsg=QString::fromLocal8Bit("access denied!");
      return false;
   }

   QDomDocument doc=QDomDocument("root");
   if (!doc.setContent(&file)) {
      errMsg=QString::fromLocal8Bit("malformed file!");
      return false;
   }

   file.close();
   const QDomNodeList& coordinatesList=doc.elementsByTagName("coordinates");

   if (coordinatesList.count()!=1) {
      errMsg=QString::fromLocal8Bit("refused multi-traces file!");
      return false;
   }

   Slippy slippy(rect);
   QSize imageSize=slippy.getImageSize();
   qreal min=-SIDE/2.0,max=-min,ax=SIDE/imageSize.height(),ay=SIDE/imageSize.width(),x,y;
   QVector3D p;
   bool start=true;
   const QString& text=coordinatesList.at(0).toElement().text().trimmed();
   const QStringList& lineList=text.split('\n',QString::SkipEmptyParts);
   indexList.clear();

   for (int i=0,m=lineList.count(); i<m; i++) {
      const QStringList& lonLat=lineList.at(i).trimmed().split(',');

      x=ax*slippy.lat2ImageY(lonLat.at(1).toDouble())+min;
      y=ay*slippy.lon2ImageX(lonLat.at(0).toDouble())+min;

      if (x<min || x>max || y<min || y>max) {
         errMsg=QString::fromLocal8Bit("out-of-box data!");
         return false;
      }
      else if (start) {
         p=QVector3D(x,y,0);
         indexList.append(0);
         start=false;
      }
      else {
         addSegment(p,QVector3D(x,y,0));
         indexList.append($targetVector->count()-1);
      }
   }

   if ($targetVector->isEmpty()) {
      errMsg=QString::fromLocal8Bit("missing data!");
      return false;
   }

   qDebug("FunTo3D::loadKmlFile/1 : $targetVector->count=%i  indexList.count=%i",$targetVector->count(),indexList.count());
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void FunTo3D::getCookie(const QPointF& p0,qreal r)
{  // polygone a 16 cotes : a=sin(pi/8)=cos(3*pi/8), b=sin(pi/4)=cos(pi/4), c=sin(3*pi/8)=cos(pi/8),
   // centre sur l'origine des cooordonnees (pratique pour les translations!)
   qreal a=r*sqrt(2.0-sqrt(2.0))/2.0,b=r*sqrt(2.0)/2.0,c=r*sqrt(2.0+sqrt(2.0))/2.0;
   QPolygonF polygon=QPolygonF()<<  // dans le sens horaire en partant de midi
         QPointF(0,r)  <<QPointF(a,c)  <<QPointF(b,b)  <<QPointF(c,a)  <<
         QPointF(r,0)  <<QPointF(c,-a) <<QPointF(b,-b) <<QPointF(a,-c) <<
         QPointF(0,-r) <<QPointF(-a,-c)<<QPointF(-b,-b)<<QPointF(-c,-a)<<
         QPointF(-r,0) <<QPointF(-c,a) <<QPointF(-b,b) <<QPointF(-a,c) << QPointF(0,r);
   bool start=true;
   QVector3D p;
   qreal max=SIDE/2.0,min=-max,x,y;
   QTransform matrix;

   matrix.translate(p0.x(),p0.y());
   polygon=matrix.map(polygon);

   for (int i=0,m=polygon.count(); i<m; i++) {
      x=polygon.at(i).x();
      y=polygon.at(i).y();

      if (!(x>=min && x<=max && y>=min && y<=max)) start=true;    // gestion du "clipping"
      else if (start) {
         p=QVector3D(x,y,0);
         start=false;
      }
      else addSegment(p,QVector3D(x,y,0));                        // cas ordinaire
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void FunTo3D::addSegment(QVector3D& p,const QVector3D& q)
{
   int i,m;
   bool invert;

   $segment.clear();
   if (qAbs(q.x()-p.x())>=qAbs(q.y()-p.y())) horizontalMove(p,q,invert);   // tendance "horizontale"
   else verticalMove(p,q,invert);                                          // tendance "verticale"

   m=$segment.count();     // pair, en principe...
   if (m%2) qFatal("qFatal FunTo3D::addSegment/1");

   if (invert) for (i=m-1; i>=0; i--) $targetVector->append($segment.at(i));
   else for (i=0; i<m; i++) $targetVector->append($segment.at(i));

   p=q;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void FunTo3D::horizontalMove(QVector3D p,QVector3D q,bool& invert)
{
   invert= (p.x()>q.x())? swap(p,q) : false;

   qreal
      a=q.y()-p.y(),
      b=p.x()-q.x(),
      c=q.x()*p.y()-p.x()*q.y(),
      x=p.x(),y=p.y(),nextx,nexty;
   int
      sign=sgn(-a*b),
      i=floor(x/STEP),j=floor(y/STEP),
      nexti=i,nextj=j;

   if (a==0 && b==0) return;                 // possible (et meme frequent!), qFuzzyCompare?

   goToCell(i,j,x,y,true);                   // amorcage

   for (; ; i=nexti,j=nextj,x=nextx,y=nexty) {
      nexty= (sign>0)? (j+1)*STEP : j*STEP;
      nextx= (a)? -(b*nexty+c)/a : (i+1)*STEP;

      if (nextx<(i+1)*STEP) nextj=j+sign;    // intersection horizontale
      else {                                 // intersection verticale
         nextx=(i+1)*STEP;
         nexty=-(c+a*nextx)/b;
         nexti=i+1;
      }
      if (nextx>q.x()) {
         goToCell(i,j,q.x(),q.y());
         break;
      }
      goToCell(i,j,nextx,nexty);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void FunTo3D::verticalMove(QVector3D p,QVector3D q,bool& invert)
{
   invert= (p.y()>q.y())? swap(p,q) : false;

   qreal
      a=q.y()-p.y(),
      b=p.x()-q.x(),
      c=q.x()*p.y()-p.x()*q.y(),
      x=p.x(),y=p.y(),nextx,nexty;
   int
      sign=sgn(-a*b),
      i=floor(x/STEP),j=floor(y/STEP),
      nexti=i,nextj=j;

   if (a==0 && b==0) return;                 // possible (et meme frequent!)

   goToCell(i,j,x,y,true);

   for (; ; i=nexti,j=nextj,x=nextx,y=nexty) {
      nextx= (sign>0)? (i+1)*STEP : i*STEP;
      nexty= (b)? -(a*nextx+c)/b : (j+1)*STEP;

      if (nexty<(j+1)*STEP) nexti=i+sign;    // intersection verticale
      else {                                 // intersection horizontale
         nexty=(j+1)*STEP;
         nextx=-(b*nexty+c)/a;
         nextj=j+1;
      }
      if (nexty>q.y()) {
         goToCell(i,j,q.x(),q.y());
         break;
      }
      goToCell(i,j,nextx,nexty);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void FunTo3D::goToCell(int i,int j,qreal x2,qreal y2,bool start)
{
   static QVector3D p;

   if (start) {
      p=drawTo(x2,y2,true);
      return;
   }

   qreal
      v1=i*STEP+SIDE/2.0,w1=j*STEP+SIDE/2.0,
      v2=v1+STEP,w2=w1+STEP,     // v1,w1,v2,w2 : coordonnees de la cellule
      x1=p.x(),y1=p.y(),
      a1=y2-y1,                  // intersection des 2 droites d'equations (formulaire p. 63) :
      b1=x1-x2,                  // a1*x+b1*y+c1=0 (segment a tracer) et...
      c1=x2*y1-x1*y2,            // a2*x+b2*y+c2=0 (diagonale de la cellule)
      a2=w2-w1,
      b2=v1-v2,
      c2=v2*w1-v1*w2,
      delta=a1*b2-b1*a2,         // determinant
      x,y;

   if (a1==0 && b1==0) return;   // points confondus : possible (et meme frequent!)

   if (delta) {                  // recherche d'intersection avec la diagonale
      x=(b1*c2-c1*b2)/delta;
      y=(c1*a2-a1*c2)/delta;

      if (!((x<x1 && x<x2)||(x>x1 && x>x2)||(y<y1 && y<y2)||(y>y1 && y>y2))) drawTo(x,y);
   }
   p=drawTo(x2,y2);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QVector3D FunTo3D::drawTo(qreal x,qreal y,bool start)
{
   static QVector3D p;

   qreal z,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4,d2,d4;
   int i=int((x+SIDE/2.0)/STEP),j=int((y+SIDE/2.0)/STEP),k=i*DIM+j;

   if (k>=DIM*DIM) qFatal("qFatal FunTo3D::drawTo/1");

   z=x2=y2=z2=x3=y3=z3=x4=y4=z4=0;                       // rien que pour desamorcer le warning
   QVector3D2xyz(k,x1,y1,z1);

   if (i<DIM-1 && j<DIM-1) {                             // cas general
      QVector3D2xyz(k+DIM,x2,y2,z2);
      QVector3D2xyz(k+DIM+1,x3,y3,z3);
      QVector3D2xyz(k+1,x4,y4,z4);

      d2=(x2-x)*(x2-x)+(y2-y)*(y2-y);
      d4=(x4-x)*(x4-x)+(y4-y)*(y4-y);

      if (d2==d4) z=(x-x1)*(z3-z1)/STEP+z1;              // diagonale
      else if (d2<d4) {                                  // triangle p1p2p3
         if (y==y1) z=(x-x1)*(z2-z1)/STEP+z1;            // intersection sur p1p2
         else if (x==x2) z=(y-y1)*(z3-z2)/STEP+z2;       // intersection sur p2p3 (poser : x=x1+STEP)
         else z=((x-x1)*(z2-z1)+(y-y1)*(z3-z2))/STEP+z1; // cas general
      }
      else {                                             // triangle p1p4p3
         if (y==y4) z=(x-x1)*(z3-z4)/STEP+z4;            // intersection sur p4p3 (poser : y=y1+STEP)
         else if (x==x1) z=(y-y1)*(z4-z1)/STEP+z1;       // intersection sur p1p4
         else z=((x-x1)*(z3-z4)+(y-y1)*(z4-z1))/STEP+z1; // cas general
      }
   }
   else if (i<DIM-1 && j==DIM-1) z=(x-x1)*($vertexVector.at(k+DIM).z()-z1)/STEP+z1; // triangle p1p2p3
   else if (i==DIM-1 && j<DIM-1) z=(y-y1)*($vertexVector.at(k+1).z()-z1)/STEP+z1;   // triangle p1p4p3
   else z=z1;

   QVector3D q(x,y,z);

   if (!start) {                                         // pas tres optimise, mais bon
      $segment.append(p);
      $segment.append(q);
   }
   p=q;
   return q;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool FunTo3D::swap(QVector3D& p,QVector3D& q)
{
   QVector3D r=p;
   p=q;
   q=r;

   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void FunTo3D::QVector3D2xyz(int index,qreal& x,qreal& y,qreal& z)
{
   const QVector3D* v=&$vertexVector.at(index);

   x=v->x();
   y=v->y();
   z=v->z();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

int FunTo3D::sgn(double value)
{
   return (value<0)? -1 : 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

double AStar::distance(const QPointF& p,const QPointF& q)
{
   return sqrt((p.x()-q.x())*(p.x()-q.x())/$coef.x()+(p.y()-q.y())*(p.y()-q.y())/$coef.y());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QPointF AStar::getPos(qint64 sid)
{  // methode pas vraiment necessaire pour ce bout de code, mais bon...
   return $nodeMap.value(sid).first;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void AStar::insert(qint64 sid,const Cell& cell)
{
   $openHash[sid]=cell;
   $bestCostMap.insert(cell.costf,sid);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void AStar::update(qint64 sid,const Cell& cell)
{
   if ($bestCostMap.remove($openHash.value(sid).costf,sid)!=1) qFatal("qFatal AStar::update");
   $openHash[sid]=cell;
   $bestCostMap.insert(cell.costf,sid);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void AStar::remove(qint64 sid)
{
   if ($bestCostMap.remove($openHash.value(sid).costf,sid)!=1) qFatal("qFatal AStar::remove/1");
   if ($openHash.remove(sid)!=1) qFatal("qFatal AStar::remove/2");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool AStar::isValid(qint64 sidp,qint64 sidq)
{
   if (!$chain.contains(sidq)) return true;

   for (int j=0,n=$chain.count()-1; j<=n; j++) if ($chain.at(j)==sidq) {
      if ((j && $chain.at(j-1)==sidp)||(j<n && $chain.at(j+1)==sidp)) return false;
   }
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void AStar::addNeighbors(qint64 sidp)
{
   QPointF goalPos=getPos($goal);

   if (!$closedHash.contains(sidp)) {
      qDebug("AStar::addNeighbors/1 : sidp=%lld",sidp);
      return;
   }

   double costp=$closedHash.value(sidp).costg,costg;
   qint64 sidq;
   SummitMapType::const_iterator itj=$summitMap.find(sidp);

   if (itj==$summitMap.end()) {
      qDebug("AStar::addNeighbors/2 : sidp=%lld",sidp);
      return;
   }
   // on met tous les noeud adjacents dans la liste ouverte
   for (QMap<qint64,double>::const_iterator iti=itj->constBegin(); iti!=itj->constEnd(); iti++) {
      sidq=iti.key();

      if (isValid(sidp,sidq) && !$closedHash.contains(sidq)) { // noeud absent de la liste fermee
         costg=costp+(*iti);
         Cell cell={costg,costg+distance(getPos(sidq),goalPos),sidp};

         if ($openHash.contains(sidq)) {  // noeud present dans la liste ouverte, il faut comparer les couts
            if (cell.costf<$openHash.value(sidq).costf) update(sidq,cell);// si le nouveau chemin est meilleur, on update,...
         }
         else insert(sidq,cell);          // noeud absent dans la liste ouverte, on l'ajoute
      }
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void AStar::makeChain(qint64 startParent)
{  // l'arrivee est le dernier element de la liste fermee.
   Cell& cell=$closedHash[$goal];
   qint64 prior=cell.parent;

   $chain.clear();
   $chain.prepend($goal);

   while (prior!=startParent) {
      $chain.prepend(prior);
      cell=$closedHash.value(cell.parent);
      prior=cell.parent;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

AStar::AStar(const NodeMapType& nodeMap,const SummitMapType& summitMap,QList<qint64>& chain,
      qint64 current,qint64 goal,const QPointF& coef,bool& rslt) :
   $nodeMap(nodeMap),$summitMap(summitMap),$chain(chain),$goal(goal),$coef(coef)
{
   Cell start={0.0,0.0,-1};   // -1 : identifiant inconnu expres
   insert(current,start);     // ajout de current dans la liste ouverte

   int loop=0;
   QTime time;
   time.start();

    do { // on cherche le meilleur noeud de la liste ouverte, on sait qu'elle n'est pas vide donc il existe
      current=$bestCostMap.begin().value();
       $closedHash[current]=$openHash.value(current); // on le passe dans la liste fermee, il ne peut pas deja y etre
      remove(current);        // a supprimer de la liste ouverte, ce n'est plus une solution explorable
      addNeighbors(current);
      loop++;
   }
   while (current!=$goal && !$openHash.empty());

   if (current==$goal) {
      makeChain(start.parent);// afficher ici le temps d'execution peut poser probleme si l'on veut comparer des logs
      qDebug("success AStar::AStar : loop=%i  t=%ims  optimal path(%lld,%lld) :",loop,time.elapsed(),chain.first(),$goal);

      str2k buf="";
      for (int i=0,m=$chain.count(); i<m; i++) concat(buf,"%lld ",$chain.at(i));
      qDebug("%s",buf);

      rslt=true;
   }
   else rslt=false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

AStar::AStar(const NodeMapType& nodeMap,const SummitMapType& summitMap,QList<qint64>& chain,const QPointF& coef) :
      $nodeMap(nodeMap),$summitMap(summitMap),$chain(chain),$coef(coef) {}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool AStar::approve(qint64 current,qint64 goal)
{
   $goal=goal;

   $chain.clear();
   $openHash.clear();
   $closedHash.clear();
   $bestCostMap.clear();

   Cell start={0.0,0.0,-1};
   insert(current,start);

   do {  // on cherche le meilleur noeud de la liste ouverte, on sait qu'elle n'est pas vide donc il existe
      current=$bestCostMap.begin().value();
      $closedHash[current]=$openHash.value(current);  // on le passe dans la liste fermee, il ne peut pas deja y etre
      remove(current);                 // a supprimer de la liste ouverte, ce n'est plus une solution explorable
      addNeighbors(current);
   }
   while (current!=$goal && !$openHash.empty());

   if (current==$goal) {
      makeChain(start.parent);
      return ($chain.count()>6);       // 6 : juste le bon nombre pour se debarrasser des situations indesirables
   }
   else return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

char* Shared::out(qint64 nidp,qint64 nidq)
{  // default : qint64 nidq=-1, d'apres Branch::out
   static str256 buf;

   const QPointF& p=$nodeMap.value(nidp).first;
   sprintf(buf,"%lld(%i,%i)",nidp,qRound(p.x()),qRound(p.y()));
   if (nidq==-1) return buf;

   const QPointF& q=$nodeMap.value(nidq).first;
   return concat(buf," %lld(%i,%i)",nidq,qRound(q.x()),qRound(q.y()));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Shared::msg(bool update,const char* fmt,...)
{
   va_list argptr;
   str256 s;

   va_start(argptr,fmt);
   vsprintf(s,fmt,argptr);
   va_end(argptr);

   emit msgBox(update,QString::fromLatin1(s));

   return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

int Shared::indexOf(qint64 wid,qint64 nid)
{  // en principe plus immediat que de parcourir "$wayMap.value(wid).first"
   const QList<QPair<qint64,qint16>>& nlist=$nodeMap.value(nid).second;

   for (int i=nlist.count()-1; i>=0; i--) if (nlist.at(i).first==wid) return nlist.at(i).second;

   return -1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Shared::isTip(const QPair<qint64,qint16>& pair)
{
   return (pair.second==0 || pair.second==$wayMap.value(pair.first).first.count()-1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Shared::isJunction(qint64 nid1)
{  // liste des ways ou apparaissent "nid1"
   const QList<QPair<qint64,qint16>>& nlist1=$nodeMap.value(nid1).second;

   if (nlist1.count()!=2) return false;                  // sortie ici si "nid1" n'est pas un sommet
   if (!isTip(nlist1.at(0)) || !isTip(nlist1.at(1))) return false;   // doit etre une extremite

   int count=$summitMap.value(nid1).count();

   if (count==1) {                                       // cas particulier
      qint64 sid2=$summitMap.value(nid1).begin().key();
      const QList<QPair<qint64,qint16>>& nlist2=$nodeMap.value(sid2).second;
      qint64 wid0=nlist1.at(0).first,wid1=nlist1.at(1).first;
      int i,m=nlist2.count();

      for (i=0; i<m; i++) if (nlist2.at(i).first==wid0) break;
      if (i==m) qFatal("qFatal Shared::isJunction/1 : %s",out(nid1,sid2));

      for (i=0; i<m; i++) if (nlist2.at(i).first==wid1) break;
      if (i==m) qFatal("qFatal Shared::isJunction/2 : %s",out(nid1,sid2));
   }
   else if (count!=2) qFatal("qFatal Shared::isJunction/3 : %s  count=%i",out(nid1),$summitMap.value(nid1).count());
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

char* Branch::outSegm(const Segm& segm)
{
   static str256 buf;
   qint64 wid=segm.wid();
   const QList<QPair<qint64,int>>& route=$wayMap.value(wid).first;
   sprintf(buf,"{%lld %s}",wid,out(route.at(segm.begin()).first,route.at(segm.end()).first));

   return buf;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Branch::appendToHead(const QList<Segm>& body,QList<Segm>& head,const Segm& segm)
{
   Segm newSegm=segm;
   char rslt=0;         // "newSegm" ajoute si (rslt<=0)

   if (body.isEmpty() || (rslt=isDejaVu(body,segm,newSegm))<0) head.append(newSegm);
   qDebug("Branch::appendToHead/1 : %s  rslt=%i",outSegm(newSegm),rslt);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Branch::appendToBody(QList<Segm>& body,const Segm& segm)
{
   Segm current=segm;
   qint64 nid=getNid(segm,Segm::BEGIN);
   bool
      finish=(body.count() && nid==getNid(body.first(),Segm::BEGIN) && (!$summitMap.contains(nid) || isJunction(nid))),
      inv=(body.count() && getNid(body.last(),Segm::END)!=getNid(segm,Segm::BEGIN));

   qDebug("\nBranch::appendToBody/1 : body.count=%i  finish=%s  inv=%s",body.count(),
         (finish)? "true" : "false",(inv)? "true" : "false");

   if (!inv) {
      body.append(current);

      while (isJunction(getNid(current,Segm::END))) {
         current=junction(current);       // (recursion==false)
         body.append(current);
      }
   }
   else {
      int pos=body.count();

      body.insert(pos,current.inv());

      while (isJunction(getNid(current,Segm::END))) {
         current=junction(current);       // (recursion==false)
         body.insert(pos,current.inv());
      }
   }
   return finish;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

char Branch::isDejaVu(const QList<Segm>& body,const Segm& segm,Segm& newSegm)
{  // appel depuis "Branch::appendToHead"
   Segm lastSegm= (isJunction(getNid(segm,Segm::END)))? junction(segm,true) : segm;

   if (body.contains(segm) || body.contains(segm.inv()) || body.contains(lastSegm.inv()))
      return 10;                                      // case 11,12,21,41

   const Segm& segm0=body.at(0);
   qint64 wid0=segm0.wid(),nid0=getNid(segm0,Segm::BEGIN);
   qint16 begin0=segm0.begin(),end0=segm0.end();

   if ($summitMap.contains(nid0)) {
      Segm lastSegm0= (isJunction(getNid(segm0,Segm::END)))? junction(segm0,true) : segm0;
      if (lastSegm0==lastSegm && isJunction(nid0)) {
         newSegm=junction(segm0.inv());               // (recursion==false)
         return -20;                                  // case -42
      }
   }
   else {
      QMap<qint16,qint64>::const_iterator it=$wayMap.value(wid0).second.upperBound(begin0);
      if (end0>begin0) it--;

      if (Segm(wid0,end0,it.key())==segm) return 30;  // case 31
      else {
         Segm segmx(wid0,it.key(),end0);

         if (((isJunction(getNid(segmx,Segm::END)))? junction(segmx,true) : segmx)==lastSegm) {
            newSegm=Segm(wid0,begin0,it.key());
            return -40;                               // case -22,-32
         }
      }
   }

   return -1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

qint64 Branch::getNid(const Segm& segm,Segm::Arg arg)
{
   if (arg==Segm::BEGIN) return $wayMap.value(segm.wid()).first.at(segm.begin()).first;
   else if (arg==Segm::END) return $wayMap.value(segm.wid()).first.at(segm.end()).first;
   else qFatal("qFatal Branch::getNid/1");
   return -1;                                         // jamais execute
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Branch::finish(QList<Segm>& body,QList<Segm>& head,int num)
{
   head.clear();
   drawBody(body);
   body.clear();
   return msg(true,"Branch::finish/%i\nTerminus!",num);  // en fait toujours "false"
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Segm Branch::junction(const Segm& segm,bool recursion,const QColor& color)
{  // default : recursion=false,color=QColor()
   qint64 wid0=segm.wid(),nid0=getNid(segm,Segm::BEGIN),nid1=getNid(segm,Segm::END),wid1,sid2;
   const QList<qint64> neighbor=$summitMap.value(nid1).keys();          // "nid1" serait donc un sommet!

   if (!$summitMap.contains(nid0)) {
      QMap<qint16,qint64>::const_iterator it=$wayMap.value(wid0).second.upperBound(segm.begin());
      if (it.key()==segm.end()) it--;
      nid0=$wayMap.value(wid0).first.at(it.key()).first;
   }
   // determine lequel des 2 voisins qui n'appartient pas au way wid0 (en valeur absolue!)
   if (neighbor.count()==1) {
      msg(false,"Branch::junction/1\nParticular case");
      sid2=neighbor.at(0);
   }
   else sid2=neighbor.at((neighbor.at(0)==nid0)? 1 : 0);

   const QList<QPair<qint64,qint16>> &nlist1=$nodeMap.value(nid1).second,&nlist2=$nodeMap.value(sid2).second;
   int j,i;
   // determine le way wid1 commun a nid1 et sid2
   for (j=nlist1.count()-1; j>=0; j--) {
      wid1=nlist1.at(j).first;
      if (wid1==wid0 || !isTip(nlist1.at(j))) continue;                 // nid1 doit etre une extremite
      for (i=nlist2.count()-1; i>=0; i--) if (nlist2.at(i).first==wid1) break;
      if (i>=0) break;
   }
   if (j<0) qFatal("qFatal Branch::junction/2 : %s",out(nid1,sid2));    // aie!

   Segm newSegm(nlist1.at(j),nlist2.at(i).second);
   if (color.isValid()) emit addSegm(newSegm,color);                    // equivalent a : if (color!=QColor())

   if (recursion) {
      if (isJunction(sid2)) return junction(newSegm,recursion,color);
      else return newSegm;
   }
   else return newSegm;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Branch::drawBody(const QList<Segm>& body)
{
   QList<Step> stepList;

   emit clearSegmPath();   // correct, puisqu'aucun "addSegm" n'est emis avant

   for (int i=0,m=body.count()-1; i<=m; i++) {
      const Segm& segm=body.at(i);
      emit addSegm(segm,Qt::magenta);
      if (i==0) stepList.append(Step(getNid(segm,Segm::BEGIN),1));   // 1 : blue
      if (i==m) stepList.append(Step(getNid(segm,Segm::END),2));     // 2 : red
      else {
         const qint64& nid=getNid(segm,Segm::END);
         if (!isJunction(nid)) stepList.append(Step(nid,2));         // 2 : red
      }
   }

   if (stepList.count()) emit addStepList(stepList);                 // ok
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Branch::next(qint64 nid,Qt::MouseButton button)
{  // default : qint64 nid=0,Qt::MouseButton button=Qt::NoButton
   static int pos;
   static Segm lastSegm;
   static QList<Segm> body,head;

   if (nid==0) {        // reinitialisation suite a un changement de terrain
      head.clear();
      body.clear();     // ok : voir plus loin avec (button==Qt::RightButton)
      return false;
   }

   if (nid!=-1) {
      qint64 wid,newid=0;
      qint16 index,delta=0,k;
      QMap<qint16,qint64>::const_iterator it;

      if (nid==-2) {                                                 // validation d'un choix (avec "inser")
         if (head.isEmpty()) return false;
         if (appendToBody(body,head.at(pos))) return finish(body,head,1);
         newid=lastSegm.wid();
         index=lastSegm.begin();
         if (!$summitMap.contains(getNid(lastSegm,Segm::BEGIN))) {   // au depart, par exemple...
            it=$wayMap.value(newid).second.upperBound(index);
            if (it.key()==lastSegm.end()) it--;
            index=it.key();
         }
         delta=lastSegm.end()-index;
         nid=getNid(lastSegm,Segm::END);
         if (!$summitMap.contains(nid) || $summitMap.value(nid).count()==1) return finish(body,head,2);
      }
      else if (button==Qt::RightButton) body.clear();                // initialisation
      else if (button==Qt::LeftButton) {                             // cloture
         if (body.isEmpty())                                         // sans quoi complications insensees...
               return msg(false,"Branch::next/1\nInitial segment required");
         Segm current=head.at(pos);
         bool flag=(getNid(current,Segm::BEGIN)==getNid(body.first(),Segm::BEGIN)),start=true;

         do {
            if (start) start=false;
            else current=junction(current);                          // (recursion==false)

            int i=indexOf(current.wid(),nid);
            if (i==-1) continue;
            if (current.contains(i)) {                               // finish==false, inv==true
               appendToBody(body,Segm(current.wid(),i,(flag)? current.end() : current.begin()));
               return finish(body,head,3);
            }
         } while (isJunction(getNid(current,Segm::END)));            // origine : appendToBody

         return msg(false,"Branch::next/2\nSector not selected");
      }
      else qFatal("qFatal Branch::next/1");                          // inutile, mais bon...

      pos=-1;
      head.clear();

      const QList<QPair<qint64,qint16>>& nlist=$nodeMap.value(nid).second;    // liste des ways ou apparaissent "nid"

      if (!$summitMap.contains(nid)) {                               // nid est un noeud quelconque (demarrage)
         if (nlist.count()!=1) qFatal("qFatal Branch::next/2");
         wid=nlist.at(0).first;
         if (isTip(nlist.at(0))) qFatal("qFatal Branch::next/3");    // ne devrait jamais arriver, mais bon...

         it=$wayMap.value(wid).second.upperBound(nlist.at(0).second);
         head.append(Segm(nlist.at(0),it.key()));
         it--;
         head.append(Segm(nlist.at(0),it.key()));
      }
      else {                                                         // nid est un sommet
         for (int i=nlist.count()-1; i>=0; i--) {
            wid=nlist.at(i).first;
            k=nlist.at(i).second;

            if (!isTip(nlist.at(i))) {                               // midnode
               it=$wayMap.value(wid).second.upperBound(k);
               if (wid!=newid || (k-it.key())!=delta) appendToHead(body,head,Segm(nlist.at(i),it.key()));
               it-=2;
               if (wid==newid && (k-it.key())==delta) continue;
            }
            else {                                                   // extremite
               if (wid==newid && k==lastSegm.end()) continue;
               it= (!k)? $wayMap.value(wid).second.begin()+1 : $wayMap.value(wid).second.end()-2;
            }
            appendToHead(body,head,Segm(nlist.at(i),it.key()));
         }

         if (head.isEmpty()) return finish(body,head,4);             // cul de sac (fin de parcours)
      }
   }
   else if (head.isEmpty()) return false; // (nid==-1) : selection des differentes possibilites (default)

   drawBody(body);                        // si (nid==-1) cette operation pourrait ne pas etre systematique
   pos=(pos+1)%head.count();
   const Segm& segm=head.at(pos);
   emit addSegm(segm,Qt::blue);

   lastSegm= (isJunction(getNid(segm,Segm::END)))? junction(segm,true,Qt::blue) : segm;
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

char Routing::status(const QList<Step>& stepList,qint64 nid,bool start)
{  // valeurs renvoyees : -1, 0, +1
   qint64 nidp,nidq,wid;
   int count=stepList.count(),k;

   if (start) {
      nidp=stepList.at(0).first;
      nidq=stepList.at(1).first;
      if (nid==nidp) return 0;                        // code 32
   }
   else {
      nidp=stepList.at(count-2).first;
      nidq=stepList.at(count-1).first;
      if (nid==nidq) return 0;                        // code 42
   }

   const QList<QPair<qint64,qint16>> &nlistp=$nodeMap.value(nidp).second,&nlistq=$nodeMap.value(nidq).second;
   bool flag=($summitMap.contains(nidp) && $summitMap.value(nidp).contains(nidq));

   for (int j=nlistp.count()-1,i; j>=0; j--) {        // decrementation, comme dans "drawStepList"
      wid=nlistp.at(j).first;
      for (i=nlistq.count()-1; i>=0 && nlistq.at(i).first!=wid; i--);
      if (i<0) continue;
      if ((k=indexOf(wid,nid))!=-1) {
         if (flag) {
            QMap<qint16,qint64>::const_iterator it=$wayMap.value(wid).second.upperBound(k);
            return ((*it==nidq && *(it-1)==nidp)||(*it==nidp && *(it-1)==nidq))? 1 : -1;
         }
         else {
            int p=indexOf(wid,nidp),q=indexOf(wid,nidq);
            return ((p<k && k<q)||(q<k && k<p))? 1 : -1;
         }
      }
      else if (flag && $summitMap.value(nidp).value(nidq)<0) continue;
      else return -1;                                 // nid n'appartient pas a wid
   }
   return -1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Segm Routing::summitsCase(const QList<QPair<qint64,qint16>>& nlist0,qint64 sid1)
{
   QList<Segm> segmList;

   for (int j=nlist0.count()-1; j>=0; j--) {
      qint64 wid0=nlist0.at(j).first;
      qint16 index0=nlist0.at(j).second;
      const QMap<qint16,qint64>& wmap=$wayMap.value(wid0).second;
      QMap<qint16,qint64>::const_iterator it=wmap.find(index0);

      if (it==wmap.end()) qFatal("qFatal Routing::summitsCase/1");
      if (index0 && *(it-1)==sid1) segmList.append(Segm(wid0,index0,(it-1).key()));
      if (it!=wmap.end()-1 && *(it+1)==sid1) segmList.append(Segm(wid0,index0,(it+1).key()));
   }

   if (segmList.isEmpty()) qFatal("qFatal Routing::summitsCase/2");

   // cas trivial
   if (segmList.count()==1) return segmList.at(0);

   // twinCase : il faut comparer les differentes longueurs afin d'en determiner la plus courte
   qreal minLength=1e6;
   int k=0;

   for (int j=0,n=segmList.count(); j<n; j++) {
      const Segm& segm=segmList.at(j);
      const QList<QPair<qint64,int>>& route=$wayMap.value(segm.wid()).first;
      qint16 begin=segm.begin(),end=segm.end();
      QPainterPath path;

      if (end<begin) qSwap(begin,end);
      path.moveTo($nodeMap.value(route.at(begin).first).first);

      for (int i=begin+1; i<=end; i++) path.lineTo($nodeMap.value(route.at(i).first).first);

      qreal length=path.length();
      if (length<minLength) {
         minLength=length;
         k=j;
      }
   }
   return segmList.at(k);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Routing::drawStepList(const QList<Step>& stepList)
{  // en cas de doublons(et +), recherche du plus court chemin
   int
      begin= (stepList.at(0).second==3)? 1 : 0,          // 4 -> begin=0
      i,j,max=stepList.count()-1,
      end= (stepList.at(max).second==3)? max-1 : max;    // 5 -> end=max
   qint64 nid0=stepList.at(begin).first,nid1,wid0;
   qint16 index0,index1;

   emit clearSegmPath();

   for (int k=begin+1; k<=end; k++) {
      const QList<QPair<qint64,qint16>>& nlist0=$nodeMap.value(nid0).second;
      nid1=stepList.at(k).first;

      if (!$summitMap.contains(nid0) || !$summitMap.value(nid0).contains(nid1)) {
         const QList<QPair<qint64,qint16>>& nlist1=$nodeMap.value(nid1).second;

         for (j=nlist0.count()-1; j>=0; j--) {  // recherche d'un way contenant les 2 nodes
            wid0=nlist0.at(j).first;
            for (i=nlist1.count()-1; i>=0; i--) if (wid0==nlist1.at(i).first) {
               if ($summitMap.contains(nid1)) {
                  index0=nlist0.at(j).second;
                  QMap<qint16,qint64>::const_iterator it=$wayMap.value(wid0).second.upperBound(index0);
                  index1= (*it==nid1)? it.key() : (it-1).key();
               }
               else if ($summitMap.contains(nid0)) {
                  index1=nlist1.at(i).second;
                  QMap<qint16,qint64>::const_iterator it=$wayMap.value(wid0).second.upperBound(index1);
                  index0= (*it==nid0)? it.key() : (it-1).key();
               }
               else {
                  index0=nlist0.at(j).second;
                  index1=nlist1.at(i).second;
               }
               emit addSegm(Segm(wid0,index0,index1),Qt::magenta);
               break;
            }
            if (i>=0) break;                 // sort de la boucle 'j'
         }
         // teste en amont dans "isAllRight", mais bon...
         if (j<0) return msg(false,"Routing::drawStepList/1\n%s",out(nid0,nid1));
      }
      else emit addSegm(summitsCase(nlist0,nid1),Qt::magenta);          // enchainements de sommets

      nid0=nid1;
   }
   qDebug(" ");
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Routing::otherCases(qint64 blue,qint64 red,qint64 black,QList<Step>& stepList,bool start)
{
   const QList<QPair<qint64,qint16>>& blueNlist=$nodeMap.value(blue).second;  // liste des ways ou apparaissent "blue"
   if (blueNlist.count()!=1) qFatal("qFatal Routing::otherCases/1");          // traite dans Routing::next

   const QList<QPair<qint64,qint16>>& redNlist=$nodeMap.value(red).second;    // liste des ways ou apparaissent "red"
   if (redNlist.count()!=1) qFatal("qFatal Routing::otherCases/2");           // traite dans Routing::next

   qint64 wid=blueNlist.at(0).first;

   if (redNlist.at(0).first!=wid) {                                           // ordre : "b n r"
      qDebug("Routing::otherCases/3");                                        // blue et red n'appartiennent  pas au meme way
      if (start) stepList << Step(blue,1) << Step(black,3) << Step(red,2);
      else {
         if (stepList.last().first!=black) return false;
         stepList << Step(red,2);
      }
      return true;
   }

   qint16 b=blueNlist.at(0).second,r=redNlist.at(0).second,n=indexOf(wid,black);
   if (n==-1) qFatal("qFatal Routing::otherCases/4");

   if ((b<n && n<r)||(r<n && n<b)) {                                          // ordre : "b n r"
      qDebug("Routing::otherCases/5");
      if (start) stepList << Step(blue,1) << Step(black,3) << Step(red,2);
      else {
         if (stepList.last().first!=black) return false;
         stepList << Step(red,2);
      }
   }
   else if ((n<b && b<r)||(r<b && b<n)) {                                     // ordre : "n b r"
      qDebug("Routing::otherCases/6");
      if (start) stepList << Step(black,3) << Step(blue,1) << Step(red,2);
      else {
         if (stepList.last().first==black) return false;
         stepList << Step(red,2);
      }
   }
   else {                                                                     // ordre : "b r n"
      qDebug("Routing::otherCases/7");                                        // ((b<r && r<n)||(n<r && r<b))
      if (start) stepList << Step(blue,1) << Step(red,2) << Step(black,3);
      else {
         if (stepList.last().first!=black) return false;
         stepList.insert(stepList.count()-1,Step(red,2));
      }
   }
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Routing::isAllRight(const QList<Step>& stepList)
{  // Appel depuis "Routing::next"
   int i,k,count=stepList.count();
   qint64 nid0,nid1,nid2,wid1;
   qint16 index0,index1,index2;
   bool start=true;

   // conformite de "stepList" num1 : les "sommets" entre eux
   for (int j=0; j<count; j++) if (stepList.at(j).second>=3) {
      if (start) {
         nid0=stepList.at(j).first;
         start=false;
         continue;
      }
      nid1=stepList.at(j).first;
      if (!$summitMap.value(nid0).contains(nid1)) qFatal("qFatal Routing::isAllRight/1 : %s",out(nid0,nid1));
      nid0=nid1;
   }

   // conformite de "stepList" num2 : les "non-sommets" avec les "sommets"
   for (int j=0; j<count; j++) {
      if (stepList.at(j).second>=3) continue;   // le point a analyser ne doit pas etre un sommet
      const QPair<qint64,qint16>& pair=$nodeMap.value(stepList.at(j).first).second.at(0); // nid1 n'appartient qu'a 1 way

      wid1=pair.first;     // n'etant pas un sommet, le point n'appartient qu'a un seul way et wid1>0
      index1=pair.second;  // mais s'il est present 2 fois dans ce way, il est un sommet, non?
      index0=index2=-1;

      for (i=j-1; i>=0; i--) {
         if (stepList.at(i).second<3) continue; // nid0 doit etre le 1er sommet rencontre en "descendant"
         if ((index0=indexOf(wid1,stepList.at(i).first))!=-1) break;
         return msg(false,"Routing::isAllRight/2\n%s",out(stepList.at(j).first,stepList.at(i).first));
      }                                         // si (j==0) la boucle est sautee et on a (index0==-1)

      for (k=j+1; k<count; k++) {
         if (stepList.at(k).second<3) continue; // nid2 doit etre le 1er sommet rencontre en "montant"
         if ((index2=indexOf(wid1,stepList.at(k).first))!=-1) break;
         return msg(false,"Routing::isAllRight/3\n%s",out(stepList.at(j).first,stepList.at(k).first));
      }                                         // si (j==count-1) la boucle est sautee et on a (index2==-1)

      if (index0!=-1 && index2!=-1) {           // nid1 situe entre 2 sommets nid0 et nid2
         if ((index0<index1 && index1<index2)||(index2<index1 && index1<index0)) continue;
         QMap<qint16,qint64>::const_iterator it=$wayMap.value(wid1).second.upperBound(index1);
         nid0=stepList.at(i).first;
         nid2=stepList.at(k).first;

         if (!((*it==nid2 && *(it-1)==nid0)||(*it==nid0 && *(it-1)==nid2))) return msg(false,"Routing::isAllRight/4\n%s",out(nid0,nid2));
      }
   }

   // conformite de "stepList" num3 : cas particulier du sommet de type 5 avec avant lui un simple noeud
   for (int j=0; j<count; j++) {
      if (stepList.at(j).second!=5 || j==count-1 || stepList.at(j-1).second>2) continue;
      nid0=stepList.at(j-1).first;
      nid2=stepList.at(j+1).first;              // nid2 quant a lui est un noeud quelconque, sommet ou pas
      wid1=$nodeMap.value(nid0).second.at(0).first;
      const QList<QPair<qint64,qint16>>& nlist2=$nodeMap.value(nid2).second;

      for (i=nlist2.count()-1; i>=0 && nlist2.at(i).first!=wid1; i--);
      if (i<0) continue;

      // les 3 noeuds appartiennent maintenant a la meme "route"
      index0=$nodeMap.value(nid0).second.at(0).second;
      index1=indexOf(wid1,stepList.at(j).first);
      index2=nlist2.at(i).second;
      if ((index0<index1 && index1<index2)||(index2<index1 && index1<index0)) continue;
      return msg(false,"Routing::isAllRight/5\n");
   }

   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

qint64 Routing::nextSummit(qint64 sidBegin,qint64 nidBegin)
{
   const QPair<qint64,qint16>& pair=$nodeMap.value(nidBegin).second.first(); // nidBegin n'est pas un sommet
   qint64 wid=pair.first;
   qint16 index=pair.second;
   QMap<qint16,qint64>::const_iterator it=$wayMap.value(wid).second.upperBound(index);

   return (*it==sidBegin)? *(it-1) : *it;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Routing::isValid(const QList<Step>& stepList,qint64 nidBegin,qint64 nid,char start)
{
   if (start>1 && !$summitMap.contains(nid)) {
      qint64 wid=$nodeMap.value(nid).second.at(0).first;
      qint16 index=$nodeMap.value(nid).second.at(0).second;
      int
         begin= (stepList.at(0).second==3)? 1 : 0,
         max=stepList.count()-1,
         end= (stepList.at(max).second==3)? max-1 : max;

      for (int k=begin; k<end; k++) {
         const QList<QPair<qint64,qint16>>& nlist0=$nodeMap.value(stepList.at(k).first).second;

         for (int j=nlist0.count()-1; j>=0; j--) if (nlist0.at(j).first==wid) {
            const QList<QPair<qint64,qint16>>& nlist1=$nodeMap.value(stepList.at(k+1).first).second;

            for (int i=nlist1.count()-1; i>=0; i--) if (nlist1.at(i).first==wid) {
               qint16 index0=nlist0.at(j).second,index1=nlist1.at(i).second;
               if (k==begin) {      // permet de faire une boucle!
                  if ((index0<index && index<=index1)||(index1<=index && index<index0)) return false;
               }
               else if ((index0<=index && index<=index1)||(index1<=index && index<=index0)) return false;
            }
         }
      }
   }

   if ($summitMap.contains(nidBegin) || $summitMap.contains(nid) ||
         $nodeMap.value(nidBegin).second.at(0).first!=$nodeMap.value(nid).second.at(0).first) return true;

   // nidBegin et nid sont des noeuds ordinaires appartenant au meme way
   const QMap<qint16,qint64>& wmap=$wayMap.value($nodeMap.value(nidBegin).second.at(0).first).second;
   // et dans ce way il ne peut y avoir de noeud present 2 fois (et qui est donc aussi sommet)
   for (QMap<qint16,qint64>::const_iterator itj=wmap.constBegin(); itj!=wmap.constEnd()-1; itj++) {
      for (QMap<qint16,qint64>::const_iterator iti=itj+1; iti!=wmap.constEnd(); iti++) if (*iti==*itj) return false;
   }
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

const QList<Step>& Routing::cleanStepList(const QList<Step>& stepList)
{
   static QList<Step> cleanList;

   cleanList=stepList;

   if (cleanList.last().second==3) cleanList.removeLast();
   if (cleanList.first().second==3) cleanList.removeFirst();

   // sans toucher aux extremites
   for (int i=cleanList.count()-2; i>=1; i--) if (isJunction(cleanList.at(i).first)) cleanList.removeAt(i);

   return cleanList;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Routing::next(qint64 nid,qint64 sid,Qt::MouseButton button)
{  // default : qint64 nid=0,qint64 sid=0,Qt::MouseButton button=Qt::NoButton
   static char start=0;
   static qint64 nidBegin,sidBegin;
   static QList<Step> stepList0;          // les differents points sensibles dans l'ordre d'apparition

   if (nid==0) {                             // reinitialisation suite a un changement de terrain
      start=0;
      return false;
   }

   if (button==Qt::RightButton) {            // click "droit" : point de depart et reinitialisations
      nidBegin=nid;
      sidBegin=sid;  // note : caler le sommet de depart sans connaitre celui d'arrivee peut fausser le resultat
      stepList0.clear();
      start=1;
      return false;
   }

   if (start==0) return false;

   // click "gauche" : cas ordinaire du point d'arrivee, id0End : nid, id1End : sid
   QList<Step> stepList1=stepList0;

   if (!isValid(stepList1,nidBegin,nid,start)) return msg(false,"Routing::next/1\nInvalid position...");

   if (sidBegin==sid) {
      if (nidBegin==nid) return false;
      if (start==1) {
         if (nidBegin==sidBegin) stepList1 << Step(nidBegin,4) << Step(nid,2);
         else if (nid==sid) stepList1 << Step(nidBegin,1) << Step(nid,5);     // nouveau cas!
         else otherCases(nidBegin,nid,sid,stepList1,true);
      }
      else {
         if (nidBegin==sidBegin) stepList1 << Step(nid,2);
         else if (nid==sid) {
            if (stepList1.last().first!=sid) return msg(false,"Routing::next/2\nInvalid position...");
            stepList1.replace(stepList1.count()-1,Step(nid,5));
         }
         else if (!otherCases(nidBegin,nid,sid,stepList1))
               return msg(false,"Routing::next/3\nInvalid position...");
      }
   }
   else {
      QList<qint64> chain;
      bool rslt;

      if (stepList1.count() && stepList1.last().second==2) {
         qint64 sidNext=nextSummit(sidBegin,nidBegin);
         if (sidNext!=sid) {
            sidBegin=sidNext;
            stepList1.append(Step(sidBegin,3));
         }
      }

      for (int i=0,m=stepList1.count(); i<m; i++) if (stepList1.at(i).second>2) chain.append(stepList1.at(i).first);

      AStar($nodeMap,$summitMap,chain,sidBegin,sid,$coef,rslt);         // sidBegin negatif!
      if (!rslt) return msg(false,"Routing::next/4\nNo solution!");

      for (int i= (start==1)? 0 : 1,m=chain.count(); i<m; i++) stepList1.append(Step(chain.at(i),3)); // yop!

      if (start==1) {
         switch (status(stepList1,nidBegin,true)) {
            case -1:       // nidBegin avant sidBegin
               stepList1.prepend(Step(nidBegin,1));   // rond bleu
               break;
            case 0:        // nidBegin==sidBegin
               stepList1.replace(0,Step(nidBegin,4));// carre bleu
               break;
            case 1:        // nidBegin apres sidBegin
               stepList1.insert(1,Step(nidBegin,1));  // rond bleu
               break;
         }
      }

      switch (status(stepList1,nid,false)) {
         case -1:          // id0End apres id1End
            stepList1.append(Step(nid,2));            // rond rouge
            break;
         case 0:           // id0End==id1End
            stepList1.replace(stepList1.count()-1,Step(nid,5));      // carre rouge
            break;
         case 1:           // id0End avant id1End
            stepList1.insert(stepList1.count()-1,Step(nid,2));       // insere id0End avant id1End
            break;
      }
   }

   if (!isAllRight(stepList1)) return false;
   if (!drawStepList(stepList1)) return false;
   emit addStepList(cleanStepList(stepList1));

   nidBegin=nid;
   sidBegin=sid;
   stepList0=stepList1;
   start++;

   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

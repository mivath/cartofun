/*
 - Date : 20/03/2012
 - Nom : routedlg.h
*/

#ifndef ROUTEDLG_H
#define ROUTEDLG_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "global.h"

const qreal MARGIN=37.0,DELTA=11.0;
const int ZOOM=13;

////////////////////////////////////////////////////////////////////////////////////////////////////

class Slippy
{
   public:
      Slippy();
      Slippy(const QRectF& rect);
      void init(const QRectF& rect);
      void init(const QRectF& rect,QPair<int,int>& xPair,QPair<int,int>& yPair);
      double lon2ImageX(double lon);
      double lat2ImageY(double lat);
      double imageX2Lon(double imageX);
      double imageY2Lat(double imageY);
      QRect& extract(QRect& rect);
      QSize getImageSize();

   private:
      double $scale;
      QRectF $rect;
      QPointF $tile00,$leftTop;

      double lon2TileX(double lon);
      double lat2TileY(double lat);
      double lon2MosaicX(double lon);
      double lat2MosaicY(double lat);
      double tileX2Lon(double x);
      double tileY2Lat(double y);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class Node : public QObject, public QGraphicsEllipseItem
{
   Q_OBJECT

   public:
      Node(const QRectF& rect,QGraphicsItem* parent=0);
      QPointF center() {return $center;}
      void reset();

   private:
      QPointF $initPos,$center;

   protected:
      QVariant itemChange(GraphicsItemChange change,const QVariant& value);

   signals:
      void refresh();
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class GraphicsView : public QGraphicsView
{
   protected:
      QGraphicsScene* $graphicsScene;

      GraphicsView(qreal w,qreal h,QWidget* parent=0);
      void horizontalLabel(const QString& text,qreal y);
      void verticalLabel(const QString& text,qreal x);
      int getTypeItemCount(int type);                    // debogage
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class SlopeTab : public GraphicsView
{
   Q_OBJECT

   public:
      SlopeTab(qreal w,qreal h,const QVector<qreal>& range,QVector<QPointF>& posVector);  // do not specify parent widgets
      ~SlopeTab();

   private:
      QVector<Node*> $nodeVector;
      QPolygonF $polygon;
      QGraphicsPathItem* $gpi;

      void catmullRomToBezier(QPainterPath& path);
      void drawGrid(const QVector<qreal>& range);

   protected:
      void keyPressEvent(QKeyEvent* event);
      void leaveEvent(QEvent*);

   public slots:
      void refresh();

   signals:
      void polygon(QPolygonF* polygon);
      void nodePos(QVector<QPointF>& posVector);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class RouteTab : public GraphicsView
{
   Q_OBJECT

   public:  // do not specify parent widgets (et pour la destruction?)
      RouteTab(qreal w,qreal h,const QVector<qreal>& range,qreal thickness,Slippy* slippy,double pixelScale,
            const QMap<qreal,QVector3D>& ramp,qreal statMin,qreal statMax,const QColor& pathColor);
      void init(const QString& msg=QString());
      void refresh();
      void setStats(qreal statMin,qreal statMax);
      void setData(const QVector<QVector3D>& dlgVector,const QMap<int,quint16>& forkMap=QMap<int,quint16>());
      bool getIndexData(QVector3D& indexData);

   private:
      enum Side {LEFT,RIGHT};

      qreal $thickness,$statMin,$statMax;
      int $hMin,$hMax;
      qreal $length;
      QLinearGradient $gradient;
      QPainterPath $path;
      int $index,$indexH;
      QVector<QVector3D> $dlgVector;
      QVector<qreal> $lengthVector,$slopeVector;
      QPolygonF* $polygon;
      QVector<qreal> $range;
      QColor $pathColor;
      Slippy* $slippy;
      double $pixelScale;
      QMap<int,quint16> $forkMap;

      void moreInfo(qreal elevation,qreal length);
      void printxy(Side x,int y,const char* fmt,...);
      qreal f(qreal x);
      void getLength();
      void duration();
      void minMaxSlope(qreal& slopeMin,qreal& slopeMax);
      void goToNearestFork(Side direction);
      bool isActive();

   protected:
      void keyPressEvent(QKeyEvent* event);
      void mousePressEvent(QMouseEvent* event);    // ne delivre que des positions "entieres"

   private slots:
      void polygon(QPolygonF* polygon);

   signals:
      void update();
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class RouteDlg : public QWidget
{
   Q_OBJECT

   public:
      RouteDlg(qreal thickness,const QRectF& rect,double pixelScale,const QMap<qreal,QVector3D>& ramp,
            QVector<QPointF>& posVector,qreal statMin,qreal statMax,QWidget* parent=0);
      ~RouteDlg();
      QWidget* page(int index) {return $tabs->widget(index);}
      void center();
      void setTabText(int index,const QString& label);
      void setCurrentIndex(int index);

   private:
      QTabWidget* $tabs;
      Slippy* $slippy;

   protected:
      void keyPressEvent(QKeyEvent* event);
      void closeEvent(QCloseEvent*);
      void showEvent(QShowEvent*);

   signals:
      void closeRouteDlg();
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif

/*
 - Date : 23/06/2012
 - Nom : world.h
*/

#ifndef WORLD_H
#define WORLD_H

#include <QtGui>
#include <QtWidgets>

#include <JlCompress.h>

#include <gdal_alg.h>

#include "global.h"
#include "gdalwarp.h"
#include "gdaltranslate.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

class WorldScene : public QGraphicsScene
{
   Q_OBJECT

   public:
      WorldScene(const QString& regionDir,const QString& imgFile,QObject* parent);
      QPoint pos() {return $worldSelector->scenePos().toPoint();}
      void moveTo(QPoint pos);
      void repaintRegion(QRgb=0);

   private:
      QImage $img;
      QMap<QRgb,QString> $continent;
      QGraphicsRectItem* $worldSelector;
      QGraphicsPixmapItem* $pixmapItem;

      void paintCacheRegions(const QString& regionDir);

   protected:
      void mousePressEvent(QGraphicsSceneMouseEvent* event);
      void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event);
      void keyPressEvent(QKeyEvent* event);

   signals:
      void mousePos(QPointF pos,const QString& continent);
      void getRegion();
      void delRegion(bool& rslt);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class ColorShade
{
   public:
      ColorShade(const QString& src,const QString& dst,const QMap<qreal,QVector3D>& ramp,float& statMin,float& statMax);

   private:
      QMap<qreal,QVector3D> $ramp;
      int $nXSize,$nYSize;
      float* $inPixels;

      QVector3D getColor(float percent);
      void getCell(int j,int i,float* win);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class World
{
   public:
      World(const QByteArray& array,const QString& regionDir,const QString& regionName,
            const QMap<qreal,QVector3D>& ramp,QTime* clock);

   private:
      void unzip(const QString& src,const QString& dstDir);
      void hgtToTif(const QString& src,const QString& dst);
      void voidFill(const QString& src);
      void warp(const QString& src,const QString& dst);
      void greyScale(const QString& src,const QString& dst,int statMin,int statMax);
      void metaData(const QString& src,const QString& dst,int statMin,int statMax);
      void elapsedTime(const char* section,QTime* clock,bool init=false);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif

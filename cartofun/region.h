/*
 - Date : 12/05/2011
 - Nom : region.h
*/

#ifndef REGION_H
#define REGION_H

#include <QtGui>
#include <QtWidgets>

#include "global.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

class RegionSelector : public QObject, public QGraphicsPathItem
{  // "QGraphicsPathItem" n'herite pas de "QObject", l'heritage multiple est donc necessaire
   Q_OBJECT

   public:
      enum {Type=UserType+1};

      RegionSelector();
      void setColors(bool toggle);
      int type() const {return Type;}

   private:
      QPainterPath $path;
      QColor $enabled,$disabled;

      void setMyPath();
      void paint(QPainter* painter,const QStyleOptionGraphicsItem* option,QWidget* widget=0);
      bool tabTerrains();

   protected:
      QVariant itemChange(GraphicsItemChange change,const QVariant& value);
      void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event);
      void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
      void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);
      void keyPressEvent(QKeyEvent* event);
      bool sceneEvent(QEvent* event);

   signals:
      void selectedPos(QPointF pos);
      void getTile(QPointF pos);
      void getBack();
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class RegionScene : public QGraphicsScene
{
   Q_OBJECT

   public:
      RegionScene(QObject* parent);
      void setHighestPos(QPointF pos);
      void setPixmap(const QString& imgFile,const QList<QPoint>& cornerList=QList<QPoint>(),const QPoint& pos=QPoint(-1,-1));
      void addCorner(QPoint p,bool check=false);

   private:
      QGraphicsPixmapItem* $pixmapItem;
      QGraphicsRectItem* $highestPos;
      RegionSelector* $regionSelector;

      void removeCorner(bool deleted);
      void goToHighestPos();

   protected:
      void keyPressEvent(QKeyEvent* event);

   signals:
      void delTile(QPointF pos,bool& deleted);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif

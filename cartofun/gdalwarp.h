/******************************************************************************
 *
 * Project:  High Performance Image Reprojector
 * Purpose:  Test program for high performance warper API.
 * Author:   Frank Warmerdam <warmerdam@pobox.com>
 *
 ******************************************************************************
 * Copyright (c) 2002, i3 - information integration and imaging
 *                          Fort Collin, CO
 * Copyright (c) 2007-2015, Even Rouault <even dot rouault at mines-paris dot org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

#ifndef GDALWARP_H
#define GDALWARP_H


#include "cpl_string.h"
#include "cpl_error.h"
#include "gdal_version.h"
#include "commonutils.h"
#include "gdal_utils_priv.h"

#include <vector>

////////////////////////////////////////////////////////////////////////////////////////////////////

class ErrorStruct
{
   public:
      CPLErr type;
      CPLErrorNum no;
      CPLString msg;

      ErrorStruct() : type(CE_None), no(CPLE_None) {}
      ErrorStruct(CPLErr eErrIn, CPLErrorNum noIn, const char* msgIn) : type(eErrIn), no(noIn), msg(msgIn) {}
};

//////////////////////////////////////////////////

class GdalWarp
{
   public:
      GdalWarp(int argc,char* argv[],int& rtrn);

   private:
      int GDALExit( int nCode );
      void Usage(const char* pszErrorMsg = nullptr);
      GDALWarpAppOptionsForBinary *GDALWarpAppOptionsForBinaryNew(void);
      void GDALWarpAppOptionsForBinaryFree( GDALWarpAppOptionsForBinary* psOptionsForBinary );
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif

/*
 - Date : 16/01/2018
 - Nom : main.h
*/

#ifndef MAIN_H
#define MAIN_H

#include <QtNetwork>
#include <QtWebKitWidgets>
#include <QSound>

#include "world.h"
#include "region.h"
#include "viewer.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

struct Config {
   QPoint posRegion;
   QPoint posTile;
   QVector<QPointF> posVector;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class GoogleMap : public QObject
{
   Q_OBJECT

   public:
      GoogleMap();
      void execute(const QUrl& url,const QString& destFile);

   private:
      QString $destFile;
      QWebPage $page;

      int getValue(const QWebElementCollection& collection,const QString id);

   private slots:
      void titleChanged(const QString& newTitle);

   signals:
      void finished();
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class OsmMap : public QObject
{  // d'apres "Examples\Qt-5.5\network\download", et la page : http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
   Q_OBJECT

   public:
      OsmMap();
      void execute(const QPointF& lonwLatn,const QPointF& loneLats,const QString& destFile);

   private:
      QNetworkAccessManager $manager;
      QList<QNetworkReply*> $currentDownloads;
      QPair<int,int> $xPair,$yPair;
      QString $destFile;
      QImage $mosaic;
      Slippy $slippy;

      void download(const QUrl& url);
      void assemble(QNetworkReply* reply);
      void abort(const char* fmt,...);
      void extract();

   public slots:
      void downloadFinished(QNetworkReply* reply);
      void sslErrors(const QList<QSslError>& errors);

   signals:
      void osmFinished(const QString& msg);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class RegionThread : public QThread
{  // d'apres https://github.com/DeiVadder/QtThreadExample
   Q_OBJECT

   public:
      RegionThread(const QByteArray& array,const QString& regionDir,const QString& regionName,
         const QMap<qreal,QVector3D>& ramp,QTime* clock,QObject* parent=nullptr);

   private:
      QByteArray $array;
      QString $regionDir,$regionName;
      QMap<qreal,QVector3D> $ramp;
      QTime* $clock;

   protected:
      virtual void run() override;

   signals:
      void operationDone(bool rslt);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class Window : public QMainWindow
{
   Q_OBJECT

   public:
      Window(QWidget* parent=0);
      ~Window();

   private:
      enum Mode {WORLD,REGION,TERRAIN};
      enum Flags {GREYSCALE,BOTTOMLEFT,DMSDISPLAY,FORCEUPDATE,KEEPXMLFILE,TOGGLEBRANCH};

      quint32 $width,$height;
      QList<QLabel*> $label;
      QFile $regionFile;
      uchar* $regionData;
      QStackedWidget* $stackedWidget;
      WorldScene* $worldScene;
      RegionScene* $regionScene;
      Viewer* $viewer;
      Transform* $transform;
      UnTransform* $unTransform;
      QString $resourcesDir,$regionDir,$terrainDir,$continent,$regionName,$configFile,$download;
      QMap<qreal,QVector3D> $ramp;
      Config $config;
      GoogleMap* $googleMap;
      OsmMap* $osmMap;
      quint16 $flags;
      QTime $clock;
      QTimer* $timer;

      void loadRegionFile(const QString& regionFile,QString& metaData);
      qint16 getElevation(int x,int y);
      int getHighest(QPointF pos);
      void initRegion();
      QString getFileName(const QString& download);
      bool fileExist(const QString& download);
      void loadRampFile(const QString& rampFile);
      void loadConfigFile();
      void msgBox(const QString& title,const QString& msg);
      void initViewer();
      void scanCache(QList<QPoint>& cornerList);
      void scanCache(QStringList& fileList);
      QPointF getLonLat(const QPointF& pos);
      void refresh(const QString& arg);
      void imgPathToClipBoard();
      void saveRectFile();
      void checkData();
      QString& coord2text(qreal dd,bool isLat);

      void createMenus(const QIcon& icon);
      QAction* createAction(const QString& text,const QString& key,const QObject* receiver,const char* slot,
            int index=0,bool enabled=true);
      void updateAction(const QString& title,const QString& text,void (QAction::*setValue)(bool),bool value);
      QString getRadioChecked(const QString& title);
      void enableMenusContent(const QStringList& title,bool value);
      void setActionFontWeight();

      void networkRequest(const QString& download);
      void initNetworkAcess(QUrl url);
      QUrl getRegionUrl();
      QUrl getGoogleUrl(QPointF& lonwLatn,QPointF& loneLats);
      QUrl getXmlUrl(QPointF& lonwLatn,QPointF& loneLats);

      uchar getBit(int pos);
      void setBit(int pos,uchar bit);
      void invBit(int pos);

   private slots:
      void replyFinished(QNetworkReply* reply);
      void downloadFinished(QNetworkReply* reply);
      void osmFinished(const QString& msg);
      void finished();
      void selectedPos(QPointF pos);
      void getTile(QPointF pos);
      void delTile(QPointF pos,bool& deleted);
      void changeTexture();
      void mousePos(QPointF pos,const QString& continent);
      void getRegion();
      void delRegion(bool& rslt);
      void getBack();
      void openAboutBox();
      void saveGeotiffTile();
      void saveGpsPath();
      void saveConfigFile();
      void handleOptions();
      void getOptionState(const QString& option,bool& state);
      void resetOptions(const QStringList& options=QStringList());
      void handleFlags();
      void timeout();
      void operationDone(bool rslt);

   protected:
      void keyPressEvent(QKeyEvent* event);
      void closeEvent(QCloseEvent* event);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif

/*
 - Date : 08/04/2020
 - Nom : main.cpp
*/

#include "main.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void OsmMap::sslErrors(const QList<QSslError>& sslErrors)
{
   for (const QSslError& error: sslErrors) fprintf(stderr,"SSL error: %s\n",qPrintable(error.errorString()));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void OsmMap::abort(const char* fmt,...)
{
   va_list argptr;
   str256 msg;

   va_start(argptr,fmt);
   vsprintf(msg,fmt,argptr);
   va_end(argptr);

   emit osmFinished(QString(msg));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void OsmMap::extract()
{
   QRect rect;

   QImage img=$mosaic.copy($slippy.extract(rect));

   img.save($destFile,"JPG",100);

   qDebug("OsmMap::extract/1 : x=%i  y=%i  w=%i  h=%i",rect.left(),rect.top(),rect.width(),rect.height());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void OsmMap::assemble(QNetworkReply* reply)
{
   QImage tile;
   if (!tile.loadFromData(reply->readAll())) abort("OsmMap::assemble,QImage::loadFromData failed");

   QString subStr=reply->url().toString().section('.',-2,-2);
   int x=subStr.section('/',-2,-2).toInt(),y=subStr.section('/',-1).toInt();
   QPainter painter;

   painter.begin(&$mosaic);
   painter.drawImage(QPoint((x-$xPair.first)*256,(y-$yPair.first)*256),tile);
   painter.end();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void OsmMap::downloadFinished(QNetworkReply* reply)
{
   if (reply->error()) abort("OsmMap::downloadFinished,%s",qPrintable(reply->errorString()));
   else assemble(reply);

   $currentDownloads.removeAll(reply);
   reply->deleteLater();

   if ($currentDownloads.isEmpty()) {
      extract();
      emit osmFinished("");
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void OsmMap::download(const QUrl& url)
{
   QNetworkRequest request(url);
   request.setRawHeader("User-Agent","Fake Browser/1.0(linux-gnu)");
   QNetworkReply* reply=$manager.get(request);

   connect(reply,SIGNAL(sslErrors(QList<QSslError>)),this,SLOT(sslErrors(QList<QSslError>)));

   $currentDownloads.append(reply);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

OsmMap::OsmMap()
{
   connect(&$manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(downloadFinished(QNetworkReply*)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void OsmMap::execute(const QPointF& lonwLatn,const QPointF& loneLats,const QString& destFile)
{
   $destFile=destFile;

   $slippy.init(QRectF(lonwLatn,loneLats),$xPair,$yPair);

   if ($xPair.first>$xPair.second) abort("OsmMap::execute/1,incorrect x values");
   if ($yPair.first>$yPair.second) abort("OsmMap::execute/2,incorrect y values");

   $mosaic=QImage(($xPair.second-$xPair.first+1)*256,($yPair.second-$yPair.first+1)*256,QImage::Format_RGB32);
   QString baseUrl="http://tile.openstreetmap.org/"+QString::number(ZOOM)+"/";

   for (int y=$yPair.first; y<=$yPair.second; y++)       // north -> south
      for (int x=$xPair.first; x<=$xPair.second; x++)    // west  -> east
         download(QUrl(baseUrl+QString::number(x)+"/"+QString::number(y)+".png"));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

GoogleMap::GoogleMap()
{
   connect($page.mainFrame(),SIGNAL(titleChanged(const QString&)),this,SLOT(titleChanged(const QString&)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void GoogleMap::execute(const QUrl& url,const QString& destFile)
{
   $destFile=destFile;

   $page.mainFrame()->load(url);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void GoogleMap::titleChanged(const QString& newTitle)
{
   if (newTitle=="ko") {
      qDebug("GoogleMap::titleChanged/1");
      emit finished();
      return;
   }
   else if (newTitle!="ok") return;

   // success case : (newTitle=="ok")
   $page.setViewportSize($page.mainFrame()->contentsSize());
   QImage image($page.viewportSize(),QImage::Format_RGB32);
   QPainter painter;

   if (!painter.begin(&image)) qFatal("qFatal GoogleMap::titleChanged/2");

   $page.mainFrame()->render(&painter);
   if (!painter.end()) qFatal("qFatal GoogleMap::titleChanged/3");

   // extract
   QWebElement document=$page.mainFrame()->documentElement();
   QWebElementCollection collection=document.findAll("input");

   int x=getValue(collection,"x"),y=getValue(collection,"y"),w=getValue(collection,"w"),h=getValue(collection,"h");

   qDebug("GoogleMap::titleChanged/4 : x=%i  y=%i  w=%i  h=%i",x,y,w,h);

   image=image.copy(x,y,w,h);
   if (!image.save($destFile,"JPG",100)) qFatal("qFatal GoogleMap::titleChanged/5");

   emit finished();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

int GoogleMap::getValue(const QWebElementCollection& collection,const QString id)
{
   for (QWebElement we: collection)
      if (we.hasAttribute("type") && we.hasAttribute("id") && we.hasAttribute("value") &&
            we.attribute("type")=="hidden" && we.attribute("id")==id) return we.attribute("value").toInt();

   return -1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

RegionThread::RegionThread(const QByteArray& array,const QString& regionDir,const QString& regionName,
      const QMap<qreal,QVector3D>& ramp,QTime* clock,QObject* parent) :
   QThread(parent),$array(array),$regionDir(regionDir),$regionName(regionName),$ramp(ramp),$clock(clock)
{}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionThread::run()
{
   World($array,$regionDir,$regionName,$ramp,$clock);
   emit operationDone(true);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

uchar Window::getBit(int pos)
{
   return ($flags>>pos)& 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::setBit(int pos,uchar bit)
{
   $flags=($flags&~(1<<pos))|(bit<<pos);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::invBit(int pos)
{
   setBit(pos,1-getBit(pos));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::loadConfigFile()
{  // lecture : le fichier est cense exister
   QFile file($configFile);

   if (!file.open(QIODevice::ReadOnly)) qFatal("qFatal Window::loadConfigFile/1");

   QDataStream stream(&file);    // read the data serialized from the file
   stream >> $config.posRegion;
   stream >> $config.posTile;
   stream >> $config.posVector;
   file.close();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::saveConfigFile()
{  // ecriture
   QFile file($configFile);
   if (!file.open(QIODevice::WriteOnly)) qFatal("qFatal Window::saveConfigFile/1");
   QDataStream stream(&file);                      // we will serialize the data into the file

   $config.posRegion=$worldScene->pos();
   $config.posTile=$transform->position().toPoint();
   if ($viewer) $config.posVector=$viewer->posVector();

   stream << $config.posRegion;
   stream << $config.posTile;
   stream << $config.posVector;
   file.close();

   QMessageBox::information(this,"information","File<br />"+$configFile+"<br />successfully saved!<br />");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::saveGeotiffTile()
{
   static QString lastDir=QString();

   QString
      srcFile=$regionDir+$regionName+".tif",
      dstFile=QFileDialog::getSaveFileName(this,"save geotiff",
            lastDir+$transform->preset().section('/',-1)+".tif","Images (*.tif)");

   if (dstFile.isEmpty()) return;
   if (!dstFile.endsWith(".tif",Qt::CaseInsensitive)) dstFile+=".tif";
   lastDir=dstFile.section('/',0,-2)+'/';

   const char* arg[]={
      "exe_name",          // 0
      "-srcwin",           // 1
      "xoff",              // 2
      "yoff",              // 3
      "xsize",             // 4
      "ysize",             // 5
      "src_dataset",       // 6
      "dst_dataset",       // 7
      "-q"                 // 8 : mode silencieux
   };

   str256 xoff,yoff,xsize,ysize,src_dataset,dst_dataset;
   int rtrn=0;
   QPoint position=$transform->position().toPoint();

   sprintf(xoff,"%i",position.x());
   sprintf(yoff,"%i",position.y());
   sprintf(xsize,"%i",DIM);
   sprintf(ysize,"%i",DIM);

   arg[2]=xoff;
   arg[3]=yoff;
   arg[4]=xsize;
   arg[5]=ysize;
   arg[6]=strcpy(src_dataset,qPrintable(srcFile));
   arg[7]=strcpy(dst_dataset,qPrintable(dstFile));

   GdalTranslate(sizeof(arg)/sizeof(char*),(char**)arg,rtrn);

   QMessageBox::information(this,"information","File<br />"+dstFile+"<br />successfully saved!<br />");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::saveRectFile()
{
   static QString lastDir=QString();

   QString dstFile=QFileDialog::getSaveFileName(this,"save rect file",
         lastDir+$transform->preset().section('/',-1)+".rect","(*.rect)");

   if (dstFile.isEmpty()) return;
   if (!dstFile.endsWith(".rect",Qt::CaseInsensitive)) dstFile+=".rect";
   lastDir=dstFile.section('/',0,-2)+'/';

   QFile file(dstFile);
   if (!file.open(QIODevice::WriteOnly)) qFatal("qFatal Window::saveRectFile/1");
   QDataStream stream(&file);
   stream << $transform->getRect();
   file.close();
   QMessageBox::information(this,"information","File<br />"+dstFile+"<br />successfully saved!<br />");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::loadRampFile(const QString& rampFile)
{
   QFile file(rampFile);
   QStringList list;
   QString line,s;
   qreal key;

   if (!file.open(QIODevice::ReadOnly|QIODevice::Text)) qFatal("qFatal Window::loadRampFile/1");

   QTextStream stream(&file);

   while (!stream.atEnd()) {
      line=stream.readLine();
      if (line.at(0)==QChar('#')) continue;
      list=line.split(QRegExp("\\s+"));
      s=list.at(0);
      if (!s.endsWith(QChar('%')) || list.count()!=4) continue;
      key=s.mid(0,s.count()-1).toFloat()/100.0;
      if (key<0.0 || key>1.0) qFatal("qFatal Window::loadRampFile/2");
      $ramp.insert(key,QVector3D(list.at(1).toFloat(),list.at(2).toFloat(),list.at(3).toFloat()));
   }
   if (!$ramp.contains(0.0) || !$ramp.contains(1.0)) qFatal("qFatal Window::loadRampFile/3");

   file.close();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::refresh(const QString& arg)
{
   setActionFontWeight();
   $viewer->refresh(arg);
   $regionScene->addCorner($transform->position().toPoint()+QPoint(0,DIM-1),true);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QPointF Window::getLonLat(const QPointF& pos)
{
   QPointF lonLat=$transform->pixelToLonLat(pos,true);
   QString s;

   s.sprintf("%8.6f",lonLat.x());
   lonLat.setX(s.toDouble());

   s.sprintf("%8.6f",lonLat.y());
   lonLat.setY(s.toDouble());

   return lonLat;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::scanCache(QList<QPoint>& cornerList)
{  // "get" version
   QPointF
      min=getLonLat(QPointF(0,$height)),                 // bottom-left
      max=getLonLat(QPointF($width-DIM,DIM));            // top-right
   double minLon=min.x(),minLat=min.y(),maxLon=max.x(),maxLat=max.y();
   QDir dir($terrainDir);
   QStringList entry=dir.entryList({"*.jpg","*.fun"},QDir::Files,QDir::Name),list;
   QString current,prior="";
   QChar cy=$regionName.at(0),cx=$regionName.at(3);
   char signy=(cy=='N')? 1 : -1,signx=(cx=='E')? 1 : -1;
   double lonw,lats;
   int i,n=entry.count();

   for (i=0; i<n && entry[i].at(0)!=cy; i++);

   for (int j=i; j<n && entry[j].at(0)==cy; j++) {
      list=entry[j].section('.',0,-2).split('_');
      if (list[1].at(0)!=cx) continue;

      current=list[0]+'_'+list[1];
      if (current==prior) continue;

      prior=current;
      lats=list[0].mid(1).toDouble()*signy;
      lonw=list[1].mid(1).toDouble()*signx;

      if (((minLat<lats && lats<maxLat)||qFuzzyCompare(lats,minLat)||qFuzzyCompare(lats,maxLat))&&
            ((minLon<lonw && lonw<maxLon)||qFuzzyCompare(lonw,minLon)||qFuzzyCompare(lonw,maxLon))) {
         const QPoint& p=$unTransform->lonLatToPixel(QPointF(lonw,lats)).toPoint();
         cornerList.append(p);
         qDebug("Window::scanCache/1 : %s(%i,%i)",qPrintable(current),p.x(),p.y());
      }
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::scanCache(QStringList& fileList)
{  // "del" version
   QPointF
      min=getLonLat(QPointF(0,$height)),                 // bottom-left
      max=getLonLat(QPointF($width-DIM,DIM));            // top-right
   double minLon=min.x(),minLat=min.y(),maxLon=max.x(),maxLat=max.y();

   QDir dir($terrainDir);
   QStringList entry=dir.entryList({"*.jpg","*.fun","*.xml"},QDir::Files,QDir::Name),list;
   QString current,ok="",ko="";
   QChar cy=$regionName.at(0),cx=$regionName.at(3);
   char signy=(cy=='N')? 1 : -1,signx=(cx=='E')? 1 : -1;
   double lonw,lats;
   int i,n=entry.count();

   for (i=0; i<n && entry[i].at(0)!=cy; i++);

   for (int j=i; j<n && entry[j].at(0)==cy; j++) {

      list=entry[j].section('.',0,-2).split('_');
      if (list[1].at(0)!=cx) continue;

      current=list[0]+'_'+list[1];
      if (current==ok) {
         fileList.append(entry[j]);
         continue;
      }
      else if (current==ko) continue;

      lats=list[0].mid(1).toDouble()*signy;
      lonw=list[1].mid(1).toDouble()*signx;

      if (((minLat<lats && lats<maxLat)||qFuzzyCompare(lats,minLat)||qFuzzyCompare(lats,maxLat))&&
            ((minLon<lonw && lonw<maxLon)||qFuzzyCompare(lonw,minLon)||qFuzzyCompare(lonw,maxLon))) {
         fileList.append(entry[j]);
         ok=current;
      }
      else ko=current;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::delTile(QPointF pos,bool& deleted)
{  // sur le modele de Window::delRegion
   $transform->setPosition(pos);

   QDir dir($terrainDir);
   QRegExp rx('^'+$transform->preset().section('/',-1));
   QStringList entry=dir.entryList({"*.jpg","*.fun","*.xml"},QDir::Files,QDir::Name).filter(rx);

   if (entry.isEmpty() || rx.pattern()=="^N42.423655_E2.333726") {
      qDebug("Window::delTile/1 : valid tile not selected");
      return;     // protection de la tuile du Canigou, necessaire pour comprendre la doc
   }

   QString msg="Are you ready to delete these files ? :\n\n";
   QString terrainDirLight=$terrainDir.section('/',-3);

   for (const QString& s: entry) msg+=" - "+terrainDirLight+s+'\n';

   if (QMessageBox::question(this,"question",msg)==QMessageBox::No) return;

   msg.clear();
   for (const QString& s: entry) {
      QString fullName=$terrainDir+s;
      bool removed=QFile::remove(fullName);
      qDebug("Window::delTile/2 : deleting file \"%s\" : %s",qPrintable(fullName),(removed)? "success!" : "failure...");
      if (!removed) msg+=" - "+terrainDirLight+s+'\n';
   }

   if (!msg.isEmpty()) msgBox("Window::delTile/3","Failure deleting file(s) :\n\n"+msg);
   deleted=true;  // ambiguite si l'echec concerne tous les fichiers (mais est-ce vraiment possible?)
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::delRegion(bool& rslt)
{
   QString s=$regionDir+$regionName;

   if ($label[4]->text()=="?" || $regionName=="N42E002" ||
            !(QFile::exists(s+".tif")&& QFile::exists(s+"_colorshade.tif")&& QFile::exists(s+"_greyscale.tif"))) {
      qDebug("Window::delRegion/1 : valid region not selected");
      return;     // zones interdites, mers, oceans, region du Canigou et regions sans donnees
   }

   QStringList fileList={$regionName+".tif",$regionName+"_colorshade.tif",$regionName+"_greyscale.tif"};
   QString metaData="delRegion";

   loadRegionFile($regionDir+$regionName+".tif",metaData);
   scanCache(fileList);

   QString msg="Are you ready to delete these files ? :\n\n";
   QString regionDirLight=$regionDir.section('/',-3),terrainDirLight=$terrainDir.section('/',-3);

   for (int i=fileList.count()-1; i>=0; i--) msg+=" - "+((i<3)? regionDirLight : terrainDirLight)+fileList.at(i)+'\n';

   if (QMessageBox::question(this,"question",msg)==QMessageBox::No) return;

   msg.clear();

   for (int i=fileList.count()-1; i>=0; i--) {  // en partant de la fin
      QString fullName= ((i<3)? $regionDir : $terrainDir)+fileList.at(i);
      bool removed=QFile::remove(fullName);
      qDebug("Window::delRegion/2 : deleting file \"%s\" : %s",qPrintable(fullName),(removed)? "success!" : "failure...");
      if (!removed) msg+=" - "+fullName.section('/',-3)+'\n';
   }

   if (!msg.isEmpty()) msgBox("Window::delRegion/3","Failure deleting file(s) :\n\n"+msg);

   rslt=true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::initRegion()
{
   QString metaData,configRegion;
   QList<QPoint> cornerList;
   int lon=$config.posRegion.x()/12-180,lat=(179-$config.posRegion.y()/12)-90;
   configRegion.sprintf("%c%02i%c%03i",(lat>=0)? 'N' : 'S',qAbs(lat),(lon>=0)? 'E' : 'W',qAbs(lon));
   QPoint pos=(configRegion==$regionName)? $config.posTile : QPoint();

   loadRegionFile($regionDir+$regionName+".tif",metaData);
   scanCache(cornerList);

   QString background= (getBit(GREYSCALE))? "greyscale" : "colorshade";

   if (!$regionScene) {
      $regionScene=new RegionScene(this);
      QGraphicsView* regionView=new QGraphicsView($regionScene,this);
      regionView->setGeometry(0,0,640,480);
      $regionScene->setPixmap($regionDir+$regionName+"_"+background+".tif",cornerList,pos);
      $stackedWidget->addWidget(regionView);
   }
   else {
      $regionScene->setPixmap($regionDir+$regionName+"_"+background+".tif",cornerList,pos);
      $stackedWidget->setCurrentIndex(REGION);
   }

   if (!$viewer) {
      $viewer=new Viewer($ramp,$config.posVector,!(getBit(TOGGLEBRANCH)),$transform,this);
      $viewer->setGeometry(0,0,640,480);

      $stackedWidget->addWidget($viewer);
      $stackedWidget->setCurrentIndex(REGION);
   }

   $viewer->queryMetaData(metaData);
   selectedPos(pos);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::loadRegionFile(const QString& regionFile,QString& metaData)
{
   static const quint16             // tags recherches
      IMAGE_WIDTH=0x0100,
      IMAGE_LENGTH=0x0101,
      STRIPS_OFFSET=0x0111,
      MODEL_PIXEL_SCALE=0x830E,
      MODEL_TIEPOINT=0x8482,
      GDAL_METADATA=0xA480;

   struct imageFileHeader {         // 8 octets
      quint16 byteOrder;
      quint16 version;
      quint32 firstIFD;
   };

   struct imageFileDirectory {      // 12 octets
      quint16 tagID;
      quint16 fieldType;
      quint32 numberOfValues;
      quint32 valueOffset;          // la plupart du temps une adresse
   };

   imageFileHeader ifh;
   imageFileDirectory ifd;
   quint16 i,dirCount;
   quint32 dataOffset;
   quint32 offset=0,modelTiepointLen=0,modelTiepointOfs=0,modelPixelScaleLen=0,modelPixelScaleOfs=0,
         gdalMetadataLen=0,gdalMetadataOfs=0;
   double modelPixelScale[3],modelTiepoint[6];

   $width=$height=0;

   $regionFile.setFileName(regionFile);
   if (!$regionFile.open(QIODevice::ReadOnly)) qFatal("qFatal Window::loadRegionFile/1");
   if ($regionFile.read((char*)&ifh,sizeof(ifh))<=0) qFatal("qFatal Window::loadRegionFile/2");
   if (!$regionFile.seek(ifh.firstIFD)) qFatal("qFatal Window::loadRegionFile/3");
   if ($regionFile.read((char*)&dirCount,sizeof(dirCount))<=0) qFatal("qFatal Window::loadRegionFile/4");

   for (i=0; i<dirCount; i++) {
      if ($regionFile.read((char*)&ifd,sizeof(ifd))<=0) qFatal("qFatal Window::loadRegionFile/5");

      switch (ifd.tagID) {
         case IMAGE_WIDTH: $width=ifd.valueOffset;break;
         case IMAGE_LENGTH: $height=ifd.valueOffset;break;
         case STRIPS_OFFSET: offset=ifd.valueOffset;break;
         case MODEL_PIXEL_SCALE:
            modelPixelScaleLen=ifd.numberOfValues;
            modelPixelScaleOfs=ifd.valueOffset;
            break;
         case MODEL_TIEPOINT:
            modelTiepointLen=ifd.numberOfValues;
            modelTiepointOfs=ifd.valueOffset;
            break;
         case GDAL_METADATA:
            gdalMetadataLen=ifd.numberOfValues;
            gdalMetadataOfs=ifd.valueOffset;
            break;
      }
   }

   if (!$width || !$height || !offset || !modelPixelScaleOfs || modelPixelScaleLen!=3 || !modelTiepointOfs
         || modelTiepointLen!=6 || !gdalMetadataLen || !gdalMetadataOfs) qFatal("qFatal Window::loadRegionFile/6");

   // MODEL_PIXEL_SCALE ( -> Pixel Size)
   if (!$regionFile.seek(modelPixelScaleOfs)) qFatal("qFatal Window::loadRegionFile/7");
   if ($regionFile.read((char*)&modelPixelScale,sizeof(modelPixelScale))<=0) qFatal("qFatal Window::loadRegionFile/8");
   if (modelPixelScale[0]!=modelPixelScale[1]) qFatal("qFatal Window::loadRegionFile/9");

   // le "Pixel Size" de gdalinfo
   qDebug("Window::loadRegionFile : modelPixelScale=%f",modelPixelScale[0]);

   // MODEL_TIEPOINT ( -> Origin)
   if (!$regionFile.seek(modelTiepointOfs)) qFatal("qFatal Window::loadRegionFile/10");
   if ($regionFile.read((char*)&modelTiepoint,sizeof(modelTiepoint))<=0) qFatal("qFatal Window::loadRegionFile/11");

   if (metaData=="delRegion") $regionFile.unmap($regionData);     // provenance Window::delRegion
   else {                                                         // provenance Window::initRegion
      // STRIPS_OFFSET
      if (!$regionFile.seek(offset)) qFatal("qFatal Window::loadRegionFile/12");
      if ($regionFile.read((char*)&dataOffset,sizeof(dataOffset))<=0) qFatal("qFatal Window::loadRegionFile/13");
      if (!($regionData=$regionFile.map(dataOffset,$height*$width*2))) qFatal("qFatal Window::loadRegionFile/14");

      if (!$regionFile.seek(gdalMetadataOfs)) qFatal("qFatal Window::loadRegionFile/15");
      QByteArray array=$regionFile.read(gdalMetadataLen);
      if (array.isEmpty()) qFatal("qFatal Window::loadRegionFile/16");
      metaData=QString(array);
   }

   $regionFile.close();

   if (!$transform) $transform=new Transform($terrainDir);
   $transform->setModel(modelPixelScale,modelTiepoint);

   if (!$unTransform) $unTransform=new UnTransform;
   $unTransform->setModel(modelPixelScale,modelTiepoint);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::checkData()
{
   QString cacheDir=QCoreApplication::applicationDirPath()+"/../cache/";
   $regionDir=cacheDir+"region/";
   $terrainDir=cacheDir+"terrain/";
   $resourcesDir=QCoreApplication::applicationDirPath()+"/../resources/";

   QMap<QString,QStringList> data={
      {$regionDir,{     // necessaires pour comprendre la doc
         "N42E002.tif","N42E002_colorshade.tif","N42E002_greyscale.tif"
      }},
      {$resourcesDir,{
         "config.cfg","ramp.txt","world.gif","left_arrow.png","artichoke.gif",
         "google.html","speech_on.wav","gcs.csv","ellipsoid.csv"
      }},
      {$terrainDir,{    // necessaires pour comprendre la doc
         "N42.423655_E2.333726_roadmap.jpg","N42.423655_E2.333726_satellite.jpg","N42.423655_E2.333726_terrain.jpg",
         "N42.423655_E2.333726_osm.jpg","N42.423655_E2.333726.fun"
      }}
   };

   for (QMap<QString,QStringList>::const_iterator it=data.constBegin(); it!=data.constEnd(); it++) {
      const QString& folder=it.key();
      QDir dir(folder);
      if (!dir.exists()) qFatal("qFatal Window::checkData/1 : \"%s\" folder not found",qPrintable(folder));

      for (const QString& fileName : *it) if (!QFile::exists(folder+fileName))
         qFatal("qFatal Window::checkData/2 : \"%s\" file not found",qPrintable(folder+fileName));
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Window::Window(QWidget* parent) : QMainWindow(parent)
{
   $transform=0;
   $unTransform=0;
   $regionData=0;
   $regionScene=0;
   $viewer=0;
   $googleMap=new GoogleMap;
   $osmMap=new OsmMap;
   $flags=0;

   QWidget* centralWidget=new QWidget;

   checkData();

   if (!qputenv("GDAL_DATA",$resourcesDir.toUtf8())) qFatal("qFatal Window::Window/1");

   $configFile=$resourcesDir+"config.cfg";
   loadConfigFile();

   loadRampFile($resourcesDir+"ramp.txt");

   $continent="?";
   $worldScene=new WorldScene($regionDir,$resourcesDir+"world.gif",this);
   QGraphicsView* worldView=new QGraphicsView($worldScene,this);
   worldView->setGeometry(0,0,640,480);
   worldView->centerOn($config.posRegion);

   $stackedWidget=new QStackedWidget(this);
   $stackedWidget->addWidget(worldView);

   for (int i=0; i<5; i++) {
      $label.append(new QLabel(this));
      $label.last()->setStyleSheet((i!=2)?         // (i==2) : c'est le timer
            "font-family:MS Shell Dlg 2; font-size:12pt" : "font-family:MS Shell Dlg 2; font-size:12pt; font-weight:bold");
   }

   $timer=new QTimer(this);
   $label[2]->setText("000:0");

   QGridLayout* mainLayout=new QGridLayout;
   mainLayout->addWidget($stackedWidget,0,0,1,5);  // row de depart, col de depart, nb de rows verticalement, nb de cols horizontalement
   for (int i=0; i<$label.count(); i++) mainLayout->addWidget($label[i],1,i,Qt::AlignHCenter);
   centralWidget->setLayout(mainLayout);
   setCentralWidget(centralWidget);

   createMenus(QIcon($resourcesDir+"left_arrow.png"));

   connect($worldScene,SIGNAL(mousePos(QPointF,const QString&)),this,SLOT(mousePos(QPointF,const QString&)));
   connect($worldScene,SIGNAL(getRegion()),this,SLOT(getRegion()));
   connect($worldScene,SIGNAL(delRegion(bool&)),this,SLOT(delRegion(bool&)));
   connect($googleMap,SIGNAL(finished()),this,SLOT(finished()));
   connect($osmMap,SIGNAL(osmFinished(const QString&)),this,SLOT(osmFinished(const QString&)));
   connect($timer,SIGNAL(timeout()),this,SLOT(timeout()));

   $worldScene->moveTo($config.posRegion);

   setWindowIcon(QIcon($resourcesDir+"artichoke.gif"));
   setWindowTitle("CartoFun");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

int Window::getHighest(QPointF pos)
{
   int j,i,y,y0=int(pos.y()),x0=int(pos.x()),ym=0,xm=0;
   uchar* ptr;
   qint16 h,hmax=0;

   for (j=0,y=y0; j<DIM; j++,y++) {
      ptr=$regionData+2*(y*$width+x0);
      for (i=0; i<DIM; i++,ptr+=2) {
         h=*((qint16*)ptr);
         if (h>hmax) {
            hmax=h;
            xm=x0+i;
            ym=y;
         }
      }
   }
   $regionScene->setHighestPos(QPointF(xm,ym));
   return hmax;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

qint16 Window::getElevation(int x,int y)
{
   uchar* p=$regionData+2*(y*$width+x);

   return *((qint16*)p);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QString& Window::coord2text(qreal dd,bool isLat)
{  // "text.sprintf" est obsolete, mais on peut le remplacer s'il le faut par "text=QString::asprintf"
   static QString text;

   if (getBit(DMSDISPLAY)==0) {                 // default dd display
      if (isLat) text.sprintf("lat %8.6f%c",qAbs(dd),(dd>=0)? 'N' : 'S');
      else text.sprintf("lon %8.6f%c",qAbs(dd),(dd>=0)? 'E' : 'W');
   }
   else {                                       // dms display
      qreal absdd=qAbs(dd);
      int d = int(absdd);
      int m = int((absdd - qreal(d))*60.0);
      int s = qRound((absdd - qreal(d) - qreal(m)/60.0) * 3600.0);

      if (isLat) text.sprintf("lat %i\u00B0%02i'%02i\"%c",d,m,s,(dd>=0)? 'N' : 'S');
      else text.sprintf("lon %i\u00B0%02i'%02i\"%c",d,m,s,(dd>=0)? 'E' : 'W');
   }

   return text;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::selectedPos(QPointF pos)
{
   QString s;
   int center=(DIM-1)/2;                        // autrement dit 128
   QPointF p,q;

   if ($stackedWidget->currentIndex()==REGION) {// provenance : region
      if (pos==QPointF(-1,-1)) pos=$transform->position();
      p= (getBit(BOTTOMLEFT))? pos+QPointF(0,DIM-1) : pos+QPointF(center,center);
      $transform->setPosition(pos);
   }
   else {                                       // provenance : viewer
      if (pos==QPointF(-1,-1)) p=$transform->position()+QPointF(center,center);
      else p=$transform->position()+pos;
   }

   q=$transform->pixelToLonLat(p);

   $label[0]->setText(coord2text(q.y(),true));
   $label[1]->setText(coord2text(q.x(),false));
   $label[3]->setText(s.sprintf("h %im",getElevation(p.x(),p.y())));
   if ($stackedWidget->currentIndex()==REGION) $label[4]->setText(s.sprintf("hmax %im",getHighest(pos)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::mousePos(QPointF pos,const QString& continent)
{
   int lon=int(pos.x()/12)-180,lat=(179-int(pos.y()/12))-90;

   $regionName.sprintf("%c%02i%c%03i",(lat>=0)? 'N' : 'S',qAbs(lat),(lon>=0)? 'E' : 'W',qAbs(lon));
   $continent=continent;

   $label[0]->setText("lat "+QString::number(qAbs(lat))+((lat>=0)? "N" : "S"));
   $label[1]->setText("lon "+QString::number(qAbs(lon))+((lon>=0)? "E" : "W"));
   $label[3]->setText($regionName);
   $label[4]->setText($continent);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::getRegion()
{
   if ($label[4]->text()=="?") return;       // zones interdites, mers, oceans, etc...

   qDebug("Window::getRegion : $regionName=%s",qPrintable($regionName));
   QString s=$regionDir+$regionName;

   if (QFile::exists(s+".tif")&& QFile::exists(s+"_colorshade.tif")&& QFile::exists(s+"_greyscale.tif"))
         initRegion();                       // region "disque"
   else  networkRequest("region");           // region "reseau"
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::networkRequest(const QString& download)
{
   $download=download;
   $clock.start();
   $label[2]->setText("000:0");
   //$timer->start(100);
   $timer->start(89);   // tous les 1/10eme de seconde, avec correctif QtThreadExample\12_11_1.cpp

   QNetworkAccessManager* manager=new QNetworkAccessManager(this);
   connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(replyFinished(QNetworkReply*)));

   manager->get(QNetworkRequest(QUrl("https://www.google.fr")));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::replyFinished(QNetworkReply* reply)
{
   int error=reply->error();

   if (error) {         // generalement absence de connection internet ou blocage firewall
      $timer->stop();

      if ($download=="hiking") resetOptions({"hiking"});
      else if ($download!="region") updateAction("Texture",$viewer->oldTexture(),&QAction::setChecked,true);

       QMessageBox::critical(this,"critical","<center>Error num : "+QString::number(error)+"<br>"+reply->errorString()+"</center>");
   }
   else if ($download=="region") initNetworkAcess(getRegionUrl());
   else {
      QPointF lonwLatn,loneLats;
      $transform->getCorners(lonwLatn,loneLats);

      if ($download=="osm") $osmMap->execute(lonwLatn,loneLats,getFileName($download));
      else if ($download=="hiking") initNetworkAcess(getXmlUrl(lonwLatn,loneLats));
      else $googleMap->execute(getGoogleUrl(lonwLatn,loneLats),getFileName($download));
   }
   reply->deleteLater();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::initNetworkAcess(QUrl url)
{
   QNetworkAccessManager* manager=new QNetworkAccessManager(this);
   QNetworkRequest request(url);
   request.setRawHeader("User-Agent","Fake Browser/1.0(linux-gnu)");
   connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(downloadFinished(QNetworkReply*)));
   manager->get(request);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::osmFinished(const QString& msg)
{
   if (msg.isEmpty()) finished();
   else {                  // error
      updateAction("Texture",$viewer->oldTexture(),&QAction::setChecked,true);   // mise a jour du menu
      msgBox(msg.section(',',0,0),msg.section(',',1));   // separation du titre et du corps du message
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::downloadFinished(QNetworkReply* reply)
{
   if ($download=="region") {
      if (reply->error()) msgBox("Window::downloadFinished/1",reply->errorString());
      else {
         RegionThread* regionThread = new RegionThread(reply->readAll(),$regionDir,$regionName,$ramp,&$clock,this);

         connect(regionThread,SIGNAL(operationDone(bool)),this,SLOT(operationDone(bool)));
         connect(regionThread,SIGNAL(operationDone(bool)),regionThread,SLOT(deleteLater()));

         regionThread->start();
      }
   }
   else if ($download=="hiking") {
      if (reply->error()) {
         updateAction("Options","hiking",&QAction::setChecked,false);      // mise a jour du menu
         msgBox("Window::downloadFinished/2",reply->errorString());
      }
      else {
         const QByteArray& array=reply->readAll();
         if (array.size()<1024 || !array.contains("<node id=")) {
            updateAction("Options","hiking",&QAction::setChecked,false);   // mise a jour du menu
            msgBox("Window::downloadFinished/3","Sorry, no such data!");
         }  // en absence de donnees, un "fichier" xml est quand meme envoye (voir N45.037949_E80.354812.xml : 258 octets)
         else $viewer->xmlToFun(array,$terrainDir,(getBit(KEEPXMLFILE)),this);
      }
   }
   else qFatal("qFatal Window::downloadFinished/4");     // jamais execute

   reply->deleteLater();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::operationDone(bool rslt)
{
   if ($download=="region") {
      initRegion();
      $worldScene->repaintRegion(0xFF00FF00);                           // repeint en vert
   }
   else if ($download=="hiking") {
      if (rslt==false) {
         updateAction("Options","hiking",&QAction::setChecked,false);   // mise a jour du menu
         msgBox("Window::operationDone/1","Sorry, no such data!");
         return;
      }
   }
   else qFatal("qFatal Window::operationDone/1");                       // jamais execute

   finished();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::finished()
{
   $timer->stop();
   QSound::play($resourcesDir+"speech_on.wav");          // execution asynchrone?

   switch ($stackedWidget->currentIndex()) {
      case REGION:
         qDebug("Window::finished/1 : download region=%ims",$clock.elapsed());
         break;

      case TERRAIN:
         qDebug("Window::finished/2 : download %s=%ims",($download=="hiking")? "hiking xml file" : "texture",$clock.elapsed());
         refresh($download);
         break;

      default:
         qFatal("qFatal Window::finished/3");
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::msgBox(const QString& title,const QString& msg)
{
   $timer->stop();

   QMessageBox box(QMessageBox::Warning,title,"",QMessageBox::Ok,this);
   box.setTextInteractionFlags(Qt::TextSelectableByMouse);  // permet les selections a la souris
   box.setTextFormat(Qt::AutoText);                         // Qt::PlainText | Qt::RichText
   box.setText(msg);
   box.exec();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QUrl Window::getRegionUrl()
{  // certains noms de fichiers n'ont pas de '.' avant "hgt"
   QString addr="https://srtm.kurviger.de/SRTM3/"+$continent+"/"+$regionName;
   addr+= ($continent=="North_America" && $regionName.left(3)>="N55")?  "hgt.zip" : ".hgt.zip";

   return QUrl(addr);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QUrl Window::getGoogleUrl(QPointF& lonwLatn,QPointF& loneLats)
{  // obsolete : QString & sprintf(const char * cformat, ...)
   QString coords,addr="file:///"+$resourcesDir+"google.html"+
         coords.sprintf("?lngw=%8.6f&latn=%8.6f&lnge=%8.6f&lats=%8.6f&maptype=",
         lonwLatn.x(),lonwLatn.y(),loneLats.x(),loneLats.y())+$download;

   return QUrl(addr);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QUrl Window::getXmlUrl(QPointF& lonwLatn,QPointF& loneLats)
{
   QString addr;
   addr.sprintf("http://www.overpass-api.de/api/xapi?way[bbox=%8.6f,%8.6f,%8.6f,%8.6f][highway=path|track]",
         lonwLatn.x(),loneLats.y(),loneLats.x(),lonwLatn.y());

   return QUrl(addr);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QString Window::getFileName(const QString& download)
{
   return $transform->preset()+((download=="hiking")? ".fun" : "_"+download+".jpg");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Window::fileExist(const QString& download)
{  // texture ou xml
   return (download=="color")? true : (QFile::exists(getFileName(download)));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QAction* Window::createAction(const QString& text,const QString& key,const QObject* receiver,const char* slot,int index,bool enabled)
{  // default : int index=0, bool enabled=true (destine en priorite a des slots a usage "unique" comme par exemple saveGeotiffTile())
   QAction* action=new QAction(text,this);
   if (!key.isEmpty()) action->setShortcut(QKeySequence(key));
   action->setEnabled(enabled);
   action->setData(index);
   connect(action,SIGNAL(triggered()),receiver,slot);

   return action;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::createMenus(const QIcon& icon)
{
   QAction* arrow=new QAction(icon,"",this);
   connect(arrow,SIGNAL(triggered()),this,SLOT(getBack()));
   menuBar()->addAction(arrow);

   QMenu* fileMenu=menuBar()->addMenu("File");
   fileMenu->addAction(createAction("save geotiff tile",QString(),this,SLOT(saveGeotiffTile()),0,false));
   fileMenu->addAction(createAction("save gps path",QString(),this,SLOT(saveGpsPath()),0,false));
   fileMenu->addAction(createAction("save config file",QString(),this,SLOT(saveConfigFile()),0,false));
   fileMenu->addAction(createAction("quit","Ctrl+Q",qApp,SLOT(quit())));   // default : int index=0, bool enabled=true

   QStringList textureList={"color","roadmap","satellite","terrain","osm"};
   QMenu* textureMenu=menuBar()->addMenu("Texture");
   QActionGroup* textureGroup=new QActionGroup(this);

   for (int i=0,n=textureList.count(); i<n; i++) {
      QAction* action=createAction(textureList.at(i),QString(),this,SLOT(changeTexture()),i,false);
      action->setCheckable(true);
      if (!i) action->setChecked(true);                                    // default : "color"
      textureGroup->addAction(action);
      textureMenu->addAction(action);
   }

   QStringList optionsList={"isoline","hiking","gps file","routedlg"};
   QMenu* optionsMenu=menuBar()->addMenu("Options");
   for (int i=0,n=optionsList.count(); i<n; i++) {
      QAction* action=createAction(optionsList.at(i),QString(),this,SLOT(handleOptions()),i,false);
      action->setCheckable(true);
      optionsMenu->addAction(action);
   }

   QStringList flagsList={"greyscale","bottom-left","dms display","force update","keep xml file","toggle branch"};
   QMenu* flagsMenu=menuBar()->addMenu("Flags");
   for (int i=0,n=flagsList.count(); i<n; i++) {
      QAction* action=createAction(flagsList.at(i),QString(),this,SLOT(handleFlags()),i);
      action->setCheckable(true);
      flagsMenu->addAction(action);
   }

   QMenu* helpMenu=menuBar()->addMenu("?");
   helpMenu->addAction(createAction("about",QString(),this,SLOT(openAboutBox())));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::handleFlags()
{
   int index=((QAction*)sender())->data().toInt();

   invBit(index);

   switch (index) {
      case GREYSCALE:
         if ($stackedWidget->currentIndex()!=WORLD) {
            QString background= (getBit(GREYSCALE))? "greyscale" : "colorshade";
            $regionScene->setPixmap($regionDir+$regionName+"_"+background+".tif");
         }
         break;

      case BOTTOMLEFT:
         if ($stackedWidget->currentIndex()!=WORLD) selectedPos(QPointF(-1,-1));
         break;

      case DMSDISPLAY: case FORCEUPDATE: case KEEPXMLFILE:     // nothing to do!
         break;

      case TOGGLEBRANCH:                  // en mode TERRAIN, ne prend effet qu'apres la fin eventuelle d'un travail en cours
         if ($viewer) $viewer->toggleIsRouting();
         break;


      default:
         qFatal("qFatal Window::handleFlags/1");
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::saveGpsPath()
{
   $viewer->saveGpsPath();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::handleOptions()
{
   QAction* action=(QAction*)sender();    // autrement dit : QAction* action=qobject_cast<QAction*>(sender());
   QString option=action->text();
   bool checked=action->isChecked();

   if (option=="hiking") {
      if (checked) {
         if (getBit(FORCEUPDATE)) networkRequest("hiking");
         else {
            if (fileExist("hiking")) $viewer->refresh("hiking");  // recherche un ".fun"
            else networkRequest("hiking");
         }
      }
      else $viewer->clearRouteDlg(0);
   }
   else if (option=="gps file") $viewer->loadGpsFile(checked);
   else if (option=="routedlg") $viewer->openRouteDlg(checked);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::getOptionState(const QString& option,bool& state)
{
   QList<QAction*> menus=menuBar()->actions(),actions;

   for (int i,j=menus.count()-1; j>=1; j--) if (menus.at(j)->menu()->title()=="Options") {
      actions=menus.at(j)->menu()->actions();

      for (i=actions.count()-1; i>=0; i--) if (actions.at(i)->text()==option) {
         state=actions.at(i)->isChecked();
         return;
      }
      qFatal("qFatal Window::getOptionState/1");
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::resetOptions(const QStringList& options)
{  // default : const QStringList& options=QStringList()
   if (options.isEmpty()) {                        // QStringList() : toutes les options sont a reinitialisees
      QList<QAction*> menus=menuBar()->actions(),actions;

      // menus.at(0) etant l'action "arrow", elle ne possede pas d'attribut "menu", c'est pourquoi l'iterration s'arrete a 1
      for (int i,j=menus.count()-1; j>=1; j--) if (menus.at(j)->menu()->title()=="Options") {
         actions=menus.at(j)->menu()->actions();
         for (i=actions.count()-1; i>=0; i--) if (actions.at(i)->isChecked()) actions.at(i)->setChecked(false);
      }
   }
   else for (int i=options.count()-1; i>=0; i--)   // reinitialisation selective
      updateAction("Options",options.at(i),&QAction::setChecked,false);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::setActionFontWeight()
{  // affiche en gras les textures et options necessitant un telechargement
   QString preset=$transform->preset();
   QList<QAction*> menus=menuBar()->actions(),actions;
   QAction* action;
   QFont font;

   for (int i,j=menus.count()-1; j>=1; j--) if (menus.at(j)->menu()->title()=="Texture") {
      actions=menus.at(j)->menu()->actions();
      for (i=actions.count()-1; i>=1; i--) {    // (i>=1) : ne concerne pas la texture "color"...
         action=actions.at(i);                  // qui ne depend pas de ressources externes
         font=action->font();
         font.setWeight((QFile::exists(preset+"_"+action->text()+".jpg"))? QFont::Normal : QFont::Bold);
         action->setFont(font);
      }
   }

   for (int i,j=menus.count()-1; j>=1; j--) if (menus.at(j)->menu()->title()=="Options") {
      actions=menus.at(j)->menu()->actions();
      for (i=actions.count()-1; i>=0; i--) if (actions.at(i)->text()=="hiking") {
         font=actions.at(i)->font();
         font.setWeight((QFile::exists(preset+".fun"))? QFont::Normal : QFont::Bold);
         actions.at(i)->setFont(font);
         return;
      }
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::enableMenusContent(const QStringList& title,bool value)
{
   QList<QAction*> menus=menuBar()->actions(),actions;
   QAction* action;

   for (int k=title.count()-1; k>=0; k--)
      for (int j=menus.count()-1; j>=1; j--) if (menus.at(j)->menu()->title()==title.at(k)) {
         actions=menus.at(j)->menu()->actions();

         for (int i=actions.count()-((title.at(k)=="File")? 2 : 1); i>=0; i--) {
            action=actions.at(i);

            if (!value && action->font().weight()==QFont::Bold) {
               QFont font=action->font();
               font.setWeight(QFont::Normal);
               action->setFont(font);
            }
            action->setEnabled(value);
         }
      }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::updateAction(const QString& title,const QString& text,void (QAction::*setValue)(bool),bool value)
{  // s'applique a : setVisible, setCheckable, setChecked, setEnabled, setAutoRepeat, setDisabled, setIconVisibleInMenu
   QList<QAction*> menus=menuBar()->actions(),actions;

   for (int i,j=menus.count()-1; j>=1; j--) if (menus.at(j)->menu()->title()==title) {
      actions=menus.at(j)->menu()->actions();
      for (i=actions.count()-1; i>=0; i--) if (actions.at(i)->text()==text) {
         (actions.at(i)->*setValue)(value);
         return;
      }
   }
   qFatal("qFatal Window::updateAction/1 : title=%s  text=%s",qPrintable(title),qPrintable(text));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QString Window::getRadioChecked(const QString& title)
{  // dans un menu radio de nom "title", renvoie l'element coche
   QList<QAction*> menus=menuBar()->actions(),actions;

   for (int i,j=menus.count()-1; j>=1; j--) if (menus.at(j)->menu()->title()==title) {
      actions=menus.at(j)->menu()->actions();
      for (i=actions.count()-1; i>=0; i--) if (actions.at(i)->isChecked()) return actions.at(i)->text();
   }
   qFatal("qFatal Window::getRadioChecked/1 : title=%s",qPrintable(title));
   return QString();                            // jamais execute, mais rassurant pour le compilateur!
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::getTile(QPointF pos)
{
   $transform->setPosition(pos);
   setActionFontWeight();
   initViewer();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::getBack()  // to where you once belonged...
{
   switch ($stackedWidget->currentIndex()) {
      case WORLD:
         close();
         break;

      case REGION:      // region -> world
         $label[0]->setText("lat "+QString::number($regionName.mid(1,2).toInt())+$regionName.mid(0,1));
         $label[1]->setText("lon "+QString::number($regionName.mid(4,3).toInt())+$regionName.mid(3,1));
         $label[3]->setText($regionName);
         $label[4]->setText($continent);
         $stackedWidget->setCurrentIndex(WORLD);
         break;

      case TERRAIN:     // viewer -> region
         $viewer->closeRouteDlg();
         $viewer->clearHikingBuffer();
         updateAction("Texture","color",&QAction::setChecked,true);  // texture par defaut
         resetOptions();                                             // reinitialise toutes les options
         $stackedWidget->setCurrentIndex(REGION);
         selectedPos(QPointF(-1,-1));
         enableMenusContent({"File","Texture","Options"},false);
         break;

      default:qFatal("qFatal Window::getBack/1");
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::initViewer()
{
   QPointF lonwLatn,loneLats;
   $transform->getCorners(lonwLatn,loneLats);

   // Canigou : lonw=2.333726  latn=42.611754  lone=2.588925  lats=42.423655
   qDebug("Window::initViewer : lonw=%8.6f  latn=%8.6f  lone=%8.6f  lats=%8.6f",lonwLatn.x(),lonwLatn.y(),loneLats.x(),loneLats.y());

   $viewer->init($regionData,$width);
   $stackedWidget->setCurrentIndex(TERRAIN);
   selectedPos(QPointF(-1,-1));
   enableMenusContent({"File","Texture","Options"},true);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::changeTexture()
{
   QString texture=((QAction*)sender())->text();

   if (getBit(FORCEUPDATE)) networkRequest(texture);                 // force le telechargement
   else {
      if (texture==$viewer->oldTexture()) return;

      if (fileExist(texture)) $viewer->refresh(texture);             // texture disque
      else networkRequest(texture);                                  // texture reseau
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::timeout()
{
   static int tic;

   if ($label[2]->text()=="000:0") tic=0;

   tic++;
   $label[2]->setText(QString::asprintf("%03i:%01i",tic/10,tic%10));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::imgPathToClipBoard()
{  // associe a la touche F12, permet de mettre dans le presse-papier le chemin de diverses ressources
   QString imgPath;

   switch ($stackedWidget->currentIndex()) {
      case WORLD:
         imgPath=$resourcesDir+"world.gif";
         break;

      case REGION:
         imgPath=$regionDir+$regionName+"_"+((getBit(GREYSCALE))? "greyscale.tif" : "colorshade.tif");
         break;

      case TERRAIN:
         QString texture=getRadioChecked("Texture");
         if (texture=="color") {
            imgPath="";
            QMessageBox::information(this,"information","No file for texture \"color\"!<br />");
         }
         else imgPath=$transform->preset()+"_"+texture+".jpg";
         break;
   }

   QApplication::clipboard()->setText(imgPath.replace('/','\\'));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::openAboutBox()
{  // lien "Binaries" affiche sur 2 lignes : pas tres elegant mais fonctionnel!
   QString msg;
   QTextStream(&msg) <<
      "<b>CartoFun</b> : version 1.9<br/>" <<
      "<b>Author</b> : Michel VAN THODIEP<br/>" <<
      "<b>Runtime environment</b> : Windows10 32 or 64 bits<br/>" <<
      "<b>Development</b> : Qt-5.13.2 32 bits<br/>" <<
      "<b>Compilation</b> : MinGW-7.3.0 or MSVC2017<br/>" <<
      "<b>Libraries</b> :<br/>" <<
      " - proj-5.2.0<br/>" <<
      " - gdal-2.4.3<br/>" <<
      " - quazip-0.7.3<br/>" <<
      " - qtwebkit-5.212.0-alpha2<br/>" <<
      "<b>Documentation</b> :<br/>" <<
      " - <a href='https://mivath.gitlab.io/cartofun/en/index.html'>english</a><br/>" <<
      " - <a href='https://mivath.gitlab.io/cartofun/fr/index.html'>french</a><br/>" <<
      "<b>Sources</b> : <a href='https://gitlab.com/mivath/cartofun'>https://gitlab.com/mivath/cartofun</a><br/>" <<
      "<b>Binaries</b> : <a href='http://michel.vanthodiep.free.fr/Qt/cartofun-1.9_mingw32_win10.zip'>"
                                 "http://michel.vanthodiep.free.fr/Qt/cartofun-1.9_mingw32_win10.zip</a>";

   QMessageBox::about(this,"about",msg);  // merci Qt pour l'insertion automatique de l'icone!
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::keyPressEvent(QKeyEvent* event)
{  // pour les codes : "Qt Namespace Reference"
   // suppose que les touches F11 et F12 soient programmables (avec les lapTops, il faut aller dans le BIOS)
   switch (event->key()) {
      case Qt::Key_Escape:
         getBack();
         break;

      case Qt::Key_F11:
         if (event->modifiers() & Qt::ShiftModifier) {
            if ($stackedWidget->currentIndex()!=WORLD) saveRectFile();
         }
         break;

      case Qt::Key_F12:
         imgPathToClipBoard();
         break;

      default:
         QMainWindow::keyPressEvent(event);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Window::~Window()
{
   $regionFile.unmap($regionData);

   delete $unTransform;
   delete $transform;
   delete $googleMap;
   delete $osmMap;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Window::closeEvent(QCloseEvent*)
{
   if ($viewer) delete $viewer;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc,char* argv[])
{
   QLoggingCategory::setFilterRules("qt.network.ssl.warning=false"); // ok
   QCoreApplication::setAttribute(Qt::AA_UseDesktopOpenGL);          // force l'utilisation d'"OpenGL for desktop"

   QApplication app(argc,argv);

   Window window;
   window.show();

   return app.exec();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 - Date : 08/12/2011
 - Nom : fun.h
*/

#ifndef FUN_H
#define FUN_H

#include <QtGui>                    // gui
#include <QtWidgets>                // widgets
#include <QtXml>                    // xml

#include "routedlg.h"


typedef QPair<qint64,char> Step;    // nid, colorIndex
//           (n)id node,   coords node,   (w)id way, index node (QHash : ordre des indices arbitraire)
typedef QMap<qint64,QPair<QPointF,QList<QPair<qint64,qint16>>>> NodeMapType;
//           (w)id du way, (n)id du node, index "interface",   index du sommet (et donc trie), (s)id du sommet
typedef QMap<qint64,QPair<QList<QPair<qint64,int>>,QMap<qint16,qint64>>> WayMapType;
//           (s)id du sommet, (s)id du voisin, distance les separant (QMap ecrase les doublons)
typedef QMap<qint64,QMap<qint64,double>> SummitMapType;


char* concat(char* dest,const char* fmt,...);

////////////////////////////////////////////////////////////////////////////////////////////////////

class ConvexHull
{
   public:
      QPolygonF& getHull(QPolygonF& polygon);

   private:
      QPointF $p0;

      char orientation(const QPointF& p,const QPointF& q,const QPointF& r);
      bool lessThan(const QPointF& p1,const QPointF& p2);
      QPointF nextToTop(QStack<QPointF>& stack);
      qreal distSquared(const QPointF& p,const QPointF& q);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class Clip
{
   public:
      Clip(NodeMapType& nodeMap,WayMapType& wayMap,SummitMapType& summitMap,const QSize& imageSize);

   private:
      NodeMapType& $nodeMap;
      WayMapType& $wayMap;
      SummitMapType& $summitMap;
      QSize $imageSize;

      char* out(qint64 id);
      bool isTip(const QPair<qint64,qint16>& pair);
      void removeWay(qint64 wid);
      bool isDoubleLiaison(qint64 sid1,qint64 sid2);
      void updateSummit(qint64 wid,qint16 index,qint64 neighbor);
      void updateNode(qint64 nid,qint64 wid,qint16 index=-1);
      char getStart(const QList<qint16>& changeList,const QList<QPair<qint64,int>>& route,qint16& change);
      void mainLoop(QMap<qint64,QList<qint16>>& workMap);
      void check();
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class XmlToFun
{
   public:
      XmlToFun(NodeMapType& nodeMap,WayMapType& wayMap,SummitMapType& summitMap,
         const QRectF& rect,const QByteArray& array,const QString& terrainDir,bool keepXmlFile);

   private:
      NodeMapType& $nodeMap;
      WayMapType& $wayMap;
      SummitMapType& $summitMap;
      Slippy $slippy;

      // implementation
      void analyze(const QDomDocument& doc,QTime& time,const QString& baseName);
      void save(const QString& saveFile);
      void fetchNodes(const QDomDocument& doc);
      void fetchWays(const QDomDocument& doc);
      void fetchSummits();
      void fetchLength(SummitMapType::iterator& itj,qint64 wid,const QMap<qint16,qint64>::const_iterator& it1,char add);
      void adjustPetrovskLoop();
      int indexOf(qint64 wid,qint64 nid);
      void removeNests(int& nestCount,qreal side,int max);
      bool recursiv(qint64 sid,QSet<qint64>& summit,QSet<qint64>& way,int max);
      void repairBreaks();
      bool isValid(qint64 sid1,qint64 sid2,const QMap<qint64,QRectF>& boundMap);
      void makeEndsMeet(const QList<QPair<qint64,qint64>>& workList);
      bool isTip(const QPair<qint64,qint16>& pair);
      qreal dist(const QPointF& p,const QPointF& q);
      char* out(qint64 nid);
      void logNode(qint64 nid,char flag);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class FunTo3D
{
   public:
      FunTo3D(QVector<QVector3D>& vertexVector,QVector<QVector3D>* targetVector);

      bool loadFunFile(NodeMapType& nodeMap,WayMapType& wayMap,SummitMapType& summitMap,
            const QString& funFile,const QRectF& rect,QPointF& coef);
      bool loadGpxFile(const QString& gpxFile,QList<int>& indexList,const QRectF& rect,QString& errMsg);
      bool loadKmlFile(const QString& kmlFile,QList<int>& indexList,const QRectF& rect,QString& errMsg);
      void getCookie(const QPointF& p0,qreal r);

   private:
      QVector<QVector3D> &$vertexVector,*$targetVector,$segment;

      void horizontalMove(QVector3D p,QVector3D q,bool& invert);
      void verticalMove(QVector3D p,QVector3D q,bool& invert);
      void goToCell(int i,int j,qreal x2,qreal y2,bool start=false);
      QVector3D drawTo(qreal x,qreal y,bool start=false);
      bool swap(QVector3D& p,QVector3D& q);
      void QVector3D2xyz(int index,qreal& x,qreal& y,qreal& z);
      int sgn(double value);
      void addSegment(QVector3D& p,const QVector3D& q);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class Segm
{
   private:
      qint64 $wid;
      qint16 $begin,$end;

   public:
      enum Arg {BEGIN,END};

      Segm(const qint64 wid=0,const qint16 begin=-1,const qint16 end=-1) : $wid(wid),$begin(begin),$end(end) {}
      Segm(const QPair<qint64,qint16>& pair,const qint16 end) : $wid(pair.first),$begin(pair.second),$end(end) {}
      qint64 wid() const {return $wid;}
      qint16 begin() const {return $begin;}
      qint16 end() const {return $end;}
      void setEnd(quint16 end) {$end=end;}
      Segm inv() const {return Segm($wid,$end,$begin);}
      bool contains(const qint16 i) {return ((i>=$begin && i<=$end)||(i>=$end && i<=$begin));}
      bool operator == (const Segm& segm) {return (segm.$wid==$wid && segm.$begin==$begin && segm.$end==$end);}
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class AStar
{
   struct Cell {
      double costg,costf;
      qint64 parent;
   };
   typedef QHash<qint64,Cell> CellHashType;

   public :
      AStar(
         const NodeMapType& nodeMap,const SummitMapType& summitMap,
         QList<qint64>& chain,qint64 current,qint64 goal,const QPointF& coef,bool& rslt
      );
      AStar(const NodeMapType& nodeMap,const SummitMapType& summitMap,QList<qint64>& chain,const QPointF& coef);
      bool approve(qint64 current,qint64 goal);

   private :
      const NodeMapType& $nodeMap;
      const SummitMapType& $summitMap;
      QList<qint64>& $chain;
      qint64 $goal;
      const QPointF& $coef;

      CellHashType $openHash;
      CellHashType $closedHash;
      QMultiMap<double,qint64> $bestCostMap;

      double distance(const QPointF& p,const QPointF& q);
      void addNeighbors(qint64 sidp);
      void makeChain(qint64 startParent);
      QPointF getPos(qint64 sid);
      bool isValid(qint64 sidp,qint64 sidq);
      void insert(qint64 sid,const Cell& cell);
      void update(qint64 sid,const Cell& cell);
      void remove(qint64 sid);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class Shared : public QObject
{
   Q_OBJECT

   public:
      Shared(const NodeMapType& nodeMap,const WayMapType& wayMap,const SummitMapType& summitMap) :
            $nodeMap(nodeMap),$wayMap(wayMap),$summitMap(summitMap) {}

   protected:
      const NodeMapType& $nodeMap;
      const WayMapType& $wayMap;
      const SummitMapType& $summitMap;

      char* out(qint64 nidp,qint64 nidq=-1);
      bool msg(bool update,const char* fmt,...);
      int indexOf(qint64 wid,qint64 nid);
      bool isTip(const QPair<qint64,qint16>& pair);
      bool isJunction(qint64 nid1);

   signals:
      void msgBox(bool update,const QString& s);
      void addSegm(const Segm& segm,const QColor& color);
      void clearSegmPath();
      void addStepList(const QList<Step>& stepList);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class Branch : public Shared
{
   public:
      Branch(const NodeMapType& nodeMap,const WayMapType& wayMap,const SummitMapType& summitMap) :
            Shared(nodeMap,wayMap,summitMap) {}
      bool next(qint64 nid=0,Qt::MouseButton button=Qt::NoButton);

   private:
      char* outSegm(const Segm& segm);
      void appendToHead(const QList<Segm>& body,QList<Segm>& head,const Segm& segm);
      bool appendToBody(QList<Segm>& body,const Segm& segm);
      char isDejaVu(const QList<Segm>& body,const Segm& segm,Segm& newSegm);
      bool finish(QList<Segm>& body,QList<Segm>& head,int num);
      qint64 getNid(const Segm& segm,Segm::Arg arg);
      Segm junction(const Segm& segm,bool recursion=false,const QColor& color=QColor());
      void drawBody(const QList<Segm>& body);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class Routing : public Shared
{
   public:
      Routing(const NodeMapType& nodeMap,const WayMapType& wayMap,const SummitMapType& summitMap,const QPointF& coef) :
            Shared(nodeMap,wayMap,summitMap),$coef(coef) {}
      bool next(qint64 nid=0,qint64 sid=0,Qt::MouseButton button=Qt::NoButton);

   private:
      const QPointF& $coef;

      char status(const QList<Step>& stepList,qint64 nid,bool start);
      bool isAllRight(const QList<Step>& stepList);
      bool otherCases(qint64 blue,qint64 red,qint64 black,QList<Step>& stepList,bool start=false);
      bool drawStepList(const QList<Step>& stepList);
      Segm summitsCase(const QList<QPair<qint64,qint16>>& nlist0,qint64 sid1);
      qint64 nextSummit(qint64 sidBegin,qint64 nidBegin);
      bool isValid(const QList<Step>& stepList,qint64 nidBegin,qint64 nid,char start);
      const QList<Step>& cleanStepList(const QList<Step>& stepList);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif

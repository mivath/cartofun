/*
 - Date : 12/05/2011
 - Nom : region.cpp
*/

#include "region.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionSelector::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event)
{
   if (event->button()!=Qt::LeftButton) return;
   emit getTile(pos());
   QGraphicsPathItem::mouseDoubleClickEvent(event);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

RegionSelector::RegionSelector()
{
   setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable |
         QGraphicsItem::ItemSendsGeometryChanges); // ItemIsFocusable : pour le clavier
   setMyPath();
   setFocus();                         // permet la manipulation en absence de selection et meme de visibilite
   setSelected(true);                  // selectionne par defaut
   setZValue(1.0);                     // par defaut 0, et visibilite dependant de l'ordre
   setAcceptHoverEvents(true);         // necessaire pour gerer le "survol"
   ensureVisible();
}  // on a : path.boundingRect().width()+1==257.0, soit DIM (1 : epaisseur du trait)

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionSelector::setMyPath()
{
   qreal a=DIM-1,b=a/2+2,c=b-4;        // DIM impair! a=256,b=130,c=126

   // coordonnees du pixel au centre : 128,128 (pour dim==257)
   $path.addRect(0,0,a,a);
   $path.addRect(c,c,4,4);

   $path.moveTo(1,1);
   $path.lineTo(c-1,c-1);

   $path.moveTo(a-1,1);
   $path.lineTo(b+1,c-1);

   $path.moveTo(a-1,a-1);
   $path.lineTo(b+1,b+1);

   $path.moveTo(1,a-1);
   $path.lineTo(c-1,b+1);

   setPath($path);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionSelector::setColors(bool toggle)
{
   if (toggle) {     // colorShade
      $enabled=Qt::black;              // noir
      $disabled=QColor("#303030");     // gris
   }
   else {            // greyScale
      $enabled=Qt::green;              // vert
      $disabled=QColor("#80D080");     // vert pale
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionSelector::paint(QPainter* painter,const QStyleOptionGraphicsItem* option,QWidget*)
{  // d'apres le livre : "Creer des applications avec Qt 5" Par Jonathan Courtois (et gbdivers!)
   // (http://www.eyrolles.com/Informatique/Livre/creer-des-applications-avec-qt5-9782822701082)
   painter->save();

   painter->setPen(QPen((option->state & QStyle::State_Selected)? $enabled : $disabled));
   painter->drawPath($path);

   painter->restore();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionSelector::keyPressEvent(QKeyEvent* event)
{
   QRectF rect=scene()->sceneRect();
   qreal dim=qreal(DIM);

   switch (event->key()) {
      case Qt::Key_Return:
         emit (event->modifiers() & Qt::ShiftModifier)? getBack() : getTile(pos());
         return;

      case Qt::Key_Up:case Qt::Key_8:
         if (y()>=1) setY(y()-1);
         else return;
         break;

      case Qt::Key_9:         // diagonale NE
         if (x()<=rect.width()-dim-1 && y()>=1) setPos(x()+1,y()-1);
         else return;
         break;

      case Qt::Key_Right:case Qt::Key_6:
         if (x()<=rect.width()-dim-1) setX(x()+1);
         else return;
         break;

      case Qt::Key_3:         // diagonale SE
         if (x()<=rect.width()-dim-1 && y()<=rect.height()-dim-1) setPos(x()+1,y()+1);
         else return;
         break;

      case Qt::Key_Down:case Qt::Key_2:
         if (y()<=rect.height()-dim-1) setY(y()+1);
         else return;
         break;

      case Qt::Key_1:         // diagonale SW
         if (x()>=1 && y()<=rect.height()-dim-1) setPos(x()-1,y()+1);
         else return;
         break;

      case Qt::Key_Left:case Qt::Key_4:
         if (x()>=1) setX(x()-1);
         else return;
         break;

      case Qt::Key_7:         // diagonale NW
         if (x()>=1 && y()>=1) setPos(x()-1,y()-1);
         else return;
         break;

      case Qt::Key_Tab:       // normalement desactivee (Qt5.5.1) : activation dans RegionSelector::sceneEvent
         if (!tabTerrains()) return;
         break;

      default:
         QGraphicsPathItem::keyPressEvent(event);
         return;
   }
   ensureVisible();
   emit selectedPos(pos());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool RegionSelector::sceneEvent(QEvent* event)
{  // to avoid tab eating in QGraphicsItem (https://github.com/enricoros/fotowall/blob/master/Shared/BrowserItem.cpp)
   if (event->type()==QEvent::KeyPress) {
      QKeyEvent* ke=static_cast<QKeyEvent*>(event);

      if (ke->key()==Qt::Key_Tab) keyPressEvent(ke);     // default : ke->setAccepted(true);
      else keyPressEvent(ke);
      return true;
   }

   return QGraphicsPathItem::sceneEvent(event);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool RegionSelector::tabTerrains()
{
   QList<QGraphicsItem*> list=scene()->items();
   QMap<int,QList<int>> map;

   for (int i=0,m=list.count(); i<m; i++) if (list[i]->type()==QGraphicsPathItem::Type) {
      const QPoint& q=QPointF(((QGraphicsPathItem*)list[i])->path().elementAt(1)).toPoint();
      if (map.contains(q.y())) map[q.y()].append(q.x());
      else map.insert(q.y(),{q.x()});
   }
   if (map.isEmpty()) return false;

   for (QMap<int,QList<int>>::iterator itj=map.begin(); itj!=map.end(); itj++)
      if (itj->count()>1) std::sort((*itj).begin(),(*itj).end());

   const QPoint& p=pos().toPoint()+QPoint(0,DIM-1);
   QMap<int,QList<int>>::iterator iti=map.lowerBound(p.y());
   QPoint target;
   int index,j,n;

   if (iti==map.end()) {
      iti=map.begin();
      target=QPoint(iti->first(),iti.key());
   }
   else if (iti.key()>p.y()) target=QPoint(iti->first(),iti.key());
   else {                              // (iti.key==p.y()) : cas (iti.key<p.y()) ecarte (voir rubrique "QMap::lowerBound")
      index=iti->indexOf(p.x());

      if (index>=0) {                  // index present dans la liste
         if (index<iti->count()-1) target=QPoint(iti->at(index+1),p.y());
         else {
            iti++;
            if (iti==map.end()) iti=map.begin();
            target=QPoint(iti->first(),iti.key());
         }
      }
      else if (p.x()>iti->last()) {    // (index==-1) : absent de la liste, cas particulier
         iti++;
         if (iti==map.end()) iti=map.begin();
         target=QPoint(iti->first(),iti.key());
      }
      else {                           // (index==-1) : absent de la liste, cas general
         for (j=0,n=iti->count(); j<n && p.x()>=iti->at(j); j++);
         if (j==n) qFatal("qFatal RegionSelector::tabTerrains/1");
         target=QPoint(iti->at(j),iti.key());
      }
   }
   if (target==p) return false;

   setPos(QPointF(target-QPoint(0,DIM-1)));
   return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

QVariant RegionSelector::itemChange(GraphicsItemChange change,const QVariant& value)
{
   if (change==ItemPositionChange && scene()) {
      QRectF rect=scene()->sceneRect();
      QPointF topLeft=value.toPointF();         // value is the new position.
      qreal dim=qreal(DIM);

      if (topLeft.x()<0) topLeft.setX(0);
      else if (topLeft.x()>rect.width()-dim) topLeft.setX(rect.width()-dim);

      if (topLeft.y()<0) topLeft.setY(0);
      else if (topLeft.y()>rect.height()-dim) topLeft.setY(rect.height()-dim);

      ensureVisible();
      scene()->update();
      emit selectedPos(topLeft);
      return topLeft;
   }
   return QGraphicsItem::itemChange(change,value);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionSelector::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
   setCursor(Qt::OpenHandCursor);
   QGraphicsPathItem::hoverEnterEvent(event);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionSelector::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
   setCursor(Qt::ArrowCursor);
   QGraphicsPathItem::hoverLeaveEvent(event);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

RegionScene::RegionScene(QObject* parent)
{
   $pixmapItem=addPixmap(QPixmap());                                       // index 2, type 7, zvalue 0

   $regionSelector=new RegionSelector;                                     // index 1, type 65537, zvalue 1
   addItem($regionSelector);

   // signale le point le plus haut (pas de problemes de debordement, apparemment...)
   $highestPos=addRect(-2,-2,2,2,QPen(Qt::red),QBrush(Qt::red));           // index 0, type 3, zvalue 2
   $highestPos->setZValue(2.0);

   connect($regionSelector,SIGNAL(selectedPos(QPointF)),parent,SLOT(selectedPos(QPointF)));
   connect($regionSelector,SIGNAL(getTile(QPointF)),parent,SLOT(getTile(QPointF)));
   connect(this,SIGNAL(delTile(QPointF,bool&)),parent,SLOT(delTile(QPointF,bool&)));
   connect($regionSelector,SIGNAL(getBack()),parent,SLOT(getBack()));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionScene::setPixmap(const QString& imgFile,const QList<QPoint>& cornerList,const QPoint& pos)
{  // default : const QList<QPoint>& cornerList=QList<QPoint>(),const QPoint& pos=QPoint(-1,-1)
   QImage img(imgFile);
   setSceneRect(QRectF(img.rect()));
   $pixmapItem->setPixmap(QPixmap::fromImage(img));   // origine des warnings "TIFFReadDirectory"

   if (pos==QPoint(-1,-1)) {                          // appel depuis Window::changeBackground
      $regionSelector->setColors(imgFile.section('/',-1).contains("colorshade"));
      return;
   }

   QPen pen(Qt::red);
   QList<QGraphicsItem*> list=items();

   for (int i=0,m=list.count(); i<m; i++)             // vire les "coins" existants (Type==2)
      if (list[i]->type()==QGraphicsPathItem::Type) removeItem(list[i]);

   for (int i=0,m=cornerList.count(); i<m; i++) addCorner(cornerList.at(i));

   $regionSelector->setColors(imgFile.section('/',-1).contains("colorshade"));
   $regionSelector->setSelected(true);
   $regionSelector->setPos(pos);
   views().first()->centerOn(pos+QPointF(128,128));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionScene::addCorner(QPoint p,bool check)
{  // default : bool check=false
   static const int len=20;            // longueur des cotes

   if (check) {
      QList<QGraphicsItem*> list=items();
      QPointF pf(p);

      for (int i=0,m=list.count(); i<m; i++) if (
            list[i]->type()==QGraphicsPathItem::Type &&
            QPointF(((QGraphicsPathItem*)list[i])->path().elementAt(1))==pf
         ) return;
   }

   QPoint q=p+QPoint(DIM-1,-DIM+1);
   QPainterPath path;

   path.moveTo(p.x(),p.y()-len);
   path.lineTo(p.x(),p.y());           // coin "bottomLeft"
   path.lineTo(p.x()+len,p.y());

   path.moveTo(q.x()-len,q.y());
   path.lineTo(q.x(),q.y());           // coin "topRight"
   path.lineTo(q.x(),q.y()+len);

   addPath(path,QPen(Qt::red));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionScene::removeCorner(bool deleted)
{
   if (!deleted) return;

   QPointF pos=$regionSelector->pos()+QPointF(0,256);    // pour cibler le coin bottom-left
   QList<QGraphicsItem*> list=items();

   for (QGraphicsItem* gi: list)                         // elementAt(1) : coin bottom-left
      if (gi->type()==QGraphicsPathItem::Type && QPointF(((QGraphicsPathItem*)gi)->path().elementAt(1))==pos) {
         removeItem(gi);
         return;
      }

   qFatal("qFatal RegionScene::removeCorner/1");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionScene::setHighestPos(QPointF pos)
{
   if (pos!=$highestPos->rect().topLeft()) $highestPos->setRect(pos.x()-1,pos.y()-1,2,2);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionScene::goToHighestPos()
{
   QPointF pos=$highestPos->rect().topLeft()+QPointF(1,1),TL=pos-QPointF(128,128),BR=pos+QPointF(128,128);
   QRectF rect=sceneRect();

   if (TL.x()>=0 && TL.y()>=0 && BR.x()<rect.width() && BR.y()<rect.height()) $regionSelector->setPos(TL);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void RegionScene::keyPressEvent(QKeyEvent* event)
{
   switch (event->key()) {
      case Qt::Key_Delete:    // suppression de tous les fichiers se rapportant a ce terrain
         if (event->modifiers() & Qt::ShiftModifier) {
            bool deleted=false;
            emit delTile($regionSelector->pos(),deleted);
            removeCorner(deleted);
         }
         break;

      case Qt::Key_H:         // comme dans RouteTab::keyPressEvent
         goToHighestPos();
         break;

      default:QGraphicsScene::keyPressEvent(event);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef GLOBAL_H
#define GLOBAL_H

typedef char str256[257],str1k[1025],str2k[2049];

const int DIM=257;                              // a choisir impair pour avoir un pixel central
const double SIDE=25.6,STEP=SIDE/double(DIM-1); // STEP=25.6/256.0, soit 0.1
const char PROJ[]="+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext +over +no_defs";

#endif

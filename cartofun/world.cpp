/*
 - Date : 23/06/2012
 - Nom : world.cpp
*/

#include "world.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

WorldScene::WorldScene(const QString& regionDir,const QString& imgFile,QObject* parent) : QGraphicsScene(parent)
{
   $continent={
      {0xFF006FDE,"South_America"}, {0xFF00FFFF,"Islands"}, {0xFFDE7700,"North_America"},
      {0xFFFF0000,"Australia"},     {0xFFFF00FF,"Africa"},  {0xFFFFFF00,"Eurasia"}
   };

   $img=QImage(imgFile);
   if ($img.isNull()) qFatal("qFatal WorldScene::WorldScene/1");
   paintCacheRegions(regionDir);
   setSceneRect(0,0,$img.width(),$img.height());
   $pixmapItem=addPixmap(QPixmap::fromImage($img));

   $worldSelector=addRect(-0.5,-0.5,12,12,QPen(),QBrush());
   $worldSelector->setZValue(1.0);
   $worldSelector->ensureVisible();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void WorldScene::paintCacheRegions(const QString& regionDir)
{
   QDir dir(regionDir);
   QStringList entry=dir.entryList({"*.tif"});
   QString s,name;
   QRegExp rx("[NS][0-5]\\d[EW][01]\\d\\d");    // exemple : N42E002.tif
   int k,count,lat,lon,i,j,x,y,m,n;
   QList<QRgb> colorList;

   // suppression de l'extension
   for (i=0,count=entry.count(); i<count; i++) entry[i]=entry.at(i).section('.',0,0);

   // analyse, N : 00 -> 59, S : 01 -> 56, E : 000 -> 179, W : 001 -> 180
   for (k=0; k<count; k++) {
      name=entry.at(k);
      if (name.length()!=7 || !name.contains(rx)) continue;

      lat=((name.at(0)=='N')? 1 : -1)*name.mid(1,2).toInt();                     // 42
      lon=((name.at(3)=='E')? 1 : -1)*name.mid(4,3).toInt();                     // 002
      if (-56>lat || lat>59 || -180>lon || lon>179 || name.left(3)=="S00" || name.right(4)=="W000") continue;

      x=(lon+180)*12;
      y=(-lat+89)*12;
      colorList.clear();
      for (j=y,m=x+11,n=y+11; j<n; j++)         // d'apres WorldScene::moveTo
         for (i=x; i<m; i++) if (!colorList.contains($img.pixel(i,j))) colorList.append($img.pixel(i,j));

      if (colorList.contains(0xFFFFFFFF) || !entry.contains(name+"_colorshade") || !entry.contains(name+"_greyscale")) continue;

      // coloration en "Qt::green", d'apres WorldScene::moveTo
      for (j=y; j<n; j++)
         for (i=x; i<m; i++) if ($img.pixel(i,j)!=0xFF000000) $img.setPixel(i,j,0xFF00FF00);

      for (i=0,m=colorList.count(); i<m; i++) if ($continent.contains(colorList.at(i))) break;
      if (i==m) qFatal("qFatal WorldScene::paintCacheRegions/1");

      $img.setPixel(x,y,colorList.at(i));       // arbitrairement le top-left-point
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void WorldScene::repaintRegion(QRgb color)
{
   int y=$worldSelector->pos().y()+0.5,x=$worldSelector->pos().x()+0.5;
   QRgb topLeftColor=$img.pixel(x,y);                 // astuce : le top-left-point c'est la couleur du continent!
                                                      // note : l'ombrage de QBrush ne modifie pas $img
   if (color==0) color=topLeftColor;                  // appel depuis WorldScene::keyPressEvent
   else if (!$continent.contains(topLeftColor)) {     // (color==0xFF00FF00) appel depuis Window::downloadFinished, vert "Cache"
      for (int j=y,n=y+11,i,m=x+11; j<n; j++) {
         for (i=x; i<m; i++) if ($continent.contains($img.pixel(i,j))) {
            topLeftColor=$img.pixel(i,j);
            break;
         }
         if (i<m) break;
      }
   }

   for (int j=y,n=y+11,i,m=x+11; j<n; j++)
      for (i=x; i<m; i++) if ($img.pixel(i,j)!=0xFF000000) $img.setPixel(i,j,color);

   $img.setPixel(x,y,topLeftColor);                   // pixel top-left d'identification
   $pixmapItem->setPixmap(QPixmap::fromImage($img));  // rechargement de l'image
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void WorldScene::moveTo(QPoint pos)
{
   static QString continent="?";

   QList<QRgb> colorList;
   int y=pos.y()-pos.y()%12,x=pos.x()-pos.x()%12;

   for (int j=y,n=y+11,i,m=x+11; j<n; j++)
      for (i=x; i<m; i++) if (!colorList.contains($img.pixel(i,j))) colorList.append($img.pixel(i,j));

   int i;
   for (i=colorList.count()-1; i>=0; i--) if ($continent.contains(colorList.at(i))) break;

   $worldSelector->setPos(x-0.5,y-0.5);
   $worldSelector->setBrush((i<0)? QBrush() : QBrush(QColor(64,64,64,128)));
   $worldSelector->ensureVisible();
   update();

   continent= (i<0)? "?" : $continent.value(colorList.at(i));
   emit mousePos($worldSelector->scenePos().toPoint(),continent);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void WorldScene::keyPressEvent(QKeyEvent* event)
{  // pave numerique : suppose (event->modifiers()==Qt::KeypadModifier)
   QPoint pos=$worldSelector->scenePos().toPoint();
   int x=pos.x(),y=pos.y(),w=$img.width(),h=$img.height();

   switch (event->key()) {
      case Qt::Key_Return:    // download et|ou ouverture region selectionnee
         if (event->modifiers()==Qt::NoModifier) emit getRegion();
         break;

      case Qt::Key_Delete:    // suppression de la region avec tout ce qu'elle contient
         if (event->modifiers() & Qt::ShiftModifier) {
            bool rslt=false;
            emit delRegion(rslt);
            if (rslt) repaintRegion();
         }
         break;

      case Qt::Key_Up:case Qt::Key_8:
         if (y-12>=0) moveTo(QPoint(x,y-12));
         break;

      case Qt::Key_9:         // diagonale NE
         if (x+12<w-1 && y-12>=0) moveTo(QPoint(x+12,y-12));
         break;

      case Qt::Key_Right:case Qt::Key_6:
         if (x+12<w-1) moveTo(QPoint(x+12,y));
         break;

      case Qt::Key_3:         // diagonale SE
         if (x+12<w-1 && y+12<h-1) moveTo(QPoint(x+12,y+12));
         break;

      case Qt::Key_Down:case Qt::Key_2:
         if (y+12<h-1) moveTo(QPoint(x,y+12));
         break;

      case Qt::Key_1:         // diagonale SW
         if (x-12>=0 && y+12<h-1) moveTo(QPoint(x-12,y+12));
         break;

      case Qt::Key_Left:case Qt::Key_4:
         if (x-12>=0) moveTo(QPoint(x-12,y));
         break;

      case Qt::Key_7:         // diagonale NW
         if (x-12>=0 && y-12>=0) moveTo(QPoint(x-12,y-12));
         break;

      default:QGraphicsScene::keyPressEvent(event);
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void WorldScene::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
   if (event->button()==Qt::LeftButton) moveTo(event->scenePos().toPoint());
   else QGraphicsScene::mousePressEvent(event);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void WorldScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event)
{
   if (event->button()==Qt::LeftButton) {
      moveTo(event->scenePos().toPoint());
      emit getRegion();
   }
   else QGraphicsScene::mouseDoubleClickEvent(event);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

QVector3D ColorShade::getColor(float percent)
{  // Given an percent elevation calculate a color based on the color points table
   QMap<qreal,QVector3D>::const_iterator j=$ramp.lowerBound(percent),i=j-1;

   if (j.key()==0.0) return j.value();

   float r=(j.key()-percent)/(j.key()-i.key());
   return i.value()*r+j.value()*(1.0-r);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void ColorShade::getCell(int j,int i,float* win)
{
   int x,y,p,q;

   if (i==0) {
      if (j==0) {                   // cas 1
         for (x=0; x<2; x++)
            for (y=0,p=x*$nYSize,q=4+3*x; y<2; y++,p++,q++) win[q]=$inPixels[p];

         win[0] = 2*win[4] - win[8];
         win[1] = 2*win[4] - win[7];
         win[2] = 2*win[5] - win[8];
         win[3] = 2*win[4] - win[5];
         win[6] = 2*win[7] - win[8];
      }
      else if (j==$nXSize-1) {      // cas 2
         for (x=0; x<2; x++)
            for (y=0,p=(j-1+x)*$nYSize,q=1+3*x; y<2; y++,p++,q++) win[q]=$inPixels[p];

         win[0] = 2*win[1] - win[2];
         win[3] = 2*win[4] - win[5];
         win[6] = 2*win[4] - win[2];
         win[7] = 2*win[4] - win[1];
         win[8] = 2*win[5] - win[2];
      }
      else {                        // cas 3
         for (x=0; x<3; x++)
            for (y=0,p=(j-1+x)*$nYSize,q=1+3*x; y<2; y++,p++,q++) win[q]=$inPixels[p];

         win[0] = 2*win[1] - win[2];
         win[3] = 2*win[4] - win[5];
         win[6] = 2*win[7] - win[8];
      }
   }
   else if (i==$nYSize-1) {
      if (j==0) {                   // cas 4
         for (x=0; x<2; x++)
            for (y=0,p=x*$nYSize+i-1,q=3+3*x; y<2; y++,p++,q++) win[q]=$inPixels[p];

         win[0] = 2*win[3] - win[6];
         win[1] = 2*win[4] - win[7];
         win[2] = 2*win[4] - win[6];
         win[5] = 2*win[4] - win[3];
         win[8] = 2*win[7] - win[6];
      }
      else if (j==$nXSize-1) {      // cas 5
         for (x=0; x<2; x++)
            for (y=0,p=(j-1+x)*$nYSize+i-1,q=3*x; y<2; y++,p++,q++) win[q]=$inPixels[p];

         win[2] = 2*win[1] - win[0];
         win[5] = 2*win[4] - win[3];
         win[6] = 2*win[3] - win[0];
         win[7] = 2*win[4] - win[1];
         win[8] = 2*win[4] - win[0];
      }
      else {                        // cas 6
         for (x=0; x<3; x++)
            for (y=0,p=(j-1+x)*$nYSize+i-1,q=3*x; y<2; y++,p++,q++) win[q]=$inPixels[p];

         win[2] = 2*win[1] - win[0];
         win[5] = 2*win[4] - win[3];
         win[8] = 2*win[7] - win[6];
      }
   }
   else {
      if (j==0) {                   // cas 7
         for (x=0,q=3; x<2; x++)
            for (y=0,p=x*$nYSize+i-1; y<3; y++,p++,q++) win[q]=$inPixels[p];

         win[0] = 2*win[3] - win[6];
         win[1] = 2*win[4] - win[7];
         win[2] = 2*win[5] - win[8];
      }
      else if (j==$nXSize-1) {      // cas 8
         for (x=q=0; x<2; x++)
            for (y=0,p=(j-1+x)*$nYSize+i-1; y<3; y++,p++,q++) win[q]=$inPixels[p];

         win[6] = 2*win[3] - win[0];
         win[7] = 2*win[4] - win[1];
         win[8] = 2*win[5] - win[2];
      }
      else {                        // cas 9
         for (x=q=0; x<3; x++)
            for (y=0,p=(j-1+x)*$nYSize+i-1; y<3; y++,p++,q++) win[q]=$inPixels[p];
      }
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

ColorShade::ColorShade(const QString& src,const QString& dst,const QMap<qreal,QVector3D>& ramp,float& statMin,float& statMax)
{  // d'apres Brunner d'adresse : http://wiki.openstreetmap.org/wiki/Hiking/openhikingmap/color-shade
   if (!QFile::exists(src)) qFatal("qFatal ColorShade::ColorShade/1");

   $ramp=ramp;

   double adfGeoTransform[6];
   int i,j;
   const char* format="GTiff";
   QVector3D tempColor;
   str256 srcFile,dstFile;

   strcpy(srcFile,qPrintable(src));
   strcpy(dstFile,qPrintable(dst));

   GDALAllRegister();

   // Open dataset and get raster band (A set of associated raster bands, usually from one file)
   GDALDataset* poDataset=(GDALDataset*)GDALOpen(srcFile,GA_ReadOnly);
   if (poDataset==NULL) qFatal("qFatal ColorShade::ColorShade/2");

   GDALRasterBand* poInBand=poDataset->GetRasterBand(1);    // Fetch a band object for a dataset.
   poDataset->GetGeoTransform(adfGeoTransform); // Fetch the affine transformation coefficients.

   // attributs
   $nXSize=poInBand->GetXSize();                            // Fetch XSize of raster
   $nYSize=poInBand->GetYSize();                            // Fetch YSize of raster
   $inPixels=(float*)CPLMalloc(sizeof(float)*$nXSize*$nYSize);

   float* rowIn=(float*)CPLMalloc(sizeof(float)*$nXSize);   // entiers sur 16 bits?

   float* rowRed=(float*)CPLMalloc(sizeof(float)*$nXSize);  // les traitements se font rangees par rangees
   float* rowGreen=(float*)CPLMalloc(sizeof(float)*$nXSize);
   float* rowBlue=(float*)CPLMalloc(sizeof(float)*$nXSize);

   // Create the output dataset and copy over relevant metadata
   GDALDriver* poDriver=GetGDALDriverManager()->GetDriverByName(format); // Format specific driver
   // Create a new dataset with this driver (A set of associated raster bands, usually from one file)
   GDALDataset* poDS=poDriver->Create(dstFile,$nXSize,$nYSize,3,GDT_Byte,NULL);
   poDS->SetGeoTransform(adfGeoTransform);                  // Set the affine transformation coefficients.
   poDS->SetProjection(poDataset->GetProjectionRef());      // Set the projection reference string for this dataset.

   GDALRasterBand* poBandRed=poDS->GetRasterBand(1);        // Fetch a band object for a dataset.
   poBandRed->SetNoDataValue(0);                            // Set the no data value for this band.
   GDALRasterBand* poBandGreen=poDS->GetRasterBand(2);      // A single raster band (or channel)
   poBandGreen->SetNoDataValue(0);
   GDALRasterBand* poBandBlue=poDS->GetRasterBand(3);
   poBandBlue->SetNoDataValue(0);

   const float deg2rad=float(M_PI/180.0);
   float win[9];
   float shade;
   float x,y;
   float aspect;
   float slope;
   float z=1.0;                                             // ZFactor (default=1)
   float scale=1.0;                                         // scale (default=1)
   float az=315.0;                                          // Azimuth (default=315)
   float alt=45.0;                                          // Altitude (default=45)
   float add=0;                                             // addFactor (0..1, default 0) : a triturer
   double sinAlt=sin(alt*deg2rad),cosAlt=cos(alt*deg2rad);
   float kewres=z/(8.0*adfGeoTransform[1]*scale);           // ew : east-west ?
   float knsres=z/(8.0*adfGeoTransform[5]*scale);           // ns : north-south ?
   float h,delta;

   statMin=10000.0;
   statMax=-10000.0;

   GDALAllRegister();

   for (i=0; i<$nYSize; i++) {                  // 1ere passe
      if (poInBand->RasterIO(GF_Read,0,i,$nXSize,1,rowIn,$nXSize,1,GDT_Float32,0,0)>CE_Warning) qFatal("qFatal ColorShade::ColorShade/1");
      for (j=0; j<$nXSize; j++) {
         h=rowIn[j];
         if (h<statMin) statMin=h;
         if (h>statMax) statMax=h;
         $inPixels[j*$nYSize+i]=h;
      }
   }

   // Move a 3x3 window over each cell (where the cell in question is #4)
   //    0 1 2
   //    3 4 5
   //    6 7 8

   delta=statMax-statMin;
   az=(az-90.0)*deg2rad;

   for (i=0; i<$nYSize; i++) {                  // 2eme passe
      for (j=0; j<$nXSize; j++) {
         getCell(j,i,win);

         // compute Hillshade, first Slope
         x=(win[0]+win[3]+win[3]+win[6]-win[2]-win[5]-win[5]-win[8])*kewres;
         y=(win[6]+win[7]+win[7]+win[8]-win[0]-win[1]-win[1]-win[2])*knsres;

         slope=90.0*deg2rad-atan(sqrt(x*x+y*y));

         // ... then aspect...
         // atan renvoie l'arc tangente de l'argument entre -pi/2 et pi/2
         // atan2 renvoie l'arc tangente de y/x entre -pi et pi
         aspect=atan2(x,y);

         // ... then the shade value
         shade=sinAlt*sin(slope) + cosAlt*cos(slope)*cos(az-aspect);
         shade=(shade<=0.0)? 1.0 : 1.0+254.0*shade;

         tempColor=getColor(($inPixels[j*$nYSize+i]-statMin)/delta)*(add+shade/256.0);
         rowRed[j]=tempColor.x();
         rowGreen[j]=tempColor.y();
         rowBlue[j]=tempColor.z();
      }

      // Write lines to output raster
      if (poBandRed->RasterIO(GF_Write,0,i,$nXSize,1,rowRed,$nXSize,1,GDT_Float32,0,0)>CE_Warning) qFatal("qFatal ColorShade::ColorShade/2");
      if (poBandGreen->RasterIO(GF_Write,0,i,$nXSize,1,rowGreen,$nXSize,1,GDT_Float32,0,0)>CE_Warning) qFatal("qFatal ColorShade::ColorShade/3");
      if (poBandBlue->RasterIO(GF_Write,0,i,$nXSize,1,rowBlue,$nXSize,1,GDT_Float32,0,0)>CE_Warning) qFatal("qFatal ColorShade::ColorShade/4");
   }

   GDALClose(poDS);        // After you have finished working with the returned dataset, it is required...
   CPLFree(rowBlue);       // to close it with GDALClose()
   CPLFree(rowGreen);
   CPLFree(rowRed);
   CPLFree(rowIn);
   CPLFree($inPixels);
   GDALClose(poDataset);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void World::unzip(const QString& src,const QString& dstDir)
{
   if (!QFile::exists(src)) qFatal("qFatal World::unzip/1");
   QDir qDir(dstDir);
   if (!qDir.exists()) qFatal("qFatal World::unzip/2");
   QStringList list=JlCompress::extractDir(src,dstDir);
   if (list.isEmpty()) qFatal("qFatal World::unzip/3");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void World::hgtToTif(const QString& src,const QString& dst)
{
   if (!QFile::exists(src)) qFatal("qFatal World::hgtToTif/1");

   const char* arg[]={
      "exe_name",             // 0
      "src_dataset",          // 1
      "dst_dataset",          // 2
      "-q"                    // 3 : mode silencieux
   };
   str256 src_dataset,dst_dataset;
   int rtrn=0;

   arg[1]=strcpy(src_dataset,qPrintable(src));
   arg[2]=strcpy(dst_dataset,qPrintable(dst));

   GdalTranslate(sizeof(arg)/sizeof(char*),(char**)arg,rtrn);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void World::voidFill(const QString& src)
{
   if (!QFile::exists(src)) qFatal("qFatal World::voidFill/1");

   str256 pszFilename;
   strcpy(pszFilename,qPrintable(src));

   GDALAllRegister();

   GDALDataset* dataset=(GDALDataset*)GDALOpen(pszFilename,GA_Update);
   if (dataset==NULL) qFatal("qFatal World::voidFill/2");

   if (GDALFillNodata(dataset->GetRasterBand(1),NULL,0,0,1,NULL,NULL,NULL)==CE_Failure)
      qFatal("qFatal World::voidFill/3");

   GDALClose(dataset);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void World::warp(const QString& src,const QString& dst)
{
   if (!QFile::exists(src)) qFatal("qFatal World::warp/1");

   const char* arg[]={
      "exe_name",             // 0
      "-s_srs",               // 1 : s_ource
      "EPSG:4326",            // 2
      "-t_srs",               // 3 : t_arget
      PROJ,                   // 4
      "-rc",                  // 5 : interpolation cubique
      "srcfile",              // 6
      "dstfile",              // 7
      "-q"                    // 8 : mode silencieux
   };

   str256 srcFile,dstFile;
   int rtrn=0;

   arg[6]=strcpy(srcFile,qPrintable(src));
   arg[7]=strcpy(dstFile,qPrintable(dst));

   GdalWarp(sizeof(arg)/sizeof(char*),(char**)arg,rtrn);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void World::greyScale(const QString& src,const QString& dst,int statMin,int statMax)
{
   if (!QFile::exists(src)) qFatal("qFatal World::greyScale/1");

   const char* arg[]={
      "exe_name",             // 0
      "-ot",                  // 1 : pour que la bande en sortie soit du type de donnees indique
      "byte",                 // 2
      "-scale",               // 3 : redimensionne la valeur du pixel en entree a partir du domaine
      "src_min",              // 4
      "src_max",              // 5 : s'il est omis le domaine de sortie est de 0 a 255
      "src_dataset",          // 6
      "dst_dataset",          // 7
      "-q"                    // 8 : mode silencieux
   };

   str256 src_min,src_max,src_dataset,dst_dataset;
   int rtrn=0;

   sprintf(src_min,"%i",statMin);
   sprintf(src_max,"%i",statMax);

   arg[4]=src_min;
   arg[5]=src_max;
   arg[6]=strcpy(src_dataset,qPrintable(src));
   arg[7]=strcpy(dst_dataset,qPrintable(dst));

   GdalTranslate(sizeof(arg)/sizeof(char*),(char**)arg,rtrn);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void World::metaData(const QString& src,const QString& dst,int statMin,int statMax)
{
   if (!QFile::exists(src)) qFatal("qFatal World::metaData/1");

   const char* arg[]={
      "exe_name",             // 0
      "-mo",                  // 1
      "STATISTICS_MINIMUM",   // 2
      "-mo",                  // 3
      "STATISTICS_MAXIMUM",   // 4
      "src_dataset",          // 5
      "dst_dataset",          // 6
      "-q"                    // 7 : mode silencieux
   };

   str256 hmin,hmax,src_dataset,dst_dataset;
   int rtrn=0;

   sprintf(hmin,"%s=%i",arg[2],statMin);
   sprintf(hmax,"%s=%i",arg[4],statMax);

   arg[2]=hmin;
   arg[4]=hmax;
   arg[5]=strcpy(src_dataset,qPrintable(src));
   arg[6]=strcpy(dst_dataset,qPrintable(dst));

   GdalTranslate(sizeof(arg)/sizeof(char*),(char**)arg,rtrn);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void World::elapsedTime(const char* section,QTime* clock,bool init)
{
   static int lastTime;

   if (init) lastTime=0;
   int time=clock->elapsed();

   qDebug("%s : %i ms",section,time-lastTime);
   lastTime=time;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

World::World(const QByteArray& array,const QString& regionDir,const QString& regionName,const QMap<qreal,QVector3D>& ramp,QTime* clock)
{
   float statMin,statMax;

   QString zipFile=regionDir+regionName+".hgt.zip";
   QFile file(zipFile);
   file.open(QIODevice::WriteOnly);
   file.write(array);
   file.close();
   elapsedTime("download",clock,true);

   unzip(zipFile,regionDir);
   elapsedTime("unzip",clock);

   hgtToTif(regionDir+regionName+".hgt",regionDir+"~geo.tif");
   elapsedTime("hgtToTif",clock);

   voidFill(regionDir+"~geo.tif");
   elapsedTime("voidfill",clock);

   warp(regionDir+"~geo.tif",regionDir+"~warp.tif");
   elapsedTime("warp",clock);

   ColorShade(regionDir+"~warp.tif",regionDir+regionName+"_colorshade.tif",ramp,statMin,statMax);
   elapsedTime("colorshade",clock);

   greyScale(regionDir+"~warp.tif",regionDir+regionName+"_greyscale.tif",int(statMin),int(statMax));
   elapsedTime("greyscale",clock);

   metaData(regionDir+"~warp.tif",regionDir+regionName+".tif",int(statMin),int(statMax));
   elapsedTime("metadata",clock);         // ecriture, dans les "metaDonnees", des hauteurs min et max

   if (!QFile::remove(regionDir+regionName+".hgt.zip")) qFatal("qFatal World::World/1");
   if (!QFile::remove(regionDir+regionName+".hgt")) qFatal("qFatal World::World/2");
   if (!QFile::remove(regionDir+"~geo.tif")) qFatal("qFatal World::World/3");
   if (!QFile::remove(regionDir+"~warp.tif")) qFatal("qFatal World::World/4");
   elapsedTime("cleaning",clock);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

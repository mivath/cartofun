/*
 - Date : 20/06/2012
 - Nom : viewer.h
*/

#ifndef VIEWER_H
#define VIEWER_H

#include <QtXmlPatterns>
#include <QOpenGLFunctions_2_0>

#include "gdaltransform.h"
#include "fun.h"

typedef float fv[3];

////////////////////////////////////////////////////////////////////////////////////////////////////

class Transform
{
   public:
      Transform(const QString& terrainDir);
      void setModel(const double modelPixelScale[3],const double modelTiepoint[6]);
      QPointF pixelToLonLat(const QPointF& p,bool oldStyle=false);
      QString preset();
      QPointF position() {return $position;}
      void setPosition(QPointF position) {$position=position;}
      double pixelScale() {return $pixelScale0;}
      void getCorners(QPointF& lonwLatn,QPointF& loneLats);
      QRectF getRect();
      ~Transform() {delete $gdalTransform;}

   private:
      GdalTransform* $gdalTransform;
      double $pixelScale0,$pixelScale1,$tiepoint3,$tiepoint4;
      QString $terrainDir;
      QPointF $position;            // coin leftTop de la "tuile" par rapport a la "region"
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class UnTransform
{
   public:
      UnTransform();
      void setModel(const double modelPixelScale[3],const double modelTiepoint[6]);
      QPointF lonLatToPixel(const QPointF& lonLat);
      ~UnTransform() {delete $gdalTransform;}

   private:
      GdalTransform* $gdalTransform;
      double $pixelScale0,$pixelScale1,$tiepoint3,$tiepoint4;
};


////////////////////////////////////////////////////////////////////////////////////////////////////

namespace GLop
{
   // public
   int gluUnProject(double winx,double winy,double winz,const double modelMatrix[16],const double projMatrix[16],
         const int viewport[4],double* objx,double* objy,double* objz);
   void crossProd(const double p[3],const double q[3],double r[3]);
   void normalize(double p[3]);
   void getCamPos(double cam[3],const double modelview[16]);

   // private
   void glopMakeIdentityd(double m[16]);
   void glopMultMatrixVecd(const double matrix[16],const double in[4],double out[4]);
   int glopInvertMatrixd(const double src[16],double inverse[16]);
   void glopMultMatricesd(const double a[16],const double b[16],double r[16]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

class HikingThread : public QThread
{  // d'apres https://github.com/DeiVadder/QtThreadExample
   Q_OBJECT

   public:
      HikingThread(NodeMapType& nodeMap,WayMapType& wayMap,SummitMapType& summitMap,const QRectF& rect,
         const QByteArray& array,const QString& terrainDir,bool keepXmlFile,QObject* parent=nullptr);

   private:
      NodeMapType& $nodeMap;
      WayMapType& $wayMap;
      SummitMapType& $summitMap;
      QRectF $rect;
      QByteArray $array;
      QString $terrainDir;
      bool $keepXmlFile;

   protected:
      virtual void run() override;

   signals:
      void operationDone(bool rslt);
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class Viewer : public QOpenGLWidget, protected QOpenGLFunctions_2_0
{
   Q_OBJECT

   public:
      Viewer(QMap<qreal,QVector3D>& ramp,QVector<QPointF> posVector,bool isRouting,Transform* transform,QWidget* parent);
      ~Viewer();
      void queryMetaData(const QString& metaData);
      void init(uchar* regionData,quint32 width);
      void refresh(const QString& arg);
      QVector<QPointF> posVector();
      void loadGpsFile(bool checked);
      void xmlToFun(const QByteArray& array,const QString& terrainDir,bool keepXmlFile,QWidget* parent);
      QString oldTexture(const QString& texture="");
      void toggleIsRouting();
      void openRouteDlg(bool checked);
      void clearRouteDlg(int pageIndex);
      void saveGpsPath();
      void clearHikingBuffer();

   private:
      int $statMin,$statMax;
      qreal $thickness;
      QColor $bgColor,$textColor;
      qint16 $posx,$posy;
      QMap<qreal,QVector3D> $ramp;
      QPoint $lastPos;
      float $rho,$phi,$theta,$scaleZ;
      float $light[4];
      Transform* $transform;
      RouteDlg* $routeDlg;
      QVector<QPointF> $posVector;
      QList<QPair<Segm,char>> $segmPath;
      QList<Step> $stepList;
      Routing* $routing;
      Branch* $branch;
      bool $isRouting;
      QPointF $coef;                      // pour retrouver les coordonnees 2D a partir des coordonnees "3D" de $nodeMap

      QOpenGLTexture* $texture;
      QVector<QVector3D> $vertexVector,$normalVector,$hikingVector;
      QPair<QList<int>,QVector<QVector3D>> $gpsPack;
      QVector<QVector2D> $textureVector;
      QVector<GLuint> $indexVector;
      QOpenGLBuffer *$vertexBuffer,*$normalBuffer,*$textureBuffer,*$indexBuffer,*$hikingBuffer,*$gpsBuffer;

      // id node, coords node, id way, index node
      NodeMapType $nodeMap;
      // id du way, id du node, index "interface", index du sommet (et donc trie), id du sommet
      WayMapType $wayMap;
      // id du sommet, id du voisin, distance les separant
      SummitMapType $summitMap;

      // animation
      QTimer* $timer;
      QList<QList<float>> $run;
      uchar $runFlag;

      // implementation
      void setNormalVector();
      void makeColorImage(QImage& image);
      void getTexture(const QString& texture);
      char* compass();
      void moveTo(char dx,char dy);
      void setPos(qint16 posx,qint16 posy);
      template<typename T> void createBuffer(QOpenGLBuffer* buffer,QVector<T>* vector,QOpenGLBuffer::UsagePattern pattern);
      void createVBOs();
      void updateBuffer(QOpenGLBuffer* buffer,QVector<QVector3D>* vector,bool allocate=false);
      void paintVBOs();
      void paintAxes();
      void presetConstVectors();
      bool loadFunFile();
      int getPAM(QXmlQuery& query,const QString& name);
      void isoSegment(int indexA,int indexO,int indexB);
      void paintIsoLine();
      void checkExtensions();
      bool polyLines(const QString& option,int size,const QColor& color,QOpenGLBuffer* buffer,qreal lineWidth);
      void paintPolyLines();
      bool getNearest(qint64& nid,qint64& sid,qreal x,qreal y);
      void paintPath();
      void unProject(QMouseEvent* event,double obj[3]);
      void lookAt(const double eye[3],const double center[3],double up[3]);
      void perspective(double fovY,double aspect,double zNear,double zFar);
      qreal ellipticArc();
      void drawCookies(char tabIndex);
      void setDlgHikingCurve();
      void setDlgGpsCurve(const QString& gpsFile="");
      void makeConnection(const QObject* sender);
      void renderText();
      bool optionsContains(const QString& option);
      void saveGpxFile(QTextStream& stream,const QString& name,const QList<QPointF>& saveList);
      void saveKmlFile(QTextStream& stream,const QString& name,const QList<QPointF>& saveList);
      uchar forkInfo(qint64 prior,qint64 sid,qint64 next,const QPointF& a);
      float mod(float f);

   protected:
      void initializeGL();
      void paintGL();
      void resizeGL(int width,int height);

      void keyPressEvent(QKeyEvent* event);
      void wheelEvent(QWheelEvent* event);
      void mousePressEvent(QMouseEvent* event);
      void mouseMoveEvent(QMouseEvent* event);
      void mouseDoubleClickEvent(QMouseEvent* event);

   public slots:
      void closeRouteDlg();

   private slots:
      void nodePos(QVector<QPointF>& posVector);
      void clearSegmPath();
      void addSegm(const Segm& segm,const QColor& color);
      void msgBox(bool update,const QString& s);
      void addStepList(const QList<Step>& stepList);
      void timeout();

   signals:
      void selectedPos(QPointF pos);
      void getOptionState(const QString& option,bool& state);
      void resetOptions(const QStringList& options=QStringList());
};

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif

////////////////////////////////////////////////////////////////////////////////////////////////////

function $(id)
{
   return document.getElementById(id);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

function change(select)
{
   var name=select.options[select.selectedIndex].innerText;
   name=name.replace(/ /g,'_').toLowerCase()+'.html';
   var splitList=window.location.href.split('/');
   splitList[splitList.length-1]=name;
   var url=splitList[0];
   
   for (var i=1,m=splitList.length; i<m; i++) url+='/'+splitList[i];
   window.location.assign(url);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

function htmlTab(tab)
{
   var s='<center><table>';
   
   s+='<tr>';
   for (var i=0,m=tab[0].length; i<m; i++) s+='<th>'+tab[0][i]+'</th>';
   s+='</tr>';
   
   for (var j=1,n=tab.length; j<n; j++) {
      s+='<tr>';
      if (tab[j][0]=='') for (var i=0,m=tab[j].length; i<m; i++) s+='<th class="separator"></th>';
      else for (var i=0,m=tab[j].length; i<m; i++) s+='<td>'+tab[j][i]+'</td>';
      s+='</tr>';
   }
   
   s+='</table></center>';

   return s;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

function main(arg)
{
   var pageList;
   if (arg=='fr') pageList=['INDEX','INTERFACE WORLD','INTERFACE REGION','INTERFACE TERRAIN','LE PLUGIN ROUTEDLG',
      'COMPILATION AVEC MINGW','COMPILATION AVEC MSVC','FAQ'];
   else if (arg=='en') pageList=['INDEX','THE WORLD INTERFACE','THE REGION INTERFACE','THE TERRAIN INTERFACE','THE ROUTEDLG PLUGIN',
      'COMPILATION WITH MINGW','COMPILATION WITH MSVC','FAQ'];
   else {
      alert("error main/1");
      return;  
   }
   
   var splitList=window.location.href.split('/');
   var fileName=splitList[splitList.length-1];
   var name=fileName.split('.')[0];    
   name=name.replace(/_/g,' ').toUpperCase();
   
   var titleList=document.getElementsByTagName('title');
   var hList=document.getElementsByTagName('h');   
   titleList[0].innerText=hList[0].innerText=name;
   var insert='';
   
   for (var i=0,m=pageList.length; i<m; i++) 
         insert+=((pageList[i]==name)? '<option selected="selected">' : '<option>')+pageList[i]+'</option>';
   
   var selectList=document.getElementsByTagName('select');
   for (var i=0,m=selectList.length; i<m; i++) selectList[i].innerHTML=insert;
   
   var divList=document.getElementsByTagName('div');
   for (var i=0,m=divList.length; i<m; i++) divList[i].innerHTML=htmlTab(DATA[divList[i].id]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
